﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace BMIS.Donation.Migrations
{
    public partial class InitialCreate1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_blood_bag_donation_details_donation_encounters_donation_enc",
                table: "blood_bag_donation_details");

            migrationBuilder.DropTable(
                name: "blood_bags_setups");

            migrationBuilder.DropIndex(
                name: "ix_blood_bag_donation_details_donation_encounter_id",
                table: "blood_bag_donation_details");

            migrationBuilder.AddColumn<DateTime>(
                name: "created_on_utc_donation_queue",
                table: "donation_encounters",
                type: "timestamp without time zone",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "donation_encounter_id",
                table: "blood_bag_donation_details",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddColumn<string>(
                name: "weight_unit",
                table: "blood_bag_donation_details",
                type: "text",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "ix_blood_bag_donation_details_donation_encounter_id",
                table: "blood_bag_donation_details",
                column: "donation_encounter_id");

            migrationBuilder.AddForeignKey(
                name: "fk_blood_bag_donation_details_donation_encounters_donation_enc",
                table: "blood_bag_donation_details",
                column: "donation_encounter_id",
                principalTable: "donation_encounters",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_blood_bag_donation_details_donation_encounters_donation_enc",
                table: "blood_bag_donation_details");

            migrationBuilder.DropIndex(
                name: "ix_blood_bag_donation_details_donation_encounter_id",
                table: "blood_bag_donation_details");

            migrationBuilder.DropColumn(
                name: "created_on_utc_donation_queue",
                table: "donation_encounters");

            migrationBuilder.DropColumn(
                name: "weight_unit",
                table: "blood_bag_donation_details");

            migrationBuilder.AlterColumn<int>(
                name: "donation_encounter_id",
                table: "blood_bag_donation_details",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "blood_bags_setups",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    blood_bag_type = table.Column<string>(type: "text", nullable: true),
                    created_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    deleted = table.Column<bool>(type: "boolean", nullable: false),
                    entity_guid = table.Column<Guid>(type: "uuid", nullable: false),
                    maximum_quantity = table.Column<int>(type: "integer", nullable: false),
                    minimum_quantity = table.Column<int>(type: "integer", nullable: false),
                    quantity_unit = table.Column<int>(type: "integer", nullable: false),
                    type_of_donation = table.Column<string>(type: "text", nullable: true),
                    updated_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_blood_bags_setups", x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: "ix_blood_bag_donation_details_donation_encounter_id",
                table: "blood_bag_donation_details",
                column: "donation_encounter_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_blood_bags_setups_entity_guid",
                table: "blood_bags_setups",
                column: "entity_guid",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "fk_blood_bag_donation_details_donation_encounters_donation_enc",
                table: "blood_bag_donation_details",
                column: "donation_encounter_id",
                principalTable: "donation_encounters",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
