﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BMIS.Donation.Migrations
{
    public partial class PortalKeyAddedOnDonor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "portal_key",
                table: "donors",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<double>(
                name: "day_int",
                table: "daily_donation_query_outputs",
                type: "double precision",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "portal_key",
                table: "donors");

            migrationBuilder.DropColumn(
                name: "day_int",
                table: "daily_donation_query_outputs");
        }
    }
}
