﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BMIS.Donation.Migrations
{
    public partial class InitialCreate8 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "dispatched_by",
                table: "batch_processings",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "facility_to_name",
                table: "batch_processings",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "team_lead",
                table: "batch_processings",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "dispatched_by",
                table: "batch_processings");

            migrationBuilder.DropColumn(
                name: "facility_to_name",
                table: "batch_processings");

            migrationBuilder.DropColumn(
                name: "team_lead",
                table: "batch_processings");
        }
    }
}
