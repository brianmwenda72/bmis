﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace BMIS.Donation.Migrations
{
    public partial class InitialCreate25 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "bag_component_details");

            migrationBuilder.DropTable(
                name: "bag_definitions");

            migrationBuilder.DropTable(
                name: "component_definitions");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "bag_definitions",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    bag_type = table.Column<string>(type: "text", nullable: true),
                    created_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    deleted = table.Column<bool>(type: "boolean", nullable: false),
                    entity_guid = table.Column<Guid>(type: "uuid", nullable: false),
                    updated_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_bag_definitions", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "component_definitions",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    code = table.Column<string>(type: "text", nullable: true),
                    created_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    deleted = table.Column<bool>(type: "boolean", nullable: false),
                    entity_guid = table.Column<Guid>(type: "uuid", nullable: false),
                    max_storage_temparature = table.Column<decimal>(type: "numeric", nullable: false),
                    max_transportation_temparature = table.Column<decimal>(type: "numeric", nullable: false),
                    min_storage_temparature = table.Column<decimal>(type: "numeric", nullable: false),
                    min_transportation_temparature = table.Column<decimal>(type: "numeric", nullable: false),
                    name = table.Column<string>(type: "text", nullable: true),
                    shelf_life = table.Column<int>(type: "integer", nullable: false),
                    updated_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_component_definitions", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "bag_component_details",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    bag_definition_id = table.Column<int>(type: "integer", nullable: true),
                    component_definition_id = table.Column<int>(type: "integer", nullable: true),
                    created_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    deleted = table.Column<bool>(type: "boolean", nullable: false),
                    entity_guid = table.Column<Guid>(type: "uuid", nullable: false),
                    updated_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_bag_component_details", x => x.id);
                    table.ForeignKey(
                        name: "fk_bag_component_details_bag_definitions_bag_definition_id",
                        column: x => x.bag_definition_id,
                        principalTable: "bag_definitions",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_bag_component_details_component_definitions_component_defin",
                        column: x => x.component_definition_id,
                        principalTable: "component_definitions",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "ix_bag_component_details_bag_definition_id",
                table: "bag_component_details",
                column: "bag_definition_id");

            migrationBuilder.CreateIndex(
                name: "ix_bag_component_details_component_definition_id",
                table: "bag_component_details",
                column: "component_definition_id");

            migrationBuilder.CreateIndex(
                name: "ix_bag_component_details_entity_guid",
                table: "bag_component_details",
                column: "entity_guid",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_bag_definitions_entity_guid",
                table: "bag_definitions",
                column: "entity_guid",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_component_definitions_entity_guid",
                table: "component_definitions",
                column: "entity_guid",
                unique: true);
        }
    }
}
