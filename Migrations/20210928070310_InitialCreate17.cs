﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace BMIS.Donation.Migrations
{
    public partial class InitialCreate17 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_bag_component_details_bag_definations_bag_defination_id",
                table: "bag_component_details");

            migrationBuilder.DropForeignKey(
                name: "fk_bag_component_details_component_definations_component_defin",
                table: "bag_component_details");

            migrationBuilder.DropTable(
                name: "bag_definations");

            migrationBuilder.DropTable(
                name: "component_definations");

            migrationBuilder.RenameColumn(
                name: "component_defination_id",
                table: "bag_component_details",
                newName: "component_definition_id");

            migrationBuilder.RenameColumn(
                name: "bag_defination_id",
                table: "bag_component_details",
                newName: "bag_definition_id");

            migrationBuilder.RenameIndex(
                name: "ix_bag_component_details_component_defination_id",
                table: "bag_component_details",
                newName: "ix_bag_component_details_component_definition_id");

            migrationBuilder.RenameIndex(
                name: "ix_bag_component_details_bag_defination_id",
                table: "bag_component_details",
                newName: "ix_bag_component_details_bag_definition_id");

            migrationBuilder.CreateTable(
                name: "bag_definitions",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    entity_guid = table.Column<Guid>(type: "uuid", nullable: false),
                    bag_type = table.Column<string>(type: "text", nullable: true),
                    created_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    updated_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_bag_definitions", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "component_definitions",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    entity_guid = table.Column<Guid>(type: "uuid", nullable: false),
                    name = table.Column<string>(type: "text", nullable: true),
                    code = table.Column<string>(type: "text", nullable: true),
                    transportation_temparature_range = table.Column<string>(type: "text", nullable: true),
                    storage_temparature_range = table.Column<string>(type: "text", nullable: true),
                    expiry_days = table.Column<int>(type: "integer", nullable: false),
                    created_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    updated_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_component_definitions", x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: "ix_bag_definitions_entity_guid",
                table: "bag_definitions",
                column: "entity_guid",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_component_definitions_entity_guid",
                table: "component_definitions",
                column: "entity_guid",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "fk_bag_component_details_bag_definitions_bag_definition_id",
                table: "bag_component_details",
                column: "bag_definition_id",
                principalTable: "bag_definitions",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "fk_bag_component_details_component_definitions_component_defin",
                table: "bag_component_details",
                column: "component_definition_id",
                principalTable: "component_definitions",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_bag_component_details_bag_definitions_bag_definition_id",
                table: "bag_component_details");

            migrationBuilder.DropForeignKey(
                name: "fk_bag_component_details_component_definitions_component_defin",
                table: "bag_component_details");

            migrationBuilder.DropTable(
                name: "bag_definitions");

            migrationBuilder.DropTable(
                name: "component_definitions");

            migrationBuilder.RenameColumn(
                name: "component_definition_id",
                table: "bag_component_details",
                newName: "component_defination_id");

            migrationBuilder.RenameColumn(
                name: "bag_definition_id",
                table: "bag_component_details",
                newName: "bag_defination_id");

            migrationBuilder.RenameIndex(
                name: "ix_bag_component_details_component_definition_id",
                table: "bag_component_details",
                newName: "ix_bag_component_details_component_defination_id");

            migrationBuilder.RenameIndex(
                name: "ix_bag_component_details_bag_definition_id",
                table: "bag_component_details",
                newName: "ix_bag_component_details_bag_defination_id");

            migrationBuilder.CreateTable(
                name: "bag_definations",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    bag_type = table.Column<string>(type: "text", nullable: true),
                    created_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    deleted = table.Column<bool>(type: "boolean", nullable: false),
                    entity_guid = table.Column<Guid>(type: "uuid", nullable: false),
                    updated_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_bag_definations", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "component_definations",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    code = table.Column<string>(type: "text", nullable: true),
                    created_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    deleted = table.Column<bool>(type: "boolean", nullable: false),
                    entity_guid = table.Column<Guid>(type: "uuid", nullable: false),
                    expiry_days = table.Column<int>(type: "integer", nullable: false),
                    name = table.Column<string>(type: "text", nullable: true),
                    storage_temparature_range = table.Column<string>(type: "text", nullable: true),
                    transportation_temparature_range = table.Column<string>(type: "text", nullable: true),
                    updated_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_component_definations", x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: "ix_bag_definations_entity_guid",
                table: "bag_definations",
                column: "entity_guid",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_component_definations_entity_guid",
                table: "component_definations",
                column: "entity_guid",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "fk_bag_component_details_bag_definations_bag_defination_id",
                table: "bag_component_details",
                column: "bag_defination_id",
                principalTable: "bag_definations",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "fk_bag_component_details_component_definations_component_defin",
                table: "bag_component_details",
                column: "component_defination_id",
                principalTable: "component_definations",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
