﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BMIS.Donation.Migrations
{
    public partial class InitialCreate23 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "health_facility_reffered_to",
                table: "donation_encounters");

            migrationBuilder.AddColumn<int>(
                name: "health_facility_id",
                table: "donation_encounters",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "ix_donation_encounters_health_facility_id",
                table: "donation_encounters",
                column: "health_facility_id");

            migrationBuilder.AddForeignKey(
                name: "fk_donation_encounters_health_facilities_health_facility_id",
                table: "donation_encounters",
                column: "health_facility_id",
                principalTable: "health_facilities",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_donation_encounters_health_facilities_health_facility_id",
                table: "donation_encounters");

            migrationBuilder.DropIndex(
                name: "ix_donation_encounters_health_facility_id",
                table: "donation_encounters");

            migrationBuilder.DropColumn(
                name: "health_facility_id",
                table: "donation_encounters");

            migrationBuilder.AddColumn<string>(
                name: "health_facility_reffered_to",
                table: "donation_encounters",
                type: "text",
                nullable: true);
        }
    }
}
