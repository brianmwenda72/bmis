﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace BMIS.Donation.Migrations
{
    public partial class InitialCreate3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "drive_manager",
                table: "donation_drives");

            migrationBuilder.RenameColumn(
                name: "is_sent",
                table: "vacutainers",
                newName: "sent");

            migrationBuilder.RenameColumn(
                name: "transport_manager",
                table: "donation_drives",
                newName: "team_lead");

            migrationBuilder.RenameColumn(
                name: "is_sent",
                table: "blood_bag_donation_details",
                newName: "sent");

            migrationBuilder.AddColumn<bool>(
                name: "discarded",
                table: "vacutainers",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "vacutainer_status",
                table: "vacutainers",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "discarded",
                table: "blood_bag_donation_details",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "bag_definations",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    entity_guid = table.Column<Guid>(type: "uuid", nullable: false),
                    bag_type = table.Column<string>(type: "text", nullable: true),
                    created_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    updated_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_bag_definations", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "component_definations",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    entity_guid = table.Column<Guid>(type: "uuid", nullable: false),
                    name = table.Column<string>(type: "text", nullable: true),
                    code = table.Column<string>(type: "text", nullable: true),
                    bag_defination_id = table.Column<int>(type: "integer", nullable: true),
                    created_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    updated_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_component_definations", x => x.id);
                    table.ForeignKey(
                        name: "fk_component_definations_bag_definations_bag_defination_id",
                        column: x => x.bag_defination_id,
                        principalTable: "bag_definations",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "ix_bag_definations_entity_guid",
                table: "bag_definations",
                column: "entity_guid",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_component_definations_bag_defination_id",
                table: "component_definations",
                column: "bag_defination_id");

            migrationBuilder.CreateIndex(
                name: "ix_component_definations_entity_guid",
                table: "component_definations",
                column: "entity_guid",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "component_definations");

            migrationBuilder.DropTable(
                name: "bag_definations");

            migrationBuilder.DropColumn(
                name: "discarded",
                table: "vacutainers");

            migrationBuilder.DropColumn(
                name: "vacutainer_status",
                table: "vacutainers");

            migrationBuilder.DropColumn(
                name: "discarded",
                table: "blood_bag_donation_details");

            migrationBuilder.RenameColumn(
                name: "sent",
                table: "vacutainers",
                newName: "is_sent");

            migrationBuilder.RenameColumn(
                name: "team_lead",
                table: "donation_drives",
                newName: "transport_manager");

            migrationBuilder.RenameColumn(
                name: "sent",
                table: "blood_bag_donation_details",
                newName: "is_sent");

            migrationBuilder.AddColumn<string>(
                name: "drive_manager",
                table: "donation_drives",
                type: "text",
                nullable: true);
        }
    }
}
