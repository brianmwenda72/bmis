﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BMIS.Donation.Migrations
{
    public partial class InitialCreate19 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "storage_temparature_range",
                table: "component_definitions");

            migrationBuilder.DropColumn(
                name: "transportation_temparature_range",
                table: "component_definitions");

            migrationBuilder.RenameColumn(
                name: "expiry_days",
                table: "component_definitions",
                newName: "shelf_life");

            migrationBuilder.AddColumn<decimal>(
                name: "max_storage_temparature",
                table: "component_definitions",
                type: "numeric",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "max_transportation_temparature",
                table: "component_definitions",
                type: "numeric",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "min_storage_temparature",
                table: "component_definitions",
                type: "numeric",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "min_transportation_temparature",
                table: "component_definitions",
                type: "numeric",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "max_storage_temparature",
                table: "component_definitions");

            migrationBuilder.DropColumn(
                name: "max_transportation_temparature",
                table: "component_definitions");

            migrationBuilder.DropColumn(
                name: "min_storage_temparature",
                table: "component_definitions");

            migrationBuilder.DropColumn(
                name: "min_transportation_temparature",
                table: "component_definitions");

            migrationBuilder.RenameColumn(
                name: "shelf_life",
                table: "component_definitions",
                newName: "expiry_days");

            migrationBuilder.AddColumn<string>(
                name: "storage_temparature_range",
                table: "component_definitions",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "transportation_temparature_range",
                table: "component_definitions",
                type: "text",
                nullable: true);
        }
    }
}
