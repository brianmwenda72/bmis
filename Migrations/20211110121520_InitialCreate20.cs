﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BMIS.Donation.Migrations
{
    public partial class InitialCreate20 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "schedule_name",
                table: "donation_drives",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "schedule_name",
                table: "donation_drives");
        }
    }
}
