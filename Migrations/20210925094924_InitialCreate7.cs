﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BMIS.Donation.Migrations
{
    public partial class InitialCreate7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_blood_bag_donation_details_batch_processing_batch_processin",
                table: "blood_bag_donation_details");

            migrationBuilder.DropForeignKey(
                name: "fk_vacutainers_batch_processing_batch_processing_id",
                table: "vacutainers");

            migrationBuilder.DropPrimaryKey(
                name: "pk_batch_processing",
                table: "batch_processing");

            migrationBuilder.RenameTable(
                name: "batch_processing",
                newName: "batch_processings");

            migrationBuilder.AddColumn<int>(
                name: "donation_drive_id",
                table: "batch_processings",
                type: "integer",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "pk_batch_processings",
                table: "batch_processings",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "ix_batch_processings_donation_drive_id",
                table: "batch_processings",
                column: "donation_drive_id");

            migrationBuilder.CreateIndex(
                name: "ix_batch_processings_entity_guid",
                table: "batch_processings",
                column: "entity_guid",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "fk_batch_processings_donation_drives_donation_drive_id",
                table: "batch_processings",
                column: "donation_drive_id",
                principalTable: "donation_drives",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "fk_blood_bag_donation_details_batch_processings_batch_processi",
                table: "blood_bag_donation_details",
                column: "batch_processing_id",
                principalTable: "batch_processings",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "fk_vacutainers_batch_processings_batch_processing_id",
                table: "vacutainers",
                column: "batch_processing_id",
                principalTable: "batch_processings",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_batch_processings_donation_drives_donation_drive_id",
                table: "batch_processings");

            migrationBuilder.DropForeignKey(
                name: "fk_blood_bag_donation_details_batch_processings_batch_processi",
                table: "blood_bag_donation_details");

            migrationBuilder.DropForeignKey(
                name: "fk_vacutainers_batch_processings_batch_processing_id",
                table: "vacutainers");

            migrationBuilder.DropPrimaryKey(
                name: "pk_batch_processings",
                table: "batch_processings");

            migrationBuilder.DropIndex(
                name: "ix_batch_processings_donation_drive_id",
                table: "batch_processings");

            migrationBuilder.DropIndex(
                name: "ix_batch_processings_entity_guid",
                table: "batch_processings");

            migrationBuilder.DropColumn(
                name: "donation_drive_id",
                table: "batch_processings");

            migrationBuilder.RenameTable(
                name: "batch_processings",
                newName: "batch_processing");

            migrationBuilder.AddPrimaryKey(
                name: "pk_batch_processing",
                table: "batch_processing",
                column: "id");

            migrationBuilder.AddForeignKey(
                name: "fk_blood_bag_donation_details_batch_processing_batch_processin",
                table: "blood_bag_donation_details",
                column: "batch_processing_id",
                principalTable: "batch_processing",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "fk_vacutainers_batch_processing_batch_processing_id",
                table: "vacutainers",
                column: "batch_processing_id",
                principalTable: "batch_processing",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
