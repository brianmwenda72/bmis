﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BMIS.Donation.Migrations
{
    public partial class InitialCreate5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "defferment_reason",
                table: "donors");

            migrationBuilder.DropColumn(
                name: "defferment_type",
                table: "donors");

            migrationBuilder.DropColumn(
                name: "eligible",
                table: "donors");

            migrationBuilder.DropColumn(
                name: "is_deffered",
                table: "donors");

            migrationBuilder.AddColumn<bool>(
                name: "defered",
                table: "donation_encounters",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "deferment_reason",
                table: "donation_encounters",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "deferment_type",
                table: "donation_encounters",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "defered",
                table: "donation_encounters");

            migrationBuilder.DropColumn(
                name: "deferment_reason",
                table: "donation_encounters");

            migrationBuilder.DropColumn(
                name: "deferment_type",
                table: "donation_encounters");

            migrationBuilder.AddColumn<string>(
                name: "defferment_reason",
                table: "donors",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "defferment_type",
                table: "donors",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "eligible",
                table: "donors",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "is_deffered",
                table: "donors",
                type: "boolean",
                nullable: false,
                defaultValue: false);
        }
    }
}
