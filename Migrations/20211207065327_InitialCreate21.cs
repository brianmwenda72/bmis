﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BMIS.Donation.Migrations
{
    public partial class InitialCreate21 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "declaration_donation_type",
                table: "questionnaires",
                type: "text",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "daily_donation_query_outputs",
                columns: table => new
                {
                    day = table.Column<string>(type: "text", nullable: true),
                    successful_donations = table.Column<int>(type: "integer", nullable: false),
                    diferred_donations = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "drive_query_outputs",
                columns: table => new
                {
                    month = table.Column<string>(type: "text", nullable: true),
                    active_drives = table.Column<int>(type: "integer", nullable: false),
                    completed_drives = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "daily_donation_query_outputs");

            migrationBuilder.DropTable(
                name: "drive_query_outputs");

            migrationBuilder.DropColumn(
                name: "declaration_donation_type",
                table: "questionnaires");
        }
    }
}
