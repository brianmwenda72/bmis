﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace BMIS.Donation.Migrations
{
    public partial class InitialCreate16 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_component_definations_bag_definations_bag_defination_id",
                table: "component_definations");

            migrationBuilder.DropIndex(
                name: "ix_component_definations_bag_defination_id",
                table: "component_definations");

            migrationBuilder.DropColumn(
                name: "bag_defination_id",
                table: "component_definations");

            migrationBuilder.AddColumn<int>(
                name: "expiry_days",
                table: "component_definations",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "storage_temparature_range",
                table: "component_definations",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "transportation_temparature_range",
                table: "component_definations",
                type: "text",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "bag_component_details",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    entity_guid = table.Column<Guid>(type: "uuid", nullable: false),
                    bag_defination_id = table.Column<int>(type: "integer", nullable: true),
                    component_defination_id = table.Column<int>(type: "integer", nullable: true),
                    created_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    updated_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_bag_component_details", x => x.id);
                    table.ForeignKey(
                        name: "fk_bag_component_details_bag_definations_bag_defination_id",
                        column: x => x.bag_defination_id,
                        principalTable: "bag_definations",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_bag_component_details_component_definations_component_defin",
                        column: x => x.component_defination_id,
                        principalTable: "component_definations",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "ix_bag_component_details_bag_defination_id",
                table: "bag_component_details",
                column: "bag_defination_id");

            migrationBuilder.CreateIndex(
                name: "ix_bag_component_details_component_defination_id",
                table: "bag_component_details",
                column: "component_defination_id");

            migrationBuilder.CreateIndex(
                name: "ix_bag_component_details_entity_guid",
                table: "bag_component_details",
                column: "entity_guid",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "bag_component_details");

            migrationBuilder.DropColumn(
                name: "expiry_days",
                table: "component_definations");

            migrationBuilder.DropColumn(
                name: "storage_temparature_range",
                table: "component_definations");

            migrationBuilder.DropColumn(
                name: "transportation_temparature_range",
                table: "component_definations");

            migrationBuilder.AddColumn<int>(
                name: "bag_defination_id",
                table: "component_definations",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "ix_component_definations_bag_defination_id",
                table: "component_definations",
                column: "bag_defination_id");

            migrationBuilder.AddForeignKey(
                name: "fk_component_definations_bag_definations_bag_defination_id",
                table: "component_definations",
                column: "bag_defination_id",
                principalTable: "bag_definations",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
