﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BMIS.Donation.Migrations
{
    public partial class InitialCreate15 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "donation_start_time",
                table: "donation_encounters",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "donation_stop_time",
                table: "donation_encounters",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "donation_start_time",
                table: "donation_encounters");

            migrationBuilder.DropColumn(
                name: "donation_stop_time",
                table: "donation_encounters");
        }
    }
}
