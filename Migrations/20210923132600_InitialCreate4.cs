﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BMIS.Donation.Migrations
{
    public partial class InitialCreate4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "discard_reason",
                table: "blood_bag_donation_details",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "discard_reason",
                table: "blood_bag_donation_details");
        }
    }
}
