﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace BMIS.Donation.Migrations
{
    public partial class InitialCreate6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "batch_processing_id",
                table: "vacutainers",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "batch_processing_id",
                table: "blood_bag_donation_details",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "batch_processing",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    entity_guid = table.Column<Guid>(type: "uuid", nullable: false),
                    batch_number = table.Column<string>(type: "text", nullable: true),
                    batch_processing_types = table.Column<int>(type: "integer", nullable: false),
                    sent = table.Column<bool>(type: "boolean", nullable: false),
                    created_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    updated_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_batch_processing", x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: "ix_vacutainers_batch_processing_id",
                table: "vacutainers",
                column: "batch_processing_id");

            migrationBuilder.CreateIndex(
                name: "ix_blood_bag_donation_details_batch_processing_id",
                table: "blood_bag_donation_details",
                column: "batch_processing_id");

            migrationBuilder.AddForeignKey(
                name: "fk_blood_bag_donation_details_batch_processing_batch_processin",
                table: "blood_bag_donation_details",
                column: "batch_processing_id",
                principalTable: "batch_processing",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "fk_vacutainers_batch_processing_batch_processing_id",
                table: "vacutainers",
                column: "batch_processing_id",
                principalTable: "batch_processing",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_blood_bag_donation_details_batch_processing_batch_processin",
                table: "blood_bag_donation_details");

            migrationBuilder.DropForeignKey(
                name: "fk_vacutainers_batch_processing_batch_processing_id",
                table: "vacutainers");

            migrationBuilder.DropTable(
                name: "batch_processing");

            migrationBuilder.DropIndex(
                name: "ix_vacutainers_batch_processing_id",
                table: "vacutainers");

            migrationBuilder.DropIndex(
                name: "ix_blood_bag_donation_details_batch_processing_id",
                table: "blood_bag_donation_details");

            migrationBuilder.DropColumn(
                name: "batch_processing_id",
                table: "vacutainers");

            migrationBuilder.DropColumn(
                name: "batch_processing_id",
                table: "blood_bag_donation_details");
        }
    }
}
