﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BMIS.Donation.Migrations
{
    public partial class AddedLocationsToFacility : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "county",
                table: "facilities",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "sub_county",
                table: "facilities",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ward",
                table: "facilities",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "county",
                table: "facilities");

            migrationBuilder.DropColumn(
                name: "sub_county",
                table: "facilities");

            migrationBuilder.DropColumn(
                name: "ward",
                table: "facilities");
        }
    }
}
