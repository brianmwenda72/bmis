﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace BMIS.Donation.Migrations
{
    public partial class InitialCreate0 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "blood_bags_setups",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    entity_guid = table.Column<Guid>(type: "uuid", nullable: false),
                    blood_bag_type = table.Column<string>(type: "text", nullable: true),
                    type_of_donation = table.Column<string>(type: "text", nullable: true),
                    minimum_quantity = table.Column<int>(type: "integer", nullable: false),
                    maximum_quantity = table.Column<int>(type: "integer", nullable: false),
                    quantity_unit = table.Column<int>(type: "integer", nullable: false),
                    created_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    updated_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_blood_bags_setups", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "donors",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    entity_guid = table.Column<Guid>(type: "uuid", nullable: false),
                    name = table.Column<string>(type: "text", nullable: true),
                    donor_number = table.Column<string>(type: "text", nullable: true),
                    gender = table.Column<string>(type: "text", nullable: true),
                    eligible = table.Column<bool>(type: "boolean", nullable: false),
                    is_deffered = table.Column<bool>(type: "boolean", nullable: false),
                    defferment_type = table.Column<string>(type: "text", nullable: true),
                    defferment_reason = table.Column<string>(type: "text", nullable: true),
                    birth_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    created_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    updated_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_donors", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "facilities",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    entity_guid = table.Column<Guid>(type: "uuid", nullable: false),
                    name = table.Column<string>(type: "text", nullable: true),
                    code = table.Column<string>(type: "text", nullable: true),
                    created_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    updated_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_facilities", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "forms",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    entity_guid = table.Column<Guid>(type: "uuid", nullable: false),
                    name = table.Column<string>(type: "text", nullable: true),
                    label = table.Column<string>(type: "text", nullable: true),
                    json_schemas = table.Column<string>(type: "text", nullable: true),
                    created_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    updated_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_forms", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "lookups",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    entity_guid = table.Column<Guid>(type: "uuid", nullable: false),
                    value = table.Column<string>(type: "text", nullable: true),
                    label = table.Column<string>(type: "text", nullable: true),
                    is_active = table.Column<bool>(type: "boolean", nullable: false),
                    parent_id = table.Column<int>(type: "integer", nullable: true),
                    created_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    updated_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_lookups", x => x.id);
                    table.ForeignKey(
                        name: "fk_lookups_lookups_parent_id",
                        column: x => x.parent_id,
                        principalTable: "lookups",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "donation_drives",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    entity_guid = table.Column<Guid>(type: "uuid", nullable: false),
                    site_name = table.Column<string>(type: "text", nullable: true),
                    code = table.Column<string>(type: "text", nullable: true),
                    total_population = table.Column<int>(type: "integer", nullable: false),
                    expected_donors = table.Column<int>(type: "integer", nullable: false),
                    donation_drive_status = table.Column<int>(type: "integer", nullable: false),
                    schedule_start_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    schedule_end_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    facility_id = table.Column<int>(type: "integer", nullable: true),
                    drive_manager = table.Column<string>(type: "text", nullable: true),
                    transport_manager = table.Column<string>(type: "text", nullable: true),
                    created_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    updated_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_donation_drives", x => x.id);
                    table.ForeignKey(
                        name: "fk_donation_drives_facilities_facility_id",
                        column: x => x.facility_id,
                        principalTable: "facilities",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "donation_encounters",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    entity_guid = table.Column<Guid>(type: "uuid", nullable: false),
                    donor_id = table.Column<int>(type: "integer", nullable: true),
                    donation_number = table.Column<string>(type: "text", nullable: true),
                    is_complete = table.Column<bool>(type: "boolean", nullable: false),
                    donation_encounter_status = table.Column<int>(type: "integer", nullable: false),
                    donation_drive_id = table.Column<int>(type: "integer", nullable: true),
                    facility_id = table.Column<int>(type: "integer", nullable: true),
                    eligibility_assessment = table.Column<string>(type: "text", nullable: true),
                    created_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    updated_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_donation_encounters", x => x.id);
                    table.ForeignKey(
                        name: "fk_donation_encounters_donation_drives_donation_drive_id",
                        column: x => x.donation_drive_id,
                        principalTable: "donation_drives",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_donation_encounters_donors_donor_id",
                        column: x => x.donor_id,
                        principalTable: "donors",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_donation_encounters_facilities_facility_id",
                        column: x => x.facility_id,
                        principalTable: "facilities",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "blood_bag_donation_details",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    entity_guid = table.Column<Guid>(type: "uuid", nullable: false),
                    donation_encounter_id = table.Column<int>(type: "integer", nullable: false),
                    donation_method = table.Column<string>(type: "text", nullable: true),
                    donation_number = table.Column<string>(type: "text", nullable: true),
                    blood_bag_type = table.Column<string>(type: "text", nullable: true),
                    blood_bag_label = table.Column<string>(type: "text", nullable: true),
                    blood_bag_donataion_detail_status = table.Column<int>(type: "integer", nullable: false),
                    quantity_collected = table.Column<int>(type: "integer", nullable: false),
                    quantity_unit = table.Column<string>(type: "text", nullable: true),
                    is_sufficient = table.Column<bool>(type: "boolean", nullable: false),
                    weight = table.Column<decimal>(type: "numeric", nullable: false),
                    needle_time_in = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    needle_time_out = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    created_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    updated_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_blood_bag_donation_details", x => x.id);
                    table.ForeignKey(
                        name: "fk_blood_bag_donation_details_donation_encounters_donation_enc",
                        column: x => x.donation_encounter_id,
                        principalTable: "donation_encounters",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "questionnaires",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    entity_guid = table.Column<Guid>(type: "uuid", nullable: false),
                    donation_encounter_id = table.Column<int>(type: "integer", nullable: false),
                    questionnaire_status = table.Column<int>(type: "integer", nullable: false),
                    health_feeling_well_today = table.Column<bool>(type: "boolean", nullable: true),
                    health_eaten_in_the_last_six_hours = table.Column<bool>(type: "boolean", nullable: true),
                    health_ever_fainted = table.Column<bool>(type: "boolean", nullable: true),
                    health_any_treatment_or_medications_in_past6months = table.Column<bool>(type: "boolean", nullable: true),
                    health_any_injections_or_vaccinations_in_past6months = table.Column<bool>(type: "boolean", nullable: true),
                    health_any_blood_or_organ_transfusion_in_the_past12months = table.Column<bool>(type: "boolean", nullable: true),
                    health_any_problems_with_heart_or_lungs = table.Column<bool>(type: "boolean", nullable: true),
                    health_any_bleeding_conditions_or_blood_disease = table.Column<bool>(type: "boolean", nullable: true),
                    health_any_pregnancy_or_breast_feeding_for_female_donors = table.Column<bool>(type: "boolean", nullable: true),
                    health_do_you_have_cancer = table.Column<bool>(type: "boolean", nullable: true),
                    health_do_you_have_diabetis_or_epilepsy_or_tb = table.Column<bool>(type: "boolean", nullable: true),
                    health_any_other_long_term_illness = table.Column<bool>(type: "boolean", nullable: true),
                    health_specify_long_term_illness_if_any = table.Column<string>(type: "text", nullable: true),
                    risk_assessment_have_received_or_given_any_sexual_favours = table.Column<bool>(type: "boolean", nullable: true),
                    risk_assessment_been_raped_or_sodomised = table.Column<bool>(type: "boolean", nullable: true),
                    risk_assessment_have_had_st_ds = table.Column<bool>(type: "boolean", nullable: true),
                    risk_assessment_have_had_reckless_sex = table.Column<bool>(type: "boolean", nullable: true),
                    risk_assessment_have_had_yellow_eyes_or_skin = table.Column<bool>(type: "boolean", nullable: true),
                    risk_assessment_have_used_non_medicinal_drugs = table.Column<bool>(type: "boolean", nullable: true),
                    risk_assessment_do_you_consider_your_blood_safe = table.Column<bool>(type: "boolean", nullable: true),
                    risk_assessment_have_any_sex_besides_regular_partner = table.Column<bool>(type: "boolean", nullable: true),
                    risk_assessment_have_any_stub_wound_or_accidental_needle_stick_injury = table.Column<bool>(type: "boolean", nullable: true),
                    risk_assessment_have_sexual_contact_or_lived_with_someone_with_yellow_ey = table.Column<bool>(type: "boolean", nullable: true),
                    risk_assessment_any_injection_outside_health_facility = table.Column<bool>(type: "boolean", nullable: true),
                    risk_assessment_have_you_and_partner_been_tested_for_hiv = table.Column<bool>(type: "boolean", nullable: true),
                    vitals_weight = table.Column<decimal>(type: "numeric", nullable: true),
                    vitals_height = table.Column<decimal>(type: "numeric", nullable: true),
                    vitals_bmi = table.Column<decimal>(type: "numeric", nullable: true),
                    vitals_temparature = table.Column<decimal>(type: "numeric", nullable: true),
                    vitals_haemoglobin = table.Column<decimal>(type: "numeric", nullable: true),
                    vitals_systolic_bp = table.Column<decimal>(type: "numeric", nullable: true),
                    vitals_dystolic_bp = table.Column<decimal>(type: "numeric", nullable: true),
                    vitals_pulse_rate = table.Column<decimal>(type: "numeric", nullable: true),
                    declaration_declaration_consent_text = table.Column<string>(type: "text", nullable: true),
                    declaration_consented = table.Column<bool>(type: "boolean", nullable: true),
                    created_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    updated_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_questionnaires", x => x.id);
                    table.ForeignKey(
                        name: "fk_questionnaires_donation_encounters_donation_encounter_id",
                        column: x => x.donation_encounter_id,
                        principalTable: "donation_encounters",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "vacutainers",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    entity_guid = table.Column<Guid>(type: "uuid", nullable: false),
                    label = table.Column<string>(type: "text", nullable: true),
                    vacutainer_type = table.Column<string>(type: "text", nullable: true),
                    donation_encounter_id = table.Column<int>(type: "integer", nullable: true),
                    created_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    updated_on_utc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    deleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_vacutainers", x => x.id);
                    table.ForeignKey(
                        name: "fk_vacutainers_donation_encounters_donation_encounter_id",
                        column: x => x.donation_encounter_id,
                        principalTable: "donation_encounters",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "ix_blood_bag_donation_details_donation_encounter_id",
                table: "blood_bag_donation_details",
                column: "donation_encounter_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_blood_bag_donation_details_entity_guid",
                table: "blood_bag_donation_details",
                column: "entity_guid",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_blood_bags_setups_entity_guid",
                table: "blood_bags_setups",
                column: "entity_guid",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_donation_drives_entity_guid",
                table: "donation_drives",
                column: "entity_guid",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_donation_drives_facility_id",
                table: "donation_drives",
                column: "facility_id");

            migrationBuilder.CreateIndex(
                name: "ix_donation_encounters_donation_drive_id",
                table: "donation_encounters",
                column: "donation_drive_id");

            migrationBuilder.CreateIndex(
                name: "ix_donation_encounters_donor_id",
                table: "donation_encounters",
                column: "donor_id");

            migrationBuilder.CreateIndex(
                name: "ix_donation_encounters_entity_guid",
                table: "donation_encounters",
                column: "entity_guid",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_donation_encounters_facility_id",
                table: "donation_encounters",
                column: "facility_id");

            migrationBuilder.CreateIndex(
                name: "ix_donors_entity_guid",
                table: "donors",
                column: "entity_guid",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_facilities_entity_guid",
                table: "facilities",
                column: "entity_guid",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_forms_entity_guid",
                table: "forms",
                column: "entity_guid",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_lookups_entity_guid",
                table: "lookups",
                column: "entity_guid",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_lookups_parent_id",
                table: "lookups",
                column: "parent_id");

            migrationBuilder.CreateIndex(
                name: "ix_questionnaires_donation_encounter_id",
                table: "questionnaires",
                column: "donation_encounter_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_questionnaires_entity_guid",
                table: "questionnaires",
                column: "entity_guid",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_vacutainers_donation_encounter_id",
                table: "vacutainers",
                column: "donation_encounter_id");

            migrationBuilder.CreateIndex(
                name: "ix_vacutainers_entity_guid",
                table: "vacutainers",
                column: "entity_guid",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "blood_bag_donation_details");

            migrationBuilder.DropTable(
                name: "blood_bags_setups");

            migrationBuilder.DropTable(
                name: "forms");

            migrationBuilder.DropTable(
                name: "lookups");

            migrationBuilder.DropTable(
                name: "questionnaires");

            migrationBuilder.DropTable(
                name: "vacutainers");

            migrationBuilder.DropTable(
                name: "donation_encounters");

            migrationBuilder.DropTable(
                name: "donation_drives");

            migrationBuilder.DropTable(
                name: "donors");

            migrationBuilder.DropTable(
                name: "facilities");
        }
    }
}
