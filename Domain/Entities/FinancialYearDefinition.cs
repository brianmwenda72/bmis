﻿using BMIS.Donation.Abstracts;
using System;

namespace BMIS.Donation.Domain.Entities
{
    public class FinancialYearDefinition : AuditableEntity
    {
        public FinancialYearDefinition()
        {
        }
        public Guid EntityGuid { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; } = DateTime.UtcNow;
        public DateTime EndDate { get; set; } = DateTime.UtcNow;
        public bool IsCurrent { get; set; }
    }
}
