﻿using BMIS.Donation.Abstracts;
using Newtonsoft.Json.Linq;
using System;

namespace BMIS.Donation.Domain.Entities
{
    public class Form : AuditableEntity
    {
        public Form()
        {
            this.EntityGuid = Guid.NewGuid();
        }
        public Guid EntityGuid { get; set; }
        public string Name { get; set; }
        public string Label { get; set; }
        public JArray JsonSchemas { get; set; }
    }
}
