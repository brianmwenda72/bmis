﻿using BMIS.Donation.Abstracts;
using Humanizer;
using Humanizer.Localisation;
using System;
using System.Collections.Generic;

namespace BMIS.Donation.Domain.Entities
{
    public class Donor : AuditableEntity
    {
        public Donor()
        {
            this.EntityGuid = Guid.NewGuid();
        }
        public Guid EntityGuid { get; set; }
        public string Name { get; set; }
        public string DonorNumber { get; set; }
        public int PortalKey { get; set; }
        public string Gender { get; set; }
        public DateTime BirthDate { get; set; }
        public int Age => (DateTime.UtcNow - BirthDate).Days / 365;
        public List<DonationEncounter> DonationEncounters { get; set; } = new();
    }
}
