﻿using BMIS.Donation.Abstracts;
using BMIS.Donation.Domain.Enums;
using System;
using System.Collections.Generic;

namespace BMIS.Donation.Domain.Entities
{
    /// <summary>
    /// Represents a donation drive
    /// </summary>
    public class DonationDrive : AuditableEntity
    {
        public Guid EntityGuid { get; set; }
        public string SiteName { get; set; }
        public string ScheduleName { get; set; }
        public string Code { get; set; }
        public int TotalPopulation { get; set; }
        public int ExpectedDonors { get; set; }
        public DonationDriveStatus DonationDriveStatus { get; set; }
        public DateTime ScheduleStartDate { get; set; } = DateTime.UtcNow;
        public DateTime ScheduleEndDate { get; set; } = DateTime.UtcNow;
        public Facility Facility { get; set; }
        public List<DonationEncounter> DonationEncounters { get; set; } = new();
        public string TeamLead { get; set; }
        public List<BatchProcessing> BatchProcessings { get; set; } = new();
    }
}
