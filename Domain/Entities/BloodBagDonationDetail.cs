﻿using BMIS.Donation.Abstracts;
using BMIS.Donation.Domain.Enums;
using System;

namespace BMIS.Donation.Domain.Entities
{
    /// <summary>
    /// Represents a bloodbag
    /// </summary>
    public class BloodBagDonationDetail : AuditableEntity
    {
        public BloodBagDonationDetail()
        {
            this.EntityGuid = Guid.NewGuid();
            this.Sent = false;
            this.Discarded = false;
        }
        public Guid EntityGuid { get; set; }
        public DonationEncounter DonationEncounter { get; set; }
        public string DonationMethod { get; set; }
        public string DonationNumber { get; set; }
        public string BloodBagType { get; set; }
        public string BloodBagLabel { get; set; }
        public BloodBagDonataionDetailStatus BloodBagDonataionDetailStatus { get; set; }
        public int QuantityCollected { get; set; }
        public string QuantityUnit { get; set; }
        public string CollectedBy { get; set; }
        public bool IsSufficient { get; set; }
        public bool Sent { get; set; }
        public bool Discarded { get; set; }
        public decimal Weight { get; set; }
        public string WeightUnit { get; set; }
        public string DiscardReason { get; set; }
        public DateTime NeedleTimeIn { get; set; } = DateTime.UtcNow;
        public DateTime? NeedleTimeOut { get; set; } = DateTime.UtcNow;
        public BatchProcessing BatchProcessing { get; set; }
    }
}
