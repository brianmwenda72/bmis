﻿using BMIS.Donation.Abstracts;
using BMIS.Donation.Domain.Enums;
using System;

namespace BMIS.Donation.Domain.Entities
{
    public class Questionnaire : AuditableEntity
    {
        public Questionnaire()
        {
            this.EntityGuid = Guid.NewGuid();
        }
        public Guid EntityGuid { get; set; }
        public int DonationEncounterId { get; set; }
        public QuestionnaireStatus QuestionnaireStatus { get; set; }
        public Health Health { get; set; }
        public RiskAssessment RiskAssessment { get; set; }
        public Vitals Vitals { get; set; }
        public Declaration Declaration { get; set; }
    }
}
