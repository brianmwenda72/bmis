﻿

using BMIS.Donation.Abstracts;

namespace BMIS.Donation.Domain.Entities
{
    public class HealthFacility : AuditableEntity
    {
        public HealthFacility()
        {
        }
        public string Code { get; set; }
        public string Name { get; set; }
        public string OfficialName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }

}