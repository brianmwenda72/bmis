﻿using BMIS.Donation.Abstracts;
using BMIS.Donation.Domain.Enums;
using System;
using System.Collections.Generic;

namespace BMIS.Donation.Domain.Entities
{
    /// <summary>
    /// Gets or sets the encounter
    /// </summary>
    public class DonationEncounter : AuditableEntity
    {

        public DonationEncounter()
        {
            this.EntityGuid = Guid.NewGuid();
        }
        public Guid EntityGuid { get; set; }
        public Donor Donor { get; set; }
        public string DonationNumber { get; set; }
        public bool IsComplete { get; set; }
        public DonationEncounterStatus DonationEncounterStatus { get; set; }
        public DonationDrive DonationDrive { get; set; }
        public Facility Facility { get; set; }
        public List<BloodBagDonationDetail> BloodBagDonationDetails { get; set; } = new();
        public List<Vacutainer> Vacutainers { get; set; } = new();
        public Questionnaire Questionnaire { get; set; } //= new Questionnaire();
        public string EligibilityAssessment { get; set; }
        public DateTime? CreatedOnUtcDonationQueue { get; set; } = DateTime.UtcNow;
        public DateTime DonationStartTime { get; set; } = DateTime.UtcNow;
        public DateTime DonationStopTime { get; set; } = DateTime.UtcNow;
        public bool Defered { get; set; }
        public string DefermentType { get; set; }
        public string DefermentReason { get; set; }
        public string DefermentNote { get; set; }
        public HealthFacility HealthFacility { get; set; }
    }
}
