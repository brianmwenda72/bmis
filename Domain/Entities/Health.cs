﻿namespace BMIS.Donation.Domain.Entities
{
    public class Health
    {
        public bool FeelingWellToday { get; set; }
        public bool EatenInTheLastSixHours { get; set; }
        public bool EverFainted { get; set; }
        public bool AnyTreatmentOrMedicationsInPast6Months { get; set; }
        public bool AnyInjectionsOrVaccinationsInPast6Months { get; set; }
        public bool AnyBloodOrOrganTransfusionInThePast12Months { get; set; }
        public bool AnyProblemsWithHeartOrLungs { get; set; }
        public bool AnyBleedingConditionsOrBloodDisease { get; set; }
        public bool AnyPregnancyOrBreastFeedingForFemaleDonors { get; set; }
        public bool DoYouHaveCancer { get; set; }
        public bool DoYouHaveDiabetisOrEpilepsyOrTB { get; set; }
        public bool AnyOtherLongTermIllness { get; set; }
        public string SpecifyLongTermIllnessIfAny { get; set; }
    }
}