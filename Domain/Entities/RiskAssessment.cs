﻿namespace BMIS.Donation.Domain.Entities
{
    public class RiskAssessment
    {
        public bool HaveReceivedOrGivenAnySexualFavours { get; set; }
        public bool BeenRapedOrSodomised { get; set; }
        public bool HaveHadSTDs { get; set; }
        public bool HaveHadRecklessSex { get; set; }
        public bool HaveHadYellowEyesOrSkin { get; set; }
        public bool HaveUsedNonMedicinalDrugs { get; set; }
        public bool DoYouConsiderYourBloodSafe { get; set; }
        public bool HaveAnySexBesidesRegularPartner { get; set; }
        public bool HaveAnyStubWoundOrAccidentalNeedleStickInjury { get; set; }
        public bool HaveSexualContactOrLivedWithSomeoneWithYellowEyesOrSkin { get; set; }
        public bool AnyInjectionOutsideHealthFacility { get; set; }
        public bool HaveYouAndPartnerBeenTestedForHIV { get; set; }
    }
}