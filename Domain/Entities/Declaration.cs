﻿using BMIS.Donation.Domain.Defaults;

namespace BMIS.Donation.Domain.Entities
{
    public class Declaration
    {
        public string DonationType { get; set; }
        public string DeclarationConsentText { get; set; }
        public bool Consented { get; set; }
    }
}