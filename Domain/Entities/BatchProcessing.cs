﻿using BMIS.Donation.Abstracts;
using BMIS.Donation.Domain.Enums;
using System;
using System.Collections.Generic;

namespace BMIS.Donation.Domain.Entities
{
    public class BatchProcessing : AuditableEntity
    {
        public BatchProcessing()
        {
            this.EntityGuid = Guid.NewGuid();
            this.Sent = false;
            this.Received = false;
        }
        public Guid EntityGuid { get; set; }
        public string BatchNumber { get; set; }
        public BatchProcessingTypes BatchProcessingTypes { get; set; }
        public bool Sent { get; set; }
        public bool Received { get; set; }
        public string ReceivedBy { get; set; }
        public DateTime ReceiptTime { get; set; } = DateTime.UtcNow;
        public string FacilityToName { get; set; }
        public string TeamLead { get; set; }
        public string DispatchedBy { get; set; }
        public string BatchType { get; set; }
        public List<Vacutainer> Vacutainers { get; set; } = new();
        public List<BloodBagDonationDetail> BloodBagDonationDetails { get; set; } = new();
        public DonationDrive DonationDrive { get; set; }
    }
}
