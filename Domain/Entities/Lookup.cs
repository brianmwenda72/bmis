﻿using BMIS.Donation.Abstracts;
using System;
using System.Collections.Generic;

namespace BMIS.Donation.Domain.Entities
{
    public class Lookup : AuditableEntity
    {
        public Lookup()
        {
            this.EntityGuid = Guid.NewGuid();
            this.IsActive = true;
        }
        public Guid EntityGuid { get; set; }
        public string Value { get; set; }
        public string Label { get; set; }
        public bool IsActive { get; set; }
        public int DisplayOrder { get; set; }
        public Lookup Parent { get; set; }
        public virtual List<Lookup> Children { get; set; } = new List<Lookup>();
    }
}
