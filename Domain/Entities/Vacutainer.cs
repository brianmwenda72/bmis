﻿using BMIS.Donation.Abstracts;
using BMIS.Donation.Domain.Enums;
using System;

namespace BMIS.Donation.Domain.Entities
{
    public class Vacutainer : AuditableEntity
    {
        public Vacutainer()
        {
            this.EntityGuid = Guid.NewGuid();
            this.Sent = false;
            this.Discarded = false;
        }
        public Guid EntityGuid { get; set; }
        public string Label { get; set; }
        public string VacutainerType { get; set; }
        public VacutainerStatus VacutainerStatus { get; set; }
        public bool Sent { get; set; }
        public bool Discarded { get; set; }
        public DonationEncounter DonationEncounter { get; set; }
        public BatchProcessing BatchProcessing { get; set; }
    }
}
