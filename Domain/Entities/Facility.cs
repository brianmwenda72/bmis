﻿using BMIS.Donation.Abstracts;
using System;
using System.Collections.Generic;

namespace BMIS.Donation.Domain.Entities
{
    /// <summary>
    /// Gets or sets a donor site
    /// </summary>
    public class Facility : AuditableEntity
    {
        public Guid EntityGuid { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string County { get; set; }
        public string SubCounty { get; set; }
        public string Ward { get; set; }
        public List<DonationDrive> DonationDrives { get; set; } = new();
        public List<DonationEncounter> DonationEncounters { get; set; } = new();
    }
}
