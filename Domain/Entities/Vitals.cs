﻿namespace BMIS.Donation.Domain.Entities
{
    public class Vitals
    {
        public decimal Weight { get; set; }
        public decimal Height { get; set; }
        public decimal BMI { get; set; }
        public decimal Temparature { get; set; }
        public decimal Haemoglobin { get; set; }
        public decimal SystolicBP { get; set; }
        public decimal DystolicBP { get; set; }
        public decimal PulseRate { get; set; }
    }
}