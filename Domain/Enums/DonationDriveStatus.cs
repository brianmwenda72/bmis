﻿namespace BMIS.Donation.Domain.Enums
{
    public enum DonationDriveStatus
    {
        Active = 1,
        Complete,
    }
}