﻿namespace BMIS.Donation.Domain.Enums
{
    public enum QuestionnaireStatus
    {
        HealthQuestionnaireFilled = 1,
        RiskAssessmentQuestionnaireFilled,
        VitalsQuestionnaireFilled,
        DonorConsented,
    }
}