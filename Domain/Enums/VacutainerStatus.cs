﻿namespace BMIS.Donation.Domain.Enums
{
    public enum VacutainerStatus
    {
        Pending = 1,
        Discarded,
        Complete,
    }
}