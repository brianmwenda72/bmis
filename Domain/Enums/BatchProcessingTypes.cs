﻿namespace BMIS.Donation.Domain.Enums
{
    public enum BatchProcessingTypes
    {
        BloodBag = 1,
        Vacutainer,
        Discards,
    }
}