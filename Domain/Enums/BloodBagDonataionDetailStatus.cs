﻿namespace BMIS.Donation.Domain.Enums
{
    public enum BloodBagDonataionDetailStatus
    {
        Pending = 1,
        IssuedBagAndVacutainers,
        NeedleIn,
        NeedleOut,
        Discarded,
        Complete,
    }
}