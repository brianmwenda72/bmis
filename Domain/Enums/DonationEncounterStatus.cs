﻿namespace BMIS.Donation.Domain.Enums
{
    public enum DonationEncounterStatus
    {
        PendingCounselling = 1,
        CompleteCounselling,
        PendingDonation,
        Donating,
        CompleteDonation,
        Discard,
        DeferredAtCounselling,
        DeferredAtDonation,
    }
}