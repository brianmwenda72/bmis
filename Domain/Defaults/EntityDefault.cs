﻿namespace BMIS.Donation.Domain.Defaults
{
    public class EntityDefault
    {
        //public const string DeclarationConsentText = "I declare that I have answered all the questions truthfully and accurately. I understand that my blood will be tested for HIV, Hepatitis B & C, and Syphilis and the results of my tests may be obtained from the National Blood Transfusion Service. I understand that should any of the screening tests give a reactive result, I will be contacted by use any communication medium(s) to send me important information. Such medium(s) shall include but not limited to e-mail, post office, mobile telephone and/or fixed telephone, and offered counselling to make an informed decision about further confirmatory testing and management.I hereby give consent to KNBTS to use the contact details provided in this form to communicate to me as the need may be. I understand the blood may be used for scientific research, main objective being to improve the safety of the blood supply to patients. I consent to give blood; I understand that it may be used for transfusion for the benefit of others.";

        #region Donation Counselling Section Names
        public const string HealthSection = "Health";
        public const string RiskAssessmentSection = "RiskAssessment";
        public const string VitalsSection = "Vitals";
        public const string DeclarationSection = "Declaration";
        #endregion

        #region Vacutainer Types
        public const string GroupingVacutainer = "purple-top";
        public const string ScreeningVacutainer = "red-top";
        #endregion

        #region Blood Bag Types
        public const string SingleBloodBags = "single-blood-bag";
        public const string DoubleBloodBags = "double-blood-bag";
        public const string TripleBloodBags = "triple-blood-bag";
        public const string QuadrupleBloodBags = "quadruple-blood-bag";
        #endregion

        #region Blood Bag Listing Types
        public const string SingleBags = "single-bags";
        public const string OtherBags = "other-bags";
        #endregion

        #region Gender Types
        public const string MaleGender = "male";
        public const string FemaleGender = "female";
        public const string UnspecifiedGender = "unspecified";
        #endregion

        #region Batch Processing Types
        public const string VacutainerBatch = "vacutainer-batch";
        public const string BloodBagBatch = "blood-bag-batch";
        public const string DiscardBatch = "discard-batch";
        #endregion

        #region Whole Blood Defaults for discards
        public const string WholeBloodCode = "10006";
        public const string WholeBloodName = "Whole Blood";
        #endregion
    }
}
