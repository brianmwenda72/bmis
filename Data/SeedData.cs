﻿using BMIS.Donation.Domain.Entities;
using Humanizer;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace BMIS.Donation.Data
{
    public static class SeedData
    {
        public static async Task Execute(MainContext context, IWebHostEnvironment env)
        {
            await SeedOptions(context, env);
            await SeedOptions2(context, env);
            await SeedForms(context, env);
            await SeedHealthFacilities(context);
        }

        #region Seed Lookups
        private static async Task SeedOptions(MainContext context, IHostEnvironment env)
        {
            if (context.Lookups.Count() >= 0)
            {
                context.RemoveRange(context.Lookups.ToList());
                await context.SaveChangesAsync();

                var files = Directory.GetFiles(env.ContentRootPath + "/Data/LookupData", "*.json");
                foreach (var file in files)
                {
                    if (!context.Lookups.Any(x => x.Value == Path.GetFileNameWithoutExtension(file)))
                    {
                        var jsonModel = JsonConvert.DeserializeObject<List<Lookup>>(await File.ReadAllTextAsync(file));
                        await context.Lookups.AddAsync(new Lookup
                        {
                            Value = Path.GetFileNameWithoutExtension(file),
                            Label = Path.GetFileNameWithoutExtension(file).Humanize(LetterCasing.Title),
                            Children = jsonModel
                        });
                    }
                }
                await context.SaveChangesAsync();
            }
            
        }

        private static async Task SeedOptions2(MainContext context, IHostEnvironment env)
        {
            var files = Directory.GetFiles(env.ContentRootPath + "/Data/LookupData2", "*.json");

            foreach (var file in files)
            {

                if (!context.Lookups.Any(x => x.Value == Path.GetFileNameWithoutExtension(file)))
                {
                    var jsonModel = JsonConvert.DeserializeObject<List<Lookup>>(await File.ReadAllTextAsync(file));

                    jsonModel.ForEach(x =>
                    {
                        x.Children.ForEach(y =>
                        {
                            y.Value = y.Label.ToLower().Trim().Replace(" ", "-");
                            y.Children.ForEach(z =>
                            {
                                z.Value = z.Label.ToLower().Trim().Replace(" ", "-");
                            });
                        });
                    });

                    await context.Lookups.AddAsync(new Lookup
                    {
                        Value = Path.GetFileNameWithoutExtension(file),
                        Label = Path.GetFileNameWithoutExtension(file).Humanize(LetterCasing.Title),
                        Children = jsonModel
                                .Select(x => new Lookup
                                {
                                    Value = x.Value,
                                    Label = x.Label,
                                    Children = x.Children.Select(y => new Lookup
                                    {
                                        Value = y.Value,
                                        Label = y.Label,
                                        Children = y.Children.Select(z => new Lookup { Value = z.Value, Label = z.Label }).ToList()
                                    }).ToList()
                                }).ToList()
                    });

                    await context.SaveChangesAsync();
                }

            }

        }
        #endregion

        #region Seed Forms
        private static async Task SeedForms(MainContext context, IWebHostEnvironment env)
        {
            if (context.Forms.Count() >= 0)
            {
                context.RemoveRange(context.Forms.ToList());
                await context.SaveChangesAsync();

                var files = Directory.GetFiles(env.ContentRootPath + "/Data/Schemas", "*.json");
                foreach (var file in files)
                {
                    if (!context.Forms.Any(x => x.Name == Path.GetFileNameWithoutExtension(file)))
                    {
                        await context.Forms.AddAsync(new Form
                        {
                            Name = Path.GetFileNameWithoutExtension(file),
                            Label = Path.GetFileNameWithoutExtension(file).Humanize(LetterCasing.Title),
                            JsonSchemas = JsonConvert.DeserializeObject<JArray>(await File.ReadAllTextAsync(file))
                        });
                    }
                }
                await context.SaveChangesAsync();
            }
            
        }
        #endregion

        #region Seed Health Facilities
        private static async Task SeedHealthFacilities(MainContext context)
        {
            if (context.HealthFacilities.Count() == 0)
            {
                var options = new JsonSerializerOptions
                {
                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                    WriteIndented = true
                };
                var healthFacilityMasterList = System.Text.Json.JsonSerializer.Deserialize<List<HealthFacility>>(File.ReadAllText("Data/HealthFacilities/facilitymasterlist.json"), options);
                var healthFacilityMasterListString = (File.ReadAllText("Data/HealthFacilities/facilitymasterlist.json"));
                context.AddRange(healthFacilityMasterList);
                await context.SaveChangesAsync();
            }
        }
        #endregion

    }
}