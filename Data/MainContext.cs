﻿using BMIS.Donation.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System.Data;
using System.Reflection;
using System.Threading.Tasks;
using static BMIS.Donation.Features.DonationReports.DonationDailyQuery;
using static BMIS.Donation.Features.DonationReports.DrivesMonthlyQuery;

namespace BMIS.Donation.Data
{
    public class MainContext : DbContext
    {

        private IDbContextTransaction _currentTransaction;

        public MainContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }

        public DbSet<Form> Forms { get; set; }
        public DbSet<Lookup> Lookups { get; set; }
        public DbSet<DonationEncounter> DonationEncounters { get; set; }
        public DbSet<Donor> Donors { get; set; }
        public DbSet<Facility> Facilities { get; set; }
        public DbSet<DonationDrive> DonationDrives { get; set; }
        public DbSet<BloodBagDonationDetail> BloodBagDonationDetails { get; set; }
        public DbSet<Questionnaire> Questionnaires { get; set; }
        public DbSet<Vacutainer> Vacutainers { get; set; }
        public DbSet<BatchProcessing> BatchProcessings { get; set; }
        public DbSet<DriveQueryOutput> DriveQueryOutputs { get; set; }
        public DbSet<DailyDonationQueryOutput> DailyDonationQueryOutputs { get; set; }
        public DbSet<HealthFacility> HealthFacilities { get; set; }
        public DbSet<FinancialYearDefinition> FinancialYearDefinitions { get; set; }

        public async Task BeginTransactionAsync()
        {
            if (_currentTransaction != null)
            {
                return;
            }

            _currentTransaction = await Database.BeginTransactionAsync(IsolationLevel.ReadCommitted).ConfigureAwait(false);
        }

        public async Task CommitTransactionAsync()
        {
            try
            {
                await SaveChangesAsync().ConfigureAwait(false);

                _currentTransaction?.Commit();
            }
            catch
            {
                RollbackTransaction();
                throw;
            }
            finally
            {
                if (_currentTransaction != null)
                {
                    _currentTransaction.Dispose();
                    _currentTransaction = null;
                }
            }
        }

        public void RollbackTransaction()
        {
            try
            {
                _currentTransaction?.Rollback();
            }
            finally
            {
                if (_currentTransaction != null)
                {
                    _currentTransaction.Dispose();
                    _currentTransaction = null;
                }
            }
        }
    }
}