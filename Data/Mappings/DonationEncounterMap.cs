﻿using BMIS.Donation.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BMIS.Donation.Data.Mappings
{
    public class DonationEncounterMap : IEntityTypeConfiguration<DonationEncounter>
    {
        public void Configure(EntityTypeBuilder<DonationEncounter> builder)
        {
            builder.HasKey(donationEncounter => donationEncounter.Id);
            builder.HasIndex(donationEncounter => donationEncounter.EntityGuid).IsUnique();
        }
    }
}
