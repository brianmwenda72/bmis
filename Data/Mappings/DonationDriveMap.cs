﻿using BMIS.Donation.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BMIS.Donation.Data.Mappings
{
    public class DonationDriveMap : IEntityTypeConfiguration<DonationDrive>
    {
        public void Configure(EntityTypeBuilder<DonationDrive> builder)
        {
            builder.HasKey(donationDrive => donationDrive.Id);
            builder.HasIndex(donationDrive => donationDrive.EntityGuid).IsUnique();
        }
    }
}
