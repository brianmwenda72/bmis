﻿using BMIS.Donation.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BMIS.Donation.Data.Mappings
{
    public class BloodBagDonationDetailMap : IEntityTypeConfiguration<BloodBagDonationDetail>
    {
        public void Configure(EntityTypeBuilder<BloodBagDonationDetail> builder)
        {
            builder.HasKey(bloodBagDonationDetail => bloodBagDonationDetail.Id);
            builder.HasIndex(bloodBagDonationDetail => bloodBagDonationDetail.EntityGuid).IsUnique();
        }
    }
}
