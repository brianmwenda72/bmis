﻿using BMIS.Donation.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BMIS.Donation.Data.Mappings
{
    public class FinancialYearDefinitionMap : IEntityTypeConfiguration<FinancialYearDefinition>
    {
        public void Configure(EntityTypeBuilder<FinancialYearDefinition> builder)
        {
            builder.HasKey(facility => facility.Id);
            builder.HasIndex(facility => facility.EntityGuid).IsUnique();
        }
    }
}
