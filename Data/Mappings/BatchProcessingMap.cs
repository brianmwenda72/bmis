﻿using BMIS.Donation.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BMIS.Donation.Data.Mappings
{
    public class BatchProcessingMap : IEntityTypeConfiguration<BatchProcessing>
    {
        public void Configure(EntityTypeBuilder<BatchProcessing> builder)
        {
            builder.HasKey(batchProcessing => batchProcessing.Id);
            builder.HasIndex(batchProcessing => batchProcessing.EntityGuid).IsUnique();
        }
    }
}
