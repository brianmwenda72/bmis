﻿using BMIS.Donation.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BMIS.Donation.Data.Mappings
{
    public class QuestionnaireMap : IEntityTypeConfiguration<Questionnaire>
    {
        public void Configure(EntityTypeBuilder<Questionnaire> builder)
        {
            builder.HasKey(questionnaire => questionnaire.Id);
            builder.HasIndex(questionnaire => questionnaire.EntityGuid).IsUnique();
            builder.OwnsOne(questionnaire => questionnaire.Declaration);
            builder.OwnsOne(questionnaire => questionnaire.Health);
            builder.OwnsOne(questionnaire => questionnaire.RiskAssessment);
            builder.OwnsOne(questionnaire => questionnaire.Vitals);
        }
    }
}
