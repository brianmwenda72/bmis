﻿using BMIS.Donation.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BMIS.Donation.Data.Mappings
{
    public class VacutainerMap : IEntityTypeConfiguration<Vacutainer>
    {
        public void Configure(EntityTypeBuilder<Vacutainer> builder)
        {
            builder.HasKey(vacutainer => vacutainer.Id);
            builder.HasIndex(vacutainer => vacutainer.EntityGuid).IsUnique();
        }
    }
}
