﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using DHP.Contracts.Commands;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Linq;
using System.Threading.Tasks;

namespace BMIS.Donation.Consumers
{
    public class FacilityCreatedConsumer : IConsumer<CreateOrUpdateFacilityCommand>
    {
        private readonly MainContext db;
        private readonly IMapper mapper;
        public FacilityCreatedConsumer(MainContext db, IMapper mapper)

        {
            this.mapper = mapper;
            this.db = db;

        }
        public async Task Consume(ConsumeContext<CreateOrUpdateFacilityCommand> context)
        {
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
            LocationModel locationModel = JsonConvert.DeserializeObject<LocationModel>(context.Message.Location, settings);

            Lookup countyLookup = db.Lookups
                .FirstOrDefault(x => x.Value.ToLower() == locationModel.County.ToLower());

            Lookup subCountyLookup = db.Lookups
                .FirstOrDefault(x => x.Value.ToLower() == locationModel.SubCounty.ToLower());

            Lookup wardLookup = db.Lookups
                .FirstOrDefault(x => x.Value.ToLower() == locationModel.Ward.ToLower());

            var facility = await db.Facilities
                .FirstOrDefaultAsync(x => x.EntityGuid == context.Message.EntityGuid);
            if (facility is null)
            {
                await db.Facilities.AddAsync(new Facility
                {
                    EntityGuid = context.Message.EntityGuid,
                    Name = context.Message.Name,
                    Code = context.Message.Code,
                    County = countyLookup.Label,
                    SubCounty = subCountyLookup.Label,
                    Ward = wardLookup.Label
                });

                await db.SaveChangesAsync();
            }
            else
            {
                facility.Name = context.Message.Name;
                facility.Code = context.Message.Code;
                facility.County = countyLookup.Label;
                facility.SubCounty = subCountyLookup.Label;
                facility.Ward = wardLookup.Label;

                db.Facilities.Update(facility);
                await db.SaveChangesAsync();
            }
        }
    }
}
