﻿using MassTransit;
using System.Threading.Tasks;
using DHP.Contracts.Commands;
using System.Linq;
using System;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Domain.Enums;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;

namespace BMIS.Donation.Consumers
{
    public class QueueClientConsumer : IConsumer<QueueClient>
    {
        private readonly MainContext db;
        private readonly ILogger<QueueClientConsumer> logger;

        public QueueClientConsumer(MainContext db, ILogger<QueueClientConsumer> logger)
        {
            this.db = db;
            this.logger = logger;
        }

        public async Task Consume(ConsumeContext<QueueClient> context)
        {
            Facility facility = db.Facilities
                .SingleOrDefault(x=> x.Code == context.Message.FacilityCode);

            DonationDrive donationDrive = db.DonationDrives
                .FirstOrDefault(x=> x.EntityGuid == context.Message.DonationDriveGuid);

            if (donationDrive != null)
            {
                Donor donor = db.Donors
                .Where(client => client.DonorNumber == context.Message.ClientNumber)
                .FirstOrDefault();

                if (donor is null)
                {
                    var newDonor = new Donor
                    {
                        DonorNumber = context.Message.ClientNumber,
                        PortalKey = context.Message.BmisPortalKey,
                        Name = context.Message.ClientName,
                        Gender = context.Message.Gender,
                        BirthDate = context.Message.DateOfBirth.ToUniversalTime()
                    };
                    db.Donors.Add(newDonor);
                    await db.SaveChangesAsync();

                    DonationEncounter donationEncounter = new DonationEncounter()
                    {
                        Donor = newDonor,
                        DonationDrive = donationDrive,
                        Facility = facility,
                        CreatedOnUtc = DateTime.UtcNow,
                        DonationEncounterStatus = DonationEncounterStatus.PendingCounselling,
                    };
                    db.DonationEncounters.Add(donationEncounter);
                    await db.SaveChangesAsync();

                }
                else
                {
                    donor.Name = context.Message.ClientName;
                    donor.PortalKey = context.Message.BmisPortalKey;
                    donor.Gender = context.Message.Gender;
                    donor.BirthDate = context.Message.DateOfBirth.ToUniversalTime();
                    db.Donors.Update(donor);
                    await db.SaveChangesAsync();

                    var donationEncounter = db.DonationEncounters.Include(x => x.Donor).Any(x => x.Donor == donor && x.IsComplete == false);

                    if (!donationEncounter)
                    {
                        DonationEncounter newDonationEncounter = new DonationEncounter()
                        {
                            Donor = donor,
                            DonationDrive = donationDrive,
                            Facility = facility,
                            CreatedOnUtc = DateTime.UtcNow,
                            DonationEncounterStatus = DonationEncounterStatus.PendingCounselling,
                        };
                        db.DonationEncounters.Add(newDonationEncounter);
                        await db.SaveChangesAsync();
                    }
                    else
                    {
                        var defferedEncounterList = db.DonationEncounters
                            .Where(x => x.Donor == donor && x.DonationEncounterStatus == DonationEncounterStatus.DeferredAtCounselling || x.DonationEncounterStatus == DonationEncounterStatus.DeferredAtDonation)
                            .OrderBy(x=> x.Id)
                            .ToList();
                        var recentEncounter = defferedEncounterList.Last();
                        if (recentEncounter.DefermentType == "temporary")
                        {
                            DonationEncounter newDonationEncounter = new DonationEncounter()
                            {
                                Donor = donor,
                                DonationDrive = donationDrive,
                                Facility = facility,
                                CreatedOnUtc = DateTime.UtcNow,
                                DonationEncounterStatus = DonationEncounterStatus.PendingCounselling,
                            };
                            db.DonationEncounters.Add(newDonationEncounter);
                            await db.SaveChangesAsync();
                        }

                    }
                }
            }
            else
            {
                logger.LogInformation("No drive found");
            }
        }
    }
}
