﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using DHP.Contracts.Commands;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace BMIS.Donation.Consumers
{
    public class FinancialYearDefinitionConsumer : IConsumer<CreateOrUpdateFinancialYearDefinitionCommand>
    {
        private readonly MainContext db;
        private readonly IMapper mapper;
        public FinancialYearDefinitionConsumer(MainContext db, IMapper mapper)

        {
            this.mapper = mapper;
            this.db = db;

        }
        public async Task Consume(ConsumeContext<CreateOrUpdateFinancialYearDefinitionCommand> context)
        {
            var financialYearDefinition = await db.FinancialYearDefinitions
                .FirstOrDefaultAsync(x => x.EntityGuid == context.Message.FinancialYearDefinitionData["entityGuid"].ToObject<Guid>());
            if (financialYearDefinition is null)
            {
                var newFyDefinition = new FinancialYearDefinition()
                {
                    EntityGuid = context.Message.FinancialYearDefinitionData["entityGuid"].ToObject<Guid>(),
                    Name = context.Message.FinancialYearDefinitionData["name"].ToObject<string>(),
                    StartDate = context.Message.FinancialYearDefinitionData["startDate"].ToObject<DateTime>(),
                    EndDate = context.Message.FinancialYearDefinitionData["endDate"].ToObject<DateTime>(),
                    IsCurrent = context.Message.FinancialYearDefinitionData["isCurrent"].ToObject<bool>(),
                    CreatedOnUtc = DateTime.UtcNow


                };
                db.FinancialYearDefinitions.Add(newFyDefinition);
                await db.SaveChangesAsync();
            }
            else
            {
                if (context.Message.FinancialYearDefinitionData["deleted"].ToObject<bool>() == true)
                {
                    db.FinancialYearDefinitions.Remove(financialYearDefinition);
                    await db.SaveChangesAsync();
                }

                financialYearDefinition.EntityGuid = context.Message.FinancialYearDefinitionData["entityGuid"].ToObject<Guid>();
                financialYearDefinition.Name = context.Message.FinancialYearDefinitionData["name"].ToObject<string>();
                financialYearDefinition.StartDate = context.Message.FinancialYearDefinitionData["startDate"].ToObject<DateTime>();
                financialYearDefinition.EndDate = context.Message.FinancialYearDefinitionData["endDate"].ToObject<DateTime>();
                financialYearDefinition.IsCurrent = context.Message.FinancialYearDefinitionData["isCurrent"].ToObject<bool>();

                db.FinancialYearDefinitions.Update(financialYearDefinition);
                await db.SaveChangesAsync();
            }
        }
    }
}
