﻿using System;

namespace BMIS.Donation.Consumers
{
    public class LocationModel
    {
        public string County { get; set; }
        public string SubCounty { get; set; }
        public string Ward { get; set; }
    }
}
