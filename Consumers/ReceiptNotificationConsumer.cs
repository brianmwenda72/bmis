﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using DHP.Contracts.Events.ReceiptNotifications;
using MassTransit;
using System.Linq;
using System.Threading.Tasks;

namespace BMIS.Donation.Consumers
{
    public class ReceiptNotificationConsumer : IConsumer<ReceiptNotificationContract>
    {
        private readonly IMapper mapper;
        private readonly MainContext db;

        public ReceiptNotificationConsumer(IMapper mapper, MainContext db)
        {
            this.mapper = mapper;
            this.db = db;
        }

        public async Task Consume(ConsumeContext<ReceiptNotificationContract> context)
        {
            BatchProcessing batch = db.BatchProcessings
                .Where(x => x.BatchNumber == context.Message.BatchNumber)
                .SingleOrDefault();

            if (batch != null)
            {
                batch.Received = context.Message.Received;
                batch.ReceivedBy = context.Message.ReceivedBy;
                batch.ReceiptTime = context.Message.ReceiptTime;

                db.BatchProcessings.Update(batch);
                await db.SaveChangesAsync();
            }
        }
    }
}
