﻿using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Domain.Enums;
using DHP.Contracts.Commands;
using MassTransit;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BMIS.Donation.Consumers
{
    public class DonationDriveConsumer : IConsumer<CreateOrUpdateDonationDriveCommand>
    {

        private readonly MainContext db;
        private readonly ILogger<DonationDriveConsumer> logger;

        public DonationDriveConsumer(MainContext db, ILogger<DonationDriveConsumer> logger)
        {
            this.db = db;
            this.logger = logger;
        }

        public async Task Consume(ConsumeContext<CreateOrUpdateDonationDriveCommand> context)
        {
            Facility facility = db.Facilities
                .FirstOrDefault(x=> x.Code == context.Message.FacilityCode);
            if (facility != null)
            {
                var bloodDrive = db.DonationDrives
                .SingleOrDefault(x => x.EntityGuid == context.Message.EntityGuid);

                if (bloodDrive is null)
                {
                    DonationDrive newDonationDrive = new()
                    {
                        Code = context.Message.Code,
                        CreatedOnUtc = DateTime.UtcNow,
                        EntityGuid = context.Message.EntityGuid,
                        ExpectedDonors = context.Message.ExpectedDonors,
                        Facility = facility,
                        ScheduleStartDate = context.Message.ScheduleStartDate,
                        ScheduleEndDate = context.Message.ScheduleEndDate,
                        ScheduleName = context.Message.ScheduleName,
                        SiteName = context.Message.SiteName,
                        DonationDriveStatus = DonationDriveStatus.Active,
                        TeamLead = context.Message.TeamLead,
                        TotalPopulation = context.Message.TotalPopulation
                    };

                    await db.DonationDrives.AddAsync(newDonationDrive);
                    await db.SaveChangesAsync();
                    logger.LogInformation("Drive consumed");
                }
                else
                {
                    bloodDrive.ExpectedDonors = context.Message.ExpectedDonors;
                    bloodDrive.ScheduleStartDate = context.Message.ScheduleStartDate;
                    bloodDrive.ScheduleEndDate = context.Message.ScheduleEndDate;
                    bloodDrive.ScheduleName = context.Message.ScheduleName;
                    bloodDrive.DonationDriveStatus = DonationDriveStatus.Active;
                    bloodDrive.TotalPopulation = context.Message.TotalPopulation;

                    db.DonationDrives.Update(bloodDrive);
                    await db.SaveChangesAsync();
                }
            }
            else
            {
                logger.LogInformation("No Facility found with this code");
            }
        }
    }
}
