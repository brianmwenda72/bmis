﻿using DHP.Contracts;
using BMIS.Donation.Consumers;
using FluentValidation;
using MassTransit;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using BMIS.Donation.Common.Behaviours;
using GreenPipes;

namespace BMIS.Donation.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddCommonServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAutoMapper(typeof(Startup));
            services.AddValidatorsFromAssemblyContaining(typeof(Startup));
            services.AddMediatR(typeof(Startup));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehaviour<,>));
            services.AddMassTransitHostedService();
            IConfigurationSection rabbitMqConfigurationSection = configuration.GetSection("Rabbitmq");

            services.AddSwaggerDocument(config =>
            {
                config.PostProcess = document =>
                {
                    document.Info.Version = "v1";
                    document.Info.Title = "BMIS Donation Microservice";
                    document.Info.Description = "Manages the Donation Domain for BMIS";
                };
            });

            //services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));

            services.AddMassTransit(x =>
            {
                x.AddConsumer<QueueClientConsumer>();
                x.AddConsumer<FacilityCreatedConsumer>();
                x.AddConsumer<FinancialYearDefinitionConsumer>();
                x.AddConsumer<DonationDriveConsumer>();
                x.AddConsumer<ReceiptNotificationConsumer>();

                x.AddBus(provider => Bus.Factory.CreateUsingRabbitMq(config =>
                {
                    config.Host(new Uri(rabbitMqConfigurationSection["Host"] + configuration["Rabbitmq:VirtualHost"]), h =>
                    {
                        h.Username(rabbitMqConfigurationSection["Username"]);
                        h.Password(rabbitMqConfigurationSection["Password"]);
                    });
                    config.ReceiveEndpoint(ep =>
                    {
                        ep.PrefetchCount = 16;
                        ep.UseMessageRetry(r => r.Interval(2, 100));
                    });
                    config.ReceiveEndpoint(QueueDefinitions.BMIS.DONATION_CREATE_OR_UPDATE_FACILITY_QUEUE, ep =>
                    {
                        ep.PrefetchCount = 1;
                        ep.UseMessageRetry(r => r.Interval(2, 100));
                        ep.ConfigureConsumer<FacilityCreatedConsumer>(provider);
                    });
                    config.ReceiveEndpoint(QueueDefinitions.BMIS.DONATION_FINANCIAL_YEAR_DEFINITION_QUEUE, ep =>
                    {
                        ep.PrefetchCount = 1;
                        ep.UseMessageRetry(r => r.Interval(2, 100));
                        ep.ConfigureConsumer<FinancialYearDefinitionConsumer>(provider);
                    });
                    config.ReceiveEndpoint(QueueDefinitions.BMIS.DONATION_CREATE_OR_UPDATE_DONATION_DRIVE_QUEUE, ep =>
                    {
                        ep.PrefetchCount = 1;
                        ep.UseMessageRetry(r => r.Interval(2, 100));
                        ep.ConfigureConsumer<DonationDriveConsumer>(provider);
                    });
                    config.ReceiveEndpoint(QueueDefinitions.BMIS.DONATION_CLIENT_QUEUE, ep =>
                    {
                        ep.PrefetchCount = 1;
                        ep.UseMessageRetry(r => r.Interval(2, 100));
                        ep.ConfigureConsumer<QueueClientConsumer>(provider);
                    });
                    config.ReceiveEndpoint(QueueDefinitions.BMIS.SCREENING_RECEIPT_NOTIFICATION_QUEUE, ep =>
                    {
                        ep.PrefetchCount = 1;
                        ep.UseMessageRetry(r => r.Interval(2, 100));
                        ep.ConfigureConsumer<ReceiptNotificationConsumer>(provider);
                    });
                    config.ReceiveEndpoint(QueueDefinitions.BMIS.COMPONENTS_SEPARATION_RECEIPT_NOTIFICATION_QUEUE, ep =>
                    {
                        ep.PrefetchCount = 1;
                        ep.UseMessageRetry(r => r.Interval(2, 100));
                        ep.ConfigureConsumer<ReceiptNotificationConsumer>(provider);
                    });
                }));
            });

            return services;
        }


    }
}
