﻿using AutoMapper;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Models.Forms;

namespace BMIS.Donation.Features.Forms
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Form, FormViewModel>();
        }
    }
}
