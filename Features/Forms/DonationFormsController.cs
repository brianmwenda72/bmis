using BMIS.Donation.Abstracts;
using BMIS.Donation.Models.Forms;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.Forms
{
    public class DonationFormsController : BaseApiController
    {
        [HttpGet]
        public async Task<IActionResult> GetForms(int? page, string q) => Process(await Mediator.Send(new List.Query(page, q)));

        [HttpPost]
        public async Task<IActionResult> CreateForms(CreateOrUpdateFormViewModel createFormViewModel) => Feedback(await Mediator.Send(new Create.Command(createFormViewModel)), ActionPerformed.Add);

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateForm(int id, CreateOrUpdateFormViewModel updateFormViewModel) => Feedback(await Mediator.Send(new Update.Command(updateFormViewModel, id)), ActionPerformed.Update);

        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetFormById(int id) => Process(await Mediator.Send(new GetFormById.Query(id)));

        [HttpGet("{name}")]
        public async Task<IActionResult> GetFormByName(string name) => Process(await Mediator.Send(new GetFormByName.Query(name)));

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteForm(int id) => Feedback(await Mediator.Send(new Delete.Command(id)), "Record deleted successfully");
    }
}