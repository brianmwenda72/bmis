﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Models.Forms;
using CSharpFunctionalExtensions;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.Forms
{
    public class GetFormById
    {
        public class Query : IRequest<Result<FormViewModel>>
        {
            public Query(int id)
            {
                Id = id;
            }

            public int Id { get; }
        }

        public class QueryHandler : IRequestHandler<Query, Result<FormViewModel>>
        {
            private readonly IMapper mapper;
            private readonly MainContext context;

            public QueryHandler(IMapper mapper, MainContext context)
            {
                this.mapper = mapper;
                this.context=context;
            }

            public async Task<Result<FormViewModel>> Handle(Query request, CancellationToken cancellationToken)
            {
                var currentForm = await context.Forms.FindAsync(request.Id);

                if (currentForm is not null)
                {
                    var formViewModel = mapper.Map<FormViewModel>(currentForm);

                    return Result.Success(formViewModel);
                }

                return Result.Failure<FormViewModel>("Form not found");
            }
        }
    }
}