﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Models.Forms;
using CSharpFunctionalExtensions;
using Humanizer;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.Forms
{
    public class Create
    {
        public class Command : IRequest<Result>
        {
            public Command(CreateOrUpdateFormViewModel createFormViewModel)
            {
                CreateFormViewModel = createFormViewModel;
            }

            public CreateOrUpdateFormViewModel CreateFormViewModel { get; }
        }

        public class CommandHandler : IRequestHandler<Command, Result>
        {
            private readonly IMapper mapper;
            private readonly MainContext context;

            public CommandHandler(IMapper mapper, MainContext context)
            {
                this.mapper = mapper;
                this.context = context;
            }

            public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
            {
                await context.Forms.AddAsync(new Form
                {
                    JsonSchemas = request.CreateFormViewModel.JsonSchemas,
                    Label = request.CreateFormViewModel.Label,
                    Name = request.CreateFormViewModel.Label.Trim().Humanize(LetterCasing.LowerCase).Replace(" ", "-")
                }, cancellationToken);
                await context.SaveChangesAsync(cancellationToken);
                return Result.Success();
            }
        }
    }
}
