﻿using AutoMapper;
using BMIS.Donation.Data;
using CSharpFunctionalExtensions;
using MediatR;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.Forms
{
    public class GetFormByName
    {
        public class Query : IRequest<Result<JArray>>
        {
            public Query(string name)
            {
                Name = name;
            }
            [IgnoreDataMember]
            public string Name { get; }
        }
       

        public class QueryHandler : IRequestHandler<Query, Result<JArray>>
        {
            private readonly MainContext context;
            private readonly IMapper mapper;
            private readonly IConfigurationProvider provider;

            public QueryHandler(MainContext context, IMapper mapper, IConfigurationProvider provider)
            {
                this.context=context;
                this.mapper = mapper;
                this.provider = provider;
            }

            public Task<Result<JArray>> Handle(Query request, CancellationToken cancellationToken)
            {
                var currentForm = context.Forms
                  .FirstOrDefault(form => form.Name == request.Name);

                return Task.FromResult(currentForm is not null ? Result.Success(currentForm.JsonSchemas) : Result.Failure<JArray>("Form not found"));
            }
        }
    }
}