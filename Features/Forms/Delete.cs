﻿using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using CSharpFunctionalExtensions;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.Forms
{
    public class Delete
    {
        public class Command : IRequest<Result>
        {
            public Command(int id)
            {
                Id = id;
            }

            public int Id { get; }
        }

        public class CommandHanlder : IRequestHandler<Command, Result>
        {
            private readonly MainContext context;

            public CommandHanlder(MainContext context)
            {
                this.context=context;
            }

            public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
            {
                Form currentForm = await context.Forms.FindAsync(request.Id);

                context.Forms.Remove(currentForm);
                await context.SaveChangesAsync();

                return Result.Success();
            }
        }
    }
}
