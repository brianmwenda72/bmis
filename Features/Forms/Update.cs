﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Models.Forms;
using CSharpFunctionalExtensions;
using Humanizer;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.Forms
{
    public class Update
    {
        public class Command : IRequest<Result>
        {
            public Command(CreateOrUpdateFormViewModel updateFormViewModel, int id)
            {
                UpdateFormViewModel = updateFormViewModel;
                Id = id;
            }

            public CreateOrUpdateFormViewModel UpdateFormViewModel { get; }
            public int Id { get; }
        }

        public class CommandHandler : IRequestHandler<Command, Result>
        {
            private readonly IMapper mapper;
            private readonly MainContext context;

            public CommandHandler(IMapper mapper, MainContext context)
            {
                this.mapper = mapper;
                this.context=context;
            }

            public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
            {
                var record = await context.Forms.FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
                if (record is null)
                {
                    return Result.Failure("Form not found");
                }

                record.JsonSchemas = request.UpdateFormViewModel.JsonSchemas;
                record.Label = request.UpdateFormViewModel.Label;
                record.Name = request.UpdateFormViewModel.Label.Trim().Humanize(LetterCasing.LowerCase).Replace(" ", "-");
                context.Update(record);
                await context.SaveChangesAsync(cancellationToken);

                return Result.Success();
            }
        }
    }
}