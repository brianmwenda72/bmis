﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Extensions.Paging;
using BMIS.Donation.Models.Forms;
using CSharpFunctionalExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.Forms
{
    public class List
    {
        public class Query : IRequest<Result<PagedResult<FormViewModel>>>
        {
            public Query(int? page, string s)
            {
                Page = page;
                Q = s;
            }

            public int? Page { get; set; }
            public string Q { get; set; }
        }

        public class QueryHandler : IRequestHandler<Query, Result<PagedResult<FormViewModel>>>
        {
            private readonly IConfigurationProvider provider;
            private readonly MainContext context;

            public QueryHandler(IConfigurationProvider provider, MainContext context)
            {
                this.provider = provider;
                this.context = context;
            }

            public async Task<Result<PagedResult<FormViewModel>>> Handle(Query request, CancellationToken cancellationToken)
            {
                var pageNumber = request.Page ?? 1;
                var q = request.Q?.ToLower();
                var forms = context.Forms
                    .AsNoTracking();
                var results = !string.IsNullOrWhiteSpace(q)
                        ? forms
                            .Where(x => EF.Functions.Like(x.Label.ToLower(), $"%{q}%"))
                            .OrderBy(x => x.Label)
                        : forms.OrderBy(x => x.Label);
                return Result.Success(await results.GetPagedAsync<Form, FormViewModel>(provider, pageNumber));
            }
        }
    }
}