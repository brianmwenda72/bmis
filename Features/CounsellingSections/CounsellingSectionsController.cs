﻿using BMIS.Donation.Abstracts;
using BMIS.Donation.Models.DonationEncounters.DonationEncounterAtCounselling;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.CounsellingSections
{
    public class CounsellingSectionsController : BaseApiController
    {
        #region Active Donation Drive List within a Facility
        [HttpGet("donation-drive-list")]
        public async Task<IActionResult> GetDonationDriveList(int? page, string q, string facilityCode)
        {
            return Process(await Mediator.Send(new DonationDriveList.Query(page, q, facilityCode)));
        }
        #endregion

        #region Eligibility Routes

        [HttpPost("{donationEncounterId}/eligibility")]
        public async Task<IActionResult> CalculateEligibilityAsync(int donationEncounterId, string facilityCode, CalculateEligibility.Command command)
        {
            command.DonationEncounterId = donationEncounterId;
            command.FacilityCode = facilityCode;
            return Feedback(await Mediator.Send(command),"The donor is eligible to donate. Proceed to donation");
        }

        #endregion

        #region Pending Counselling Donation Encounter List With Donors
        [HttpGet("{donationDriveGuid}/pending-counselling-donation-encounter-list")]
        public async Task<IActionResult> GetDonationEncounterList(int? page, string q, Guid donationDriveGuid, string facilityCode) => Process(await Mediator.Send(new DonationEncounterList.Query(page, q, donationDriveGuid, facilityCode)));
        #endregion

        #region Deferment Routes

        [HttpPost("{donationEncounterId}/defer")]
        public async Task<IActionResult> DeferDonationAtCounsellingAsync(int donationEncounterId, string facilityCode, DeferDonationAtCounselling.Command command)
        {
            command.DonationEncounterId = donationEncounterId;
            command.FacilityCode = facilityCode;
            return Feedback(await Mediator.Send(command), ActionPerformed.Add);
        }

        #endregion

        #region Completing Donation Counselling and Adding a Donation Encounter to a Donation Drive
        [HttpPost("{donationEncounterId}/complete-counselling-and-link-donation-encounter-to-donation-drive")]
        public async Task<IActionResult> LinkDonationEncounterToDonationDriveAsync(int donationEncounterId, SendDonorToDonation.Command command)
        {
            command.DonationEncounterId = donationEncounterId;
            return Feedback(await Mediator.Send(command), ActionPerformed.Add);
        }
        #endregion

        #region Donor Details
        [HttpGet("{donationEncounterId}/donor-details")]
        public async Task<IActionResult> GetDonorByIdAsync(int donationEncounterId, string facilityCode)
        {
            return Process<DonorModel>(await Mediator.Send(new DonorDetails.Query(donationEncounterId, facilityCode)));
        }
        #endregion

        #region Create or Update Donation Counselling Sections
        [HttpPost("{donationEncounterId}/{sectionName}/create-or-update")]
        public async Task<IActionResult> CreateOrUpdateAsync(int donationEncounterId, string facilityCode, string sectionName, CreateOrUpdate.Command command)
        {
            command.DonationEncounterId = donationEncounterId;
            command.FacilityCode = facilityCode;
            command.SectionName = sectionName;
            return Feedback(await Mediator.Send(command), ActionPerformed.Add);
        }
        #endregion

        #region Get Details of the Donation Counselling Sections
        [HttpGet("{donationEncounterId}/details")]
        public async Task<IActionResult> GetQuestionnaireByIdAsync(int donationEncounterId, string facilityCode)
        {
            return Process<Details.QuestionnaireViewModel>(await Mediator.Send(new Details.Query(donationEncounterId, facilityCode)));
        }
        #endregion

        #region Get Health Facility by name
        [HttpGet("{officialName}")]
        public async Task<IActionResult> GetFacilityName(string officialName) => Process(await Mediator.Send(new GetHealthFacilityByName.Query(officialName)));
        #endregion

        #region Donors List
        [HttpGet("donors-list")]
        public async Task<IActionResult> GetDonorsList(int? page, string q) => Process(await Mediator.Send(new DonorsHistory.Query(page, q)));
        #endregion

        #region Donors Encounter History List
        [HttpGet("previous-donors-donation-encounter-list")]
        public async Task<IActionResult> GetDonationEncounterHistoryList(int? page, string q, string donorNumber, string facilityCode) => Process(await Mediator.Send(new DonationsHistory.Query(page, q, donorNumber,facilityCode)));
        #endregion

        #region Donation Encounter List with donor and drive details
        [HttpGet("donation-encounter-list-with-donor-and-drive-details")]
        public async Task<IActionResult> GetDonationEncounterDriveList(int? page, string q, string facilityCode) => Process(await Mediator.Send(new DonorListWithActualEncounter.Query(page, q, facilityCode)));
        #endregion

        #region Reverse Donation Encounter to its correct Drive
        [HttpPost("{donationEncounterId}/reverse-encounter-to-correct-drive")]
        public async Task<IActionResult> ReverseDriveForEncounterAsync(int donationEncounterId, string facilityCode, ReverseDriveForEncounter.Command command)
        {
            command.DonationEncounterId = donationEncounterId;
            command.FacilityCode = facilityCode;
            return Feedback(await Mediator.Send(command), ActionPerformed.Add);
        }
        #endregion

        #region Active Donation Drive Options
        [HttpGet("active-donation-drive-options")]
        public async Task<ActionResult<List<DonationDriveOptions.DonationDriveOptionsModel>>> GetDonationDriveOptions(string facilityCode)
        {
            return await Mediator.Send(new DonationDriveOptions.Query(facilityCode));
        }
        #endregion
    }
}

