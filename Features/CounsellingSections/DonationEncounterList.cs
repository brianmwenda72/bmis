﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Domain.Enums;
using BMIS.Donation.Extensions.Paging;
using BMIS.Donation.Models.DonationEncounters.DonationEncounterAtCounselling;
using CSharpFunctionalExtensions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.CounsellingSections
{
    public class DonationEncounterList
    {
        public class Query : IRequest<Result<PagedResult<DonationEncounterViewModel>>>
        {
            public Query(int? page, string s, Guid donationDriveGuid, string facilityCode)
            {
                Page = page;
                Q = s;
                DonationDriveGuid = donationDriveGuid;
                FacilityCode = facilityCode;
            }

            public int? Page { get; set; }
            public string Q { get; set; }
            public Guid DonationDriveGuid { get; set; }
            public string FacilityCode { get; set; }
        }

        public class QueryHandler : IRequestHandler<Query, Result<PagedResult<DonationEncounterViewModel>>>
        {
            private readonly IConfigurationProvider provider;
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;

            public QueryHandler(IConfigurationProvider provider, MainContext context, IHttpContextAccessor httpContextAccessor)
            {
                this.provider = provider;
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
            }

            public async Task<Result<PagedResult<DonationEncounterViewModel>>> Handle(Query request, CancellationToken cancellationToken)
            {
                string facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;

                DonationDrive donationDrive = context.DonationDrives
                    .Where(x => x.EntityGuid == request.DonationDriveGuid && x.Facility.Code == facilityCode && x.DonationDriveStatus == DonationDriveStatus.Active)
                    .Include(x => x.DonationEncounters)
                    .SingleOrDefault();

                var pageNumber = request.Page ?? 1;
                var q = request.Q?.ToLower();
                var donationEncounters = context.DonationEncounters
                    .Where(x=> x.IsComplete == false && x.DonationEncounterStatus == DonationEncounterStatus.PendingCounselling && x.Facility.Code == facilityCode && x.DonationDrive == donationDrive)
                    .Include(x=> x.Donor)
                    .AsNoTracking();
                var results = !string.IsNullOrWhiteSpace(q)
                        ? donationEncounters
                            .Where(x => EF.Functions.Like(x.Donor.DonorNumber.ToLower(), $"%{q}%") || EF.Functions.Like(x.Donor.Name.ToLower(), $"%{q}%"))
                            .OrderBy(x => x.Donor.DonorNumber)
                            .ThenBy(x=> x.Donor.Name)
                        : donationEncounters.OrderBy(x => x.Donor.DonorNumber);
                return Result.Success(await results.GetPagedAsync<DonationEncounter, DonationEncounterViewModel>(provider, pageNumber));
            }
        }
    }
}