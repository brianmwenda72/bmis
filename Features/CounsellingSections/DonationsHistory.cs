﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Domain.Enums;
using BMIS.Donation.Extensions.Paging;
using BMIS.Donation.Models.DonationEncounters;
using CSharpFunctionalExtensions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.CounsellingSections
{
    public class DonationsHistory
    {
        public class Query : IRequest<Result<PagedResult<DonationHistoryViewModel>>>
        {
            public Query(int? page, string s, string donorNumber, string facilityCode)
            {
                Page = page;
                Q = s;
                DonorNumber = donorNumber;
                FacilityCode = facilityCode;
            }

            public int? Page { get; set; }
            public string Q { get; set; }
            public string DonorNumber { get; set; }
            public string FacilityCode { get; set; }
        }

        public class QueryHandler : IRequestHandler<Query, Result<PagedResult<DonationHistoryViewModel>>>
        {
            private readonly IConfigurationProvider provider;
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;

            public QueryHandler(IConfigurationProvider provider, MainContext context, IHttpContextAccessor httpContextAccessor)
            {
                this.provider = provider;
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
            }

            public async Task<Result<PagedResult<DonationHistoryViewModel>>> Handle(Query request, CancellationToken cancellationToken)
            {
                string facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;

                var pageNumber = request.Page ?? 1;
                var q = request.Q?.ToLower();
                var donationEncounters = context.DonationEncounters
                    .Where(x=> x.DonationEncounterStatus != DonationEncounterStatus.PendingCounselling && x.Donor.DonorNumber.ToLower() == request.DonorNumber.ToLower() && x.Facility.Code == facilityCode)
                    .Include(x=> x.Donor)
                    .AsNoTracking();
                var results = !string.IsNullOrWhiteSpace(q)
                        ? donationEncounters
                            .Where(x => EF.Functions.Like(x.Donor.DonorNumber.ToLower(), $"%{q}%") || EF.Functions.Like(x.Donor.Name.ToLower(), $"%{q}%"))
                            .OrderBy(x => x.CreatedOnUtc)
                        : donationEncounters.OrderBy(x => x.CreatedOnUtc);
                return Result.Success(await results.GetPagedAsync<DonationEncounter, DonationHistoryViewModel>(provider, pageNumber));
            }
        }
    }
}