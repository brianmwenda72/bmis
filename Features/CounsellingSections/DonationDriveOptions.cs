﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using System.Collections.Generic;
using System.Linq;
using System;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Enums;
using BMIS.Donation.Domain.Entities;

namespace BMIS.Donation.Features.CounsellingSections
{
    public class DonationDriveOptions
    {
        public class Query : IRequest<List<DonationDriveOptionsModel>>
        {
            [JsonIgnore]
            public string FacilityCode { get; set; }

            public Query(string facilityCode)
            {
                this.FacilityCode = facilityCode;
            }

        }
        public class DonationDriveOptionsModel
        {
            [JsonIgnore]
            public string ScheduleName { get; set; }
            public string Label { get; set; }
            public Guid Value { get; set; }
        }

        public class QueryHandler : IRequestHandler<Query, List<DonationDriveOptionsModel>>
        {
            private readonly IConfigurationProvider provider;
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;

            public QueryHandler(IConfigurationProvider provider, IHttpContextAccessor httpContextAccessor, MainContext context)
            {
                this.provider = provider;
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
            }

            public async Task<List<DonationDriveOptionsModel>> Handle(Query request, CancellationToken cancellationToken)
            {
                string facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;

                Facility facility = context.Facilities
                        .FirstOrDefault(x => x.Code == facilityCode);

                var donationDrive = await context.DonationDrives
                    .Where(x => x.Facility.Code == facilityCode && x.DonationDriveStatus == DonationDriveStatus.Active)
                    .ProjectTo<DonationDriveOptionsModel>(provider)
                    .ToListAsync(cancellationToken: cancellationToken);


                return donationDrive;
            }
        }
    }
}