﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Domain.Enums;
using CSharpFunctionalExtensions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.CounsellingSections
{
    public class SendDonorToDonation
    {
        public class Command : IRequest<Result>
        {
            [JsonIgnore]
            public int DonationEncounterId { get; set; }
            [JsonIgnore]
            public string FacilityCode { get; set; }

            public Command(int donationEncounterId, string facilityCode)
            {
                this.DonationEncounterId = donationEncounterId;
                this.FacilityCode = facilityCode;
            }
        }
        public class CommandHandler : IRequestHandler<Command, Result>
        {
            private readonly IMapper mapper;
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;


            public CommandHandler(IMapper mapper, MainContext context, IHttpContextAccessor httpContextAccessor)
            {
                this.mapper = mapper;
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
            }

            public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
            {
                try
                {
                    string facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;
                    DonationEncounter donationEncounter = context.DonationEncounters
                        .Where(x => x.Id == request.DonationEncounterId && x.IsComplete == false && x.Facility.Code == facilityCode)
                        .Include(x => x.Donor)
                        .SingleOrDefault();
                    if (donationEncounter == null)
                    {
                        return Result.Failure("Cannot send the donor to donation since the DonationEncounter Id does not exist");
                    }
                    else
                    {
                        donationEncounter.DonationEncounterStatus = DonationEncounterStatus.CompleteCounselling;
                        donationEncounter.CreatedOnUtcDonationQueue = DateTime.UtcNow;
                        context.DonationEncounters.Update(donationEncounter);
                        await context.SaveChangesAsync();
                    }
                }
                catch (Exception ex)
                {

                    Console.WriteLine("Very bad things happened " + ex.Message);
                }

                return Result.Success();
            }
        }
    }
}
