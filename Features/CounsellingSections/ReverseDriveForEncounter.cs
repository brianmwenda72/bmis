﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using CSharpFunctionalExtensions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.CounsellingSections
{
    public class ReverseDriveForEncounter
    {
        public class Command : IRequest<Result>
        {
            [JsonIgnore]
            public int DonationEncounterId { get; set; }
            [JsonIgnore]
            public string FacilityCode { get; set; }

            public Command(int donationEncounterId, string facilityCode)
            {
                this.DonationEncounterId = donationEncounterId;
                this.FacilityCode = facilityCode;
            }

            public Guid DriveGuid { get; set; }
        }
        public class CommandHandler : IRequestHandler<Command, Result>
        {
            private readonly IMapper mapper;
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;


            public CommandHandler(IMapper mapper, MainContext context, IHttpContextAccessor httpContextAccessor)
            {
                this.mapper = mapper;
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
            }

            public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
            {
                try
                {
                    string facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;
                    DonationEncounter donationEncounter = context.DonationEncounters
                        .Include(x => x.DonationDrive)
                        .Where(x => x.Id == request.DonationEncounterId && x.IsComplete == false && x.Facility.Code == facilityCode)
                        .SingleOrDefault();
                    if (donationEncounter == null)
                    {
                        return Result.Failure("Cannot reverse the donor to the right drive since the DonationEncounter Id does not exist");
                    }
                    else
                    {
                        DonationDrive donationDrive = context.DonationDrives
                            .Include(x => x.DonationEncounters)
                            .FirstOrDefault(x => x.EntityGuid == request.DriveGuid);

                        if (donationDrive == null)
                        {
                            return Result.Failure("The drive provided does not exist");
                        }
                        else
                        {
                            donationEncounter.DonationDrive = donationDrive;
                            context.DonationEncounters.Update(donationEncounter);
                            await context.SaveChangesAsync();
                        }
                    }
                }
                catch (Exception ex)
                {

                    Console.WriteLine("Very bad things happened " + ex.Message);
                }

                return Result.Success();
            }
        }
    }
}
