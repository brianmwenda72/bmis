﻿using AutoMapper;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Models.CounsellingSections.DeclarationModels;
using BMIS.Donation.Models.CounsellingSections.HealthQuestionnaireModels;
using BMIS.Donation.Models.CounsellingSections.RiskAssessmentQuestionnaireModels;
using BMIS.Donation.Models.CounsellingSections.VitalModels;
using BMIS.Donation.Models.CounsellingDonationDrives;
using BMIS.Donation.Models.DonationEncounters.DonationEncounterAtCounselling;
using BMIS.Donation.Models.DonationEncounters;
using System.Linq;

namespace BMIS.Donation.Features.CounsellingSections
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<bool, string>().ConvertUsing(x => x.ToString().ToLower());
            CreateMap<Health, HealthQuestionnaireViewModel>();
            CreateMap<CreateOrUpdateHealthQuestionnaireViewModel, Health>();

            CreateMap<RiskAssessment, RiskAssessmentQuestionnaireViewModel>();
            CreateMap<CreateOrUpdateRiskAssessmentQuestionnaireViewModel, RiskAssessment>();

            CreateMap<Declaration, DeclarationViewModel>();
            CreateMap<CreateOrUpdateDeclarationViewModel, Declaration>();

            CreateMap<Vitals, VitalViewModel>();
            CreateMap<CreateOrUpdateVitalViewModel, Vitals>();

            CreateMap<DonationEncounter, DonationHistoryViewModel>();

            CreateMap<DonationEncounter, DonationEncounterViewModel>();
            CreateMap<Donor, DonorModel>();
            CreateMap<DonorModel, DonationEncounterViewModel>();

            CreateMap<DonationDrive, DonationDriveOptions.DonationDriveOptionsModel>()
                .ForMember(x => x.Value, y => y.MapFrom(z => z.EntityGuid))
                .ForMember(x => x.Label, y => y.MapFrom(z => $"{z.SiteName} - {z.ScheduleName}"));

            //CreateMap<DonationDriveModel, DonationDrive>();
            //CreateMap<DonationDrive, DonationDriveModel>();

            CreateMap<Questionnaire, Details.QuestionnaireViewModel>();

            CreateMap<DonationDrive, DonationDriveViewModel>();

            CreateMap<HealthFacility, GetHealthFacilityByName.Model>()
                .ForMember(x=> x.Label, y=>y.MapFrom(z=>z.OfficialName))
                .ForMember(x=> x.Value, y=>y.MapFrom(z=>z.Code));

            CreateMap<DonationEncounter, DonationEncounterDriveViewModel>();
            CreateMap<DonationDrive, DriveModel>();
            CreateMap<DriveModel, DonationEncounterDriveViewModel>();
            
        }
    }
}
