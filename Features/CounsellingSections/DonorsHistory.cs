﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Extensions.Paging;
using BMIS.Donation.Models.DonationEncounters.DonationEncounterAtCounselling;
using CSharpFunctionalExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.CounsellingSections
{
    public class DonorsHistory
    {
        public class Query : IRequest<Result<PagedResult<DonorModel>>>
        {
            public Query(int? page, string s)
            {
                Page = page;
                Q = s;
            }

            public int? Page { get; set; }
            public string Q { get; set; }
        }

        public class QueryHandler : IRequestHandler<Query, Result<PagedResult<DonorModel>>>
        {
            private readonly IConfigurationProvider provider;
            private readonly MainContext context;

            public QueryHandler(IConfigurationProvider provider, MainContext context)
            {
                this.provider = provider;
                this.context = context;
            }

            public async Task<Result<PagedResult<DonorModel>>> Handle(Query request, CancellationToken cancellationToken)
            {
                var pageNumber = request.Page ?? 1;
                var q = request.Q?.ToLower();
                var donor = context.Donors
                    .AsNoTracking();
                var results = !string.IsNullOrWhiteSpace(q)
                        ? donor
                            .Where(x => EF.Functions.Like(x.DonorNumber.ToLower(), $"%{q}%") || EF.Functions.Like(x.Name.ToLower(), $"%{q}%"))
                            .OrderBy(x => x.DonorNumber)
                            .ThenBy(x=> x.Name)
                        : donor.OrderBy(x => x.DonorNumber);
                return Result.Success(await results.GetPagedAsync<Donor, DonorModel>(provider, pageNumber));
            }
        }
    }
}