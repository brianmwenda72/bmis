﻿using CSharpFunctionalExtensions;
using MediatR;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using System.Linq;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Models.CounsellingSections.VitalModels;
using BMIS.Donation.Data;
using Microsoft.AspNetCore.Http;
using BMIS.Donation.Models.CounsellingSections.HealthQuestionnaireModels;
using BMIS.Donation.Models.CounsellingSections.RiskAssessmentQuestionnaireModels;
using BMIS.Donation.Models.CounsellingSections.DeclarationModels;
using System.Net.Http;
using System.Diagnostics;
using System.Net;
using Newtonsoft.Json;
using BMIS.Donation.Models.Questionnaires;

namespace BMIS.Donation.Features.CounsellingSections
{
    public class Details
    {
        public class Query : IRequest<Result<QuestionnaireViewModel>>
        {
            public Query(int donationEncounterId, string facilityCode)
            {
                DonationEncounterId = donationEncounterId;
                FacilityCode = facilityCode;
            }

            public int DonationEncounterId { get; set; }
            public string FacilityCode { get; set; }
        }
        public class QuestionnaireViewModel 
        {
            public HealthQuestionnaireViewModel Health { get; set; }
            public RiskAssessmentQuestionnaireViewModel RiskAssessment { get; set; }
            public VitalViewModel Vitals { get; set; }
            public DeclarationViewModel Declaration { get; set; }
        }
        public class QueryHandler : IRequestHandler<Query, Result<QuestionnaireViewModel>>
        {
            private readonly IConfigurationProvider provider;
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;
            private readonly IMapper mapper;
            private readonly HttpClient client = new HttpClient();
            private const string AccessKey = "Token 986421d031191ce762963066a8964a66b421de86";
            private const string BaseUrl = "https://portalprimaryapi.health.go.ke/api/bmis/get-donor-counselling-response/user";

            public QueryHandler(MainContext context,
                IConfigurationProvider provider,
                IHttpContextAccessor httpContextAccessor,
                IMapper mapper, HttpClient client)
            {
                this.provider = provider;
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
                this.mapper = mapper;
                this.client = client;
            }
            public async Task<Result<QuestionnaireViewModel>> Handle(Query request, CancellationToken cancellationToken)
            {
                string facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;

                DonationEncounter donationEncounter = await context.DonationEncounters
                    .Where(x => x.Id == request.DonationEncounterId && x.Facility.Code == facilityCode)
                    .Include(x => x.Donor)
                    .Include(x => x.Questionnaire)
                    .SingleOrDefaultAsync();
                if (donationEncounter.Questionnaire == null)
                {
                    var portalKey = donationEncounter.Donor.PortalKey;

                    if (portalKey != 0)
                    {
                        var url = $"{BaseUrl}/{portalKey}";
                        var req = new HttpRequestMessage(HttpMethod.Get, url);
                        req.Headers.Add("authorization", AccessKey);

                        HttpResponseMessage httpResponse = await client.SendAsync(req);
                        Debug.WriteLine("Found");
                        var content = await httpResponse.Content.ReadAsStringAsync();

                        if (httpResponse.StatusCode != HttpStatusCode.NotFound)
                        {
                            var questionnaireData = JsonConvert.DeserializeObject<PortalQuestionnaireViewModel>(content);

                            Health health = new()
                            {
                                FeelingWellToday = bool.Parse(questionnaireData.Response.FeelingWellToday),
                                EatenInTheLastSixHours = bool.Parse(questionnaireData.Response.EatenInTheLastSixHours),
                                EverFainted = bool.Parse(questionnaireData.Response.EverFainted),
                                AnyTreatmentOrMedicationsInPast6Months = bool.Parse(questionnaireData.Response.AnyTreatmentOrMedicationsInPast6Months),
                                AnyInjectionsOrVaccinationsInPast6Months = bool.Parse(questionnaireData.Response.AnyInjectionsOrVaccinationsInPast6Months),
                                AnyBloodOrOrganTransfusionInThePast12Months = bool.Parse(questionnaireData.Response.AnyBloodOrOrganTransfusionInThePast12Months),
                                AnyProblemsWithHeartOrLungs = bool.Parse(questionnaireData.Response.AnyProblemsWithHeartOrLungs),
                                AnyBleedingConditionsOrBloodDisease = bool.Parse(questionnaireData.Response.AnyBleedingConditionsOrBloodDisease),
                                AnyPregnancyOrBreastFeedingForFemaleDonors = bool.Parse(questionnaireData.Response.AnyPregnancyOrBreastFeedingForFemaleDonors),
                                DoYouHaveCancer = bool.Parse(questionnaireData.Response.DoYouHaveCancer),
                                DoYouHaveDiabetisOrEpilepsyOrTB = bool.Parse(questionnaireData.Response.DoYouHaveDiabetisOrEpilepsyOrTB),
                                AnyOtherLongTermIllness = bool.Parse(questionnaireData.Response.AnyOtherLongTermIllness),
                                SpecifyLongTermIllnessIfAny = questionnaireData.Response.SpecifyLongTermIllnessIfAny
                            };
                            RiskAssessment riskAssessment = new()
                            {
                                HaveReceivedOrGivenAnySexualFavours = bool.Parse(questionnaireData.Response.HaveReceivedOrGivenAnySexualFavours),
                                BeenRapedOrSodomised = bool.Parse(questionnaireData.Response.BeenRapedOrSodomised),
                                HaveHadSTDs = bool.Parse(questionnaireData.Response.HaveHadSTDs),
                                HaveHadRecklessSex = bool.Parse(questionnaireData.Response.HaveHadRecklessSex),
                                HaveHadYellowEyesOrSkin = bool.Parse(questionnaireData.Response.HaveHadYellowEyesOrSkin),
                                HaveUsedNonMedicinalDrugs = bool.Parse(questionnaireData.Response.HaveUsedNonMedicinalDrugs),
                                DoYouConsiderYourBloodSafe = bool.Parse(questionnaireData.Response.DoYouConsiderYourBloodSafe),
                                HaveAnySexBesidesRegularPartner = bool.Parse(questionnaireData.Response.HaveAnySexBesidesRegularPartner),
                                HaveAnyStubWoundOrAccidentalNeedleStickInjury = bool.Parse(questionnaireData.Response.HaveAnyStubWoundOrAccidentalNeedleStickInjury),
                                HaveSexualContactOrLivedWithSomeoneWithYellowEyesOrSkin = bool.Parse(questionnaireData.Response.HaveSexualContactOrLivedWithSomeoneWithYellowEyesOrSkin),
                                AnyInjectionOutsideHealthFacility = bool.Parse(questionnaireData.Response.HaveYouAndPartnerBeenTestedForHIV),
                                HaveYouAndPartnerBeenTestedForHIV = bool.Parse(questionnaireData.Response.HaveReceivedOrGivenAnySexualFavours)
                            };
                            Declaration declaration = new()
                            {
                                DonationType = questionnaireData.Response.DonationType,
                                DeclarationConsentText = questionnaireData.Response.DeclarationConsentText,
                                Consented = bool.Parse(questionnaireData.Response.Consented)
                            };

                            Questionnaire newQuestionnaire = new()
                            {
                                DonationEncounterId = donationEncounter.Id,
                                Health = health,
                                RiskAssessment = riskAssessment,
                                Declaration = declaration
                            };

                            context.Questionnaires.Add(newQuestionnaire);
                            await context.SaveChangesAsync();
                        }
                    }
                }

                Questionnaire questionnaire = await context.Questionnaires
                    .Where(x => x.DonationEncounterId == donationEncounter.Id)
                    .Include(x=> x.Health)
                    .Include(x=> x.RiskAssessment)
                    .Include(x=> x.Vitals)
                    .Include(x=> x.Declaration)
                    .SingleOrDefaultAsync();

                if (questionnaire is not null)
                {
                    return Result.Success(mapper.Map<QuestionnaireViewModel>(questionnaire)); 
                }
                else
                {
                    return Result.Failure<QuestionnaireViewModel>("No Data Found");
                }
            }
        }
    }
}
