﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Domain.Enums;
using CSharpFunctionalExtensions;
using DHP.Contracts;
using DHP.Contracts.Events.BloodBank;
using MassTransit;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.CounsellingSections
{
    public class DeferDonationAtCounselling
    {
        public class Command : IRequest<Result>
        {
            [JsonIgnore]
            public int DonationEncounterId { get; set; }
            [JsonIgnore]
            public string FacilityCode { get; set; }

            public Command(int donationEncounterId, string facilityCode)
            {
                this.DonationEncounterId = donationEncounterId;
                this.FacilityCode = facilityCode;
            }

            public string DeffermentType { get; set; }
            public string DeffermentReason { get; set; }
            public string DefermentNote { get; set; }
            public string HealthFacilityCode { get; set; }
        }
        public class CommandHandler : IRequestHandler<Command, Result>
        {
            private readonly IMapper mapper;
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;
            private readonly ISendEndpointProvider sendEndpointProvider;
            private readonly Uri deferedDonorServiceAddress = new($"queue:{QueueDefinitions.BMIS.DONOR_DEFERRED_QUEUE}");


            public CommandHandler(IMapper mapper, MainContext context, IHttpContextAccessor httpContextAccessor, ISendEndpointProvider sendEndpointProvider)
            {
                this.mapper = mapper;
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
                this.sendEndpointProvider = sendEndpointProvider;
            }

            public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
            {
                try
                {
                    string facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;
                    string userName = httpContextAccessor.HttpContext?.User.FindFirst("full_name")?.Value;

                    Facility facility = context.Facilities
                        .SingleOrDefault(x => x.Code == facilityCode);

                    DonationEncounter donationEncounter = context.DonationEncounters
                        .Where(x => x.Id == request.DonationEncounterId && x.IsComplete == false && x.Facility.Code == facilityCode && x.Defered == false)
                        .Include(x=> x.Donor)
                        .SingleOrDefault();
                    if (donationEncounter == null)
                    {
                        return Result.Failure("Cannot Deffer the donor since the DonationEncounter Id does not exist");
                    }
                    else
                    {

                        HealthFacility healthFacility = context.HealthFacilities
                            .FirstOrDefault(x=> x.Code == request.HealthFacilityCode);

                        donationEncounter.DonationEncounterStatus = DonationEncounterStatus.DeferredAtCounselling;
                        donationEncounter.Defered = true;
                        donationEncounter.HealthFacility = healthFacility;
                        donationEncounter.DefermentReason = request.DeffermentReason;
                        donationEncounter.DefermentType = request.DeffermentType;
                        donationEncounter.DefermentNote = request.DefermentNote;
                        context.DonationEncounters.Update(donationEncounter);
                        await context.SaveChangesAsync();

                        Donor donor = context.Donors
                            .SingleOrDefault(x=> x.DonorNumber.ToLower() == donationEncounter.Donor.DonorNumber.ToLower());
                        if (donor != null)
                        {
                            DonorDeferredContract donorDeferredContract = new DonorDeferredContract()
                            {
                                DonorNumber = donor.DonorNumber,
                                DeferralType = request.DeffermentType,
                                DeferralReason = request.DeffermentReason,
                                DateDeferred = DateTime.UtcNow,
                                FacilityDeferedFrom = facility.Name,
                                DeferedBy = userName
                            };
                            await (await sendEndpointProvider.GetSendEndpoint(deferedDonorServiceAddress)).Send(donorDeferredContract, cancellationToken);

                            return Result.Success();
                        }
                        else
                        {
                            return Result.Failure("The Donor with the provided Encounter does not exist");
                        }
                    }
                }
                catch (Exception ex)
                {

                    Console.WriteLine("Very bad things happened " + ex.Message);
                }

                return Result.Success();
            }
        }
    }
}
