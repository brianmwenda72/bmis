﻿using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using CSharpFunctionalExtensions;
using Humanizer;
using Humanizer.Localisation;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using RulesEngine.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.CounsellingSections
{

    public class CalculateEligibility
    {
        public class Command : IRequest<Result>
        {
            public Command(int donationEncounterId, string facilityCode)
            {
                DonationEncounterId = donationEncounterId;
                FacilityCode = facilityCode;
            }

            [JsonIgnore]
            public int DonationEncounterId { get; set; }

            [JsonIgnore]
            public string FacilityCode { get; set; }
        }

        public class CommandHandler : IRequestHandler<Command, Result>
        {
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;
            public CommandHandler(MainContext context, IHttpContextAccessor httpContextAccessor)
            {
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
            }
            public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
            {
                string facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;
                DonationEncounter donationEncounter = context.DonationEncounters
                    .Include(x=> x.Donor)
                    .Include(x=> x.Questionnaire)
                    .SingleOrDefault(x=> x.Id == request.DonationEncounterId && x.Facility.Code == facilityCode);

                var jsontext = await File.ReadAllTextAsync(@"WorkFlowRules/eligibility.json", cancellationToken);
                var permissions = JsonConvert.DeserializeObject<List<Workflow>>(jsontext);
                var rulesEngine = new RulesEngine.RulesEngine(permissions?.ToArray());
                var resultList = await rulesEngine.ExecuteAllRulesAsync("Eligibility",donationEncounter);
                var response = resultList.Where(ruleResultTree => !ruleResultTree.IsSuccess).Aggregate("", (current, ruleResultTree) => $"{current}{Environment.NewLine}{ruleResultTree.ExceptionMessage}");

                return string.IsNullOrWhiteSpace(response) ? Result.Success() : Result.Failure(response);
            }
        }
    }
}
