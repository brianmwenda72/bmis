﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using CSharpFunctionalExtensions;
using BMIS.Donation.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.CounsellingSections
{
    public class GetHealthFacilityByName
    {
        public class Query : IRequest<Result<List<Model>>>
        {
            public Query(string officialName)
            {
                OfficialName = officialName;
            }

            public string OfficialName { get; }

        }
        public class Model
        {
            public string Value { get; set; }
            public string Label { get; set; }
        }
        public class QueryHandler : IRequestHandler<Query, Result<List<Model>>>
        {
            private readonly MainContext context;
            private IConfigurationProvider provider;

            public QueryHandler(MainContext context, IConfigurationProvider provider)
            {
                this.context = context;
                this.provider = provider;
            }

            public async Task<Result<List<Model>>> Handle(Query request, CancellationToken cancellationToken)
            {
                return await context.HealthFacilities.AsNoTracking()
                    //.Where(x => x.Approved == "Yes" && EF.Functions.Like(x.OfficialName.Trim().ToLower(), $"%{request.OfficialName.Trim().ToLower()}%"))
                    .Where(x => EF.Functions.Like(x.OfficialName.Trim().ToLower(), $"%{request.OfficialName.Trim().ToLower()}%"))
                    .ProjectTo<Model>(provider)
                     .ToListAsync(cancellationToken);

            }
        }
    }
}

