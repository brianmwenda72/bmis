﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Domain.Enums;
using BMIS.Donation.Extensions.Paging;
using BMIS.Donation.Models.DonationEncounters.DonationEncounterAtCounselling;
using CSharpFunctionalExtensions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.CounsellingSections
{
    public class DonorListWithActualEncounter
    {
        public class Query : IRequest<Result<PagedResult<DonationEncounterDriveViewModel>>>
        {
            public Query(int? page, string q, string facilityCode)
            {
                Page = page;
                Q = q;
                FacilityCode = facilityCode;
            }

            public int? Page { get; set; }
            public string Q { get; set; }
            public string FacilityCode { get; set; }
        }

        public class QueryHandler : IRequestHandler<Query, Result<PagedResult<DonationEncounterDriveViewModel>>>
        {
            private readonly IConfigurationProvider provider;
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;

            public QueryHandler(IConfigurationProvider provider, MainContext context, IHttpContextAccessor httpContextAccessor)
            {
                this.provider = provider;
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
            }

            public async Task<Result<PagedResult<DonationEncounterDriveViewModel>>> Handle(Query request, CancellationToken cancellationToken)
            {
                string facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;

                var pageNumber = request.Page ?? 1;
                var q = request.Q?.ToLower();

                var donationEncounters = context.DonationEncounters
                    .Include(x => x.Facility)
                    .Include(x => x.Donor)
                    .Include(x => x.DonationDrive)
                    .Where(x => x.IsComplete == false && x.DonationEncounterStatus == DonationEncounterStatus.PendingCounselling && x.Facility.Code == facilityCode)
                    .AsNoTracking();

                var results = !string.IsNullOrWhiteSpace(q)
                        ? donationEncounters
                            .Where(x => EF.Functions.Like(x.Donor.Name.ToLower(), $"%{q}%") || EF.Functions.Like(x.Donor.DonorNumber.ToLower(), $"%{q}%"))
                            .OrderBy(x => x.CreatedOnUtc)
                        : donationEncounters.OrderBy(x => x.CreatedOnUtc);
                return Result.Success(await results.GetPagedAsync<DonationEncounter, DonationEncounterDriveViewModel>(provider, pageNumber));
            }
        }
    }
}