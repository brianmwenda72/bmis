﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Defaults;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Domain.Enums;
using BMIS.Donation.Models.CounsellingSections.DeclarationModels;
using BMIS.Donation.Models.CounsellingSections.HealthQuestionnaireModels;
using BMIS.Donation.Models.CounsellingSections.RiskAssessmentQuestionnaireModels;
using BMIS.Donation.Models.CounsellingSections.VitalModels;
using CSharpFunctionalExtensions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.CounsellingSections
{
    public class CreateOrUpdate
    {
        public class Command : IRequest<Result>
        {
            [JsonIgnore]
            public int DonationEncounterId { get; set; }
            [JsonIgnore]
            public string FacilityCode { get; set; }
            [JsonIgnore]
            public string SectionName { get; set; }
            public Command(int donationEncounterId, string facilityCode, string sectionName)
            {
                this.DonationEncounterId = donationEncounterId;
                this.FacilityCode = facilityCode;
                this.SectionName = sectionName;
            }
            
            public CreateOrUpdateHealthQuestionnaireViewModel Health { get; set; }
            public CreateOrUpdateRiskAssessmentQuestionnaireViewModel RiskAssessment { get; set; }
            public CreateOrUpdateVitalViewModel Vitals { get; set; }
            public CreateOrUpdateDeclarationViewModel Declaration { get; set; }

        }
        public class CommandHandler : IRequestHandler<Command, Result>
        {
            private readonly IMapper mapper;
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;


            public CommandHandler(IMapper mapper, MainContext context, IHttpContextAccessor httpContextAccessor)
            {
                this.mapper = mapper;
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
            }

            public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
            {
                try
                {
                    string facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;
                    DonationEncounter donationEncounter = context.DonationEncounters
                        .Where(x => x.Id == request.DonationEncounterId && x.IsComplete == false && x.Facility.Code == facilityCode)
                        .SingleOrDefault();
                    if (donationEncounter == null)
                    {
                        return Result.Failure("Cannot Add the Questionnaire since the DonationEncounter Id does not exist");
                    }
                    else
                    {
                        Questionnaire questionnaire = context.Questionnaires
                            .SingleOrDefault(x=> x.DonationEncounterId == donationEncounter.Id);
                        if (questionnaire == null)
                        {
                            if (request.SectionName.ToLower() == EntityDefault.HealthSection.ToLower())
                            {
                                Questionnaire newQuestionnaire = new Questionnaire()
                                {
                                    DonationEncounterId = donationEncounter.Id,
                                    QuestionnaireStatus = QuestionnaireStatus.HealthQuestionnaireFilled,
                                    Health = mapper.Map<Health>(request.Health)
                                };
                                context.Questionnaires.Add(newQuestionnaire);
                                await context.SaveChangesAsync();
                            }
                            else if (request.SectionName.ToLower() == EntityDefault.RiskAssessmentSection.ToLower())
                            {
                                Questionnaire newQuestionnaire = new Questionnaire()
                                {
                                    DonationEncounterId = donationEncounter.Id,
                                    QuestionnaireStatus = QuestionnaireStatus.RiskAssessmentQuestionnaireFilled,
                                    RiskAssessment = mapper.Map<RiskAssessment>(request.RiskAssessment)
                                };
                                context.Questionnaires.Add(newQuestionnaire);
                                await context.SaveChangesAsync();
                            }
                            else if (request.SectionName.ToLower() == EntityDefault.VitalsSection.ToLower())
                            {
                                Questionnaire newQuestionnaire = new Questionnaire()
                                {
                                    DonationEncounterId = donationEncounter.Id,
                                    QuestionnaireStatus = QuestionnaireStatus.VitalsQuestionnaireFilled,
                                    Vitals = mapper.Map<Vitals>(request.Vitals)
                                };
                                context.Questionnaires.Add(newQuestionnaire);
                                await context.SaveChangesAsync();
                            }
                            else if (request.SectionName.ToLower() == EntityDefault.DeclarationSection.ToLower())
                            {
                                Questionnaire newQuestionnaire = new Questionnaire()
                                {
                                    DonationEncounterId = donationEncounter.Id,
                                    QuestionnaireStatus = QuestionnaireStatus.DonorConsented,
                                    Declaration = mapper.Map<Declaration>(request.Declaration)
                                };
                                context.Questionnaires.Add(newQuestionnaire);
                                await context.SaveChangesAsync();
                            }
                        }
                        else
                        {
                            if (request.SectionName.ToLower() == EntityDefault.HealthSection.ToLower())
                            {
                                questionnaire.Health = new Health();
                                mapper.Map(request.Health, questionnaire.Health);
                                context.Questionnaires.Update(questionnaire);
                                await context.SaveChangesAsync();
                            }
                            else if (request.SectionName.ToLower() == EntityDefault.RiskAssessmentSection.ToLower())
                            {
                                questionnaire.RiskAssessment = new RiskAssessment();
                                mapper.Map(request.RiskAssessment, questionnaire.RiskAssessment);
                                context.Questionnaires.Update(questionnaire);
                                await context.SaveChangesAsync();
                            }
                            else if (request.SectionName.ToLower() == EntityDefault.VitalsSection.ToLower())
                            {
                                questionnaire.Vitals = new Vitals();
                                mapper.Map(request.Vitals, questionnaire.Vitals);
                                context.Questionnaires.Update(questionnaire);
                                await context.SaveChangesAsync();
                            }
                            else if (request.SectionName.ToLower() == EntityDefault.DeclarationSection.ToLower())
                            {
                                questionnaire.Declaration = new Declaration();
                                mapper.Map(request.Declaration, questionnaire.Declaration);
                                context.Questionnaires.Update(questionnaire);
                                await context.SaveChangesAsync();
                            }
                            
                        }
                    }
                }
                catch (Exception ex)
                {

                    Console.WriteLine("Very bad things happened " + ex.Message);
                }

                return Result.Success();
            }
        }
    }
}
