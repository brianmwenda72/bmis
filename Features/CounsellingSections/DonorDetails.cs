﻿using CSharpFunctionalExtensions;
using MediatR;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using System.Linq;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Data;
using Microsoft.AspNetCore.Http;
using BMIS.Donation.Models.DonationEncounters.DonationEncounterAtCounselling;

namespace BMIS.Donation.Features.CounsellingSections
{
    public class DonorDetails
    {
        public class Query : IRequest<Result<DonorModel>>
        {
            public Query(int donationEncounterId, string facilityCode)
            {
                DonationEncounterId = donationEncounterId;
                FacilityCode = facilityCode;
            }

            public int DonationEncounterId { get; set; }
            public string FacilityCode { get; set; }
        }
        public DonorModel Donor { get; set; }
        public class QueryHandler : IRequestHandler<Query, Result<DonorModel>>
        {
            private readonly IConfigurationProvider provider;
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;
            private readonly IMapper mapper;

            public QueryHandler(MainContext context,
                IConfigurationProvider provider,
                IHttpContextAccessor httpContextAccessor,
                IMapper mapper)
            {
                this.provider = provider;
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
                this.mapper = mapper;
            }
            public async Task<Result<DonorModel>> Handle(Query request, CancellationToken cancellationToken)
            {
                string facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;
                DonationEncounter donationEncounter = await context.DonationEncounters
                    .Where(x => x.Id == request.DonationEncounterId && x.Facility.Code == facilityCode)
                    .Include(x => x.Donor)
                    .SingleOrDefaultAsync();
                Donor donor = await context.Donors
                    .Where(x => x.Id == donationEncounter.Donor.Id)
                    .SingleOrDefaultAsync();

                if (donor is not null)
                {
                    return Result.Success(mapper.Map<DonorModel>(donor)); 
                }
                else
                {
                    return Result.Failure<DonorModel>("No Data Found");
                }

            }
        }
    }
}
