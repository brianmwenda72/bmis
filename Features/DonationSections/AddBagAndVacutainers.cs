﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Defaults;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Domain.Enums;
using CSharpFunctionalExtensions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationSections
{
    public class AddBagAndVacutainers
    {
        public class Command : IRequest<Result>
        {
            [JsonIgnore]
            public int DonationEncounterId { get; set; }
            [JsonIgnore]
            public string FacilityCode { get; set; }

            public Command(int donationEncounterId, string facilityCode)
            {
                this.DonationEncounterId = donationEncounterId;
                this.FacilityCode = facilityCode;
            }
            public string DonationMethod { get; set; } //from Lookup
            public string BloodBagType { get; set; }
            public string BloodBagLabel { get; set; } // from lookup
        }
        public class CommandHandler : IRequestHandler<Command, Result>
        {
            private readonly IMapper mapper;
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;


            public CommandHandler(IMapper mapper, MainContext context, IHttpContextAccessor httpContextAccessor)
            {
                this.mapper = mapper;
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
            }

            public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
            {
                try
                {
                    string facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;

                    DonationEncounter donationEncounter = context.DonationEncounters
                        .Where(x => x.Id == request.DonationEncounterId && x.IsComplete == false && x.Facility.Code == facilityCode)
                        .Include(x => x.DonationDrive)
                        .Include(x => x.BloodBagDonationDetails)
                        .Include(x => x.Vacutainers)
                        .SingleOrDefault();
                    if (donationEncounter == null)
                    {
                        return Result.Failure("Cannot add the Bags and vacutainers since the DonationEncounter Id does not exist");
                    }
                    else
                    {
                        var donationNumberValidator = context.BloodBagDonationDetails
                                .Where(x => x.Deleted == false)
                                .Any(a => a.BloodBagLabel.ToLower() == request.BloodBagLabel.ToLower());

                        if (donationNumberValidator == true)
                        {
                            return Result.Failure("The donation number provided for this bag has already been assigned");
                        }
                        else
                        {
                            var validationChecker = context.BloodBagDonationDetails
                            .Where(x => x.DonationEncounter == donationEncounter && x.Discarded == false)
                            .ToList();
                            if (validationChecker.Count() == 0)
                            {
                                BloodBagDonationDetail newbloodBagDonationDetail = new BloodBagDonationDetail()
                                {
                                    DonationEncounter = donationEncounter,
                                    DonationMethod = request.DonationMethod,
                                    BloodBagType = request.BloodBagType,
                                    BloodBagLabel = request.BloodBagLabel,
                                    DonationNumber = request.BloodBagLabel,
                                    BloodBagDonataionDetailStatus = BloodBagDonataionDetailStatus.IssuedBagAndVacutainers,
                                    CreatedOnUtc = DateTime.UtcNow
                                };
                                context.BloodBagDonationDetails.Add(newbloodBagDonationDetail);
                                await context.SaveChangesAsync();

                                if (newbloodBagDonationDetail.DonationNumber != null)
                                {
                                    Vacutainer screeningVacutainer = new Vacutainer()
                                    {
                                        VacutainerType = EntityDefault.ScreeningVacutainer,
                                        Label = newbloodBagDonationDetail.DonationNumber,
                                        DonationEncounter = donationEncounter
                                    };
                                    context.Vacutainers.Add(screeningVacutainer);
                                    await context.SaveChangesAsync();

                                    Vacutainer groupingVacutainer = new Vacutainer()
                                    {
                                        VacutainerType = EntityDefault.GroupingVacutainer,
                                        Label = newbloodBagDonationDetail.DonationNumber,
                                        DonationEncounter = donationEncounter
                                    };
                                    context.Vacutainers.Add(groupingVacutainer);
                                    await context.SaveChangesAsync();

                                    donationEncounter.DonationEncounterStatus = DonationEncounterStatus.PendingDonation;
                                    donationEncounter.DonationNumber = newbloodBagDonationDetail.DonationNumber;

                                    context.DonationEncounters.Update(donationEncounter);
                                    await context.SaveChangesAsync();
                                }
                            }
                            else
                            {
                                return Result.Failure("Donor has an existing assigned blood bag");
                            }
                        }

                    }
                }
                catch (Exception ex)
                {

                    Console.WriteLine("Very bad things happened " + ex.Message);
                }

                return Result.Success();
            }
        }
    }
}
