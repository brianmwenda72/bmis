﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Domain.Enums;
using CSharpFunctionalExtensions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationSections
{
    public class StartDonation
    {
        public class Command : IRequest<Result>
        {
            [JsonIgnore]
            public int DonationEncounterId { get; set; }
            [JsonIgnore]
            public string FacilityCode { get; set; }

            public Command(int donationEncounterId, string facilityCode)
            {
                this.DonationEncounterId = donationEncounterId;
                this.FacilityCode = facilityCode;
            }

            public DateTime DonationStartTime { get; set; }
        }
        public class CommandHandler : IRequestHandler<Command, Result>
        {
            private readonly IMapper mapper;
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;


            public CommandHandler(IMapper mapper, MainContext context, IHttpContextAccessor httpContextAccessor)
            {
                this.mapper = mapper;
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
            }

            public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
            {
                try
                {
                    string facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;

                    DonationEncounter donationEncounter = context.DonationEncounters
                        .Where(x => x.Id == request.DonationEncounterId && x.IsComplete == false && x.Facility.Code == facilityCode && x.DonationEncounterStatus == DonationEncounterStatus.PendingDonation)
                        .Include(x => x.BloodBagDonationDetails)
                        .SingleOrDefault();
                    if (donationEncounter == null)
                    {
                        return Result.Failure("The Donation with the given encounter Id is invalid or has already been started");
                    }
                    else
                    {
                        BloodBagDonationDetail bloodBagDonationDetail = context.BloodBagDonationDetails
                            .Where(x => x.DonationEncounter.Id == donationEncounter.Id && x.Discarded == false)
                            .SingleOrDefault();
                        if (bloodBagDonationDetail != null)
                        {
                            bloodBagDonationDetail.NeedleTimeIn = request.DonationStartTime.ToUniversalTime();
                            bloodBagDonationDetail.BloodBagDonataionDetailStatus = BloodBagDonataionDetailStatus.NeedleIn;
                            context.BloodBagDonationDetails.Update(bloodBagDonationDetail);
                            await context.SaveChangesAsync();

                            donationEncounter.DonationEncounterStatus = DonationEncounterStatus.Donating;
                            donationEncounter.DonationStartTime = request.DonationStartTime.ToUniversalTime();
                            context.DonationEncounters.Update(donationEncounter);
                            await context.SaveChangesAsync();
                        }
                        else
                        {
                            return Result.Failure("The donor has no active bag. Please add a bag againist the donor");
                        }

                    }
                }
                catch (Exception ex)
                {

                    Console.WriteLine("Very bad things happened " + ex.Message);
                }

                return Result.Success();
            }
        }
    }
}
