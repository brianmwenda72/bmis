﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Domain.Enums;
using BMIS.Donation.Extensions.Paging;
using BMIS.Donation.Models.DonationDrives;
using CSharpFunctionalExtensions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationSections
{
    public class DonationDriveList
    {
        public class Query : IRequest<Result<PagedResult<DonationDriveViewModel>>>
        {
            public Query(int? page, string s, string facilityCode)
            {
                Page = page;
                Q = s;
                FacilityCode = facilityCode;
            }

            public int? Page { get; set; }
            public string Q { get; set; }
            public string FacilityCode { get; set; }
        }

        public class QueryHandler : IRequestHandler<Query, Result<PagedResult<DonationDriveViewModel>>>
        {
            private readonly IConfigurationProvider provider;
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;

            public QueryHandler(IConfigurationProvider provider, MainContext context, IHttpContextAccessor httpContextAccessor)
            {
                this.provider = provider;
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
            }

            public async Task<Result<PagedResult<DonationDriveViewModel>>> Handle(Query request, CancellationToken cancellationToken)
            {
                string facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;

                var pageNumber = request.Page ?? 1;
                var q = request.Q?.ToLower();

                var donationDrive = context.DonationDrives
                    .Where(x => x.Facility.Code == facilityCode && x.DonationDriveStatus == DonationDriveStatus.Active)
                    .Include(x=>x.DonationEncounters)
                    .ThenInclude(x=> x.BloodBagDonationDetails)
                    .Include(x => x.DonationEncounters)
                    .ThenInclude(x => x.Vacutainers)
                    .AsNoTracking();
                var results = !string.IsNullOrWhiteSpace(q)
                        ? donationDrive
                            .Where(x => EF.Functions.Like(x.SiteName.ToLower(), $"%{q}%") || EF.Functions.Like(x.Code.ToLower(), $"%{q}%"))
                            .OrderBy(x => x.SiteName)
                            .ThenBy(x=> x.Code)
                        : donationDrive.OrderBy(x => x.SiteName);
                return Result.Success(await results.GetPagedAsync<DonationDrive, DonationDriveViewModel>(provider, pageNumber));
            }
        }
    }
}