﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Domain.Enums;
using BMIS.Donation.Extensions.Paging;
using BMIS.Donation.Models.DonationEncounters.DonationEncounterAtDonation;
using CSharpFunctionalExtensions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationSections
{
    public class DonationEncounterList
    {
        public class Query : IRequest<Result<PagedResult<DonationEncounterViewModel>>>
        {
            public Query(int? page, string s, Guid donationDriveGuid, string facilityCode, string donorStatus)
            {
                Page = page;
                Q = s;
                DonationDriveGuid = donationDriveGuid;
                FacilityCode = facilityCode;
                DonorStatus = donorStatus;
            }

            public int? Page { get; set; }
            public string Q { get; set; }
            public Guid DonationDriveGuid { get; set; }
            public string FacilityCode { get; set; }
            public string DonorStatus { get; set; }
        }

        public class QueryHandler : IRequestHandler<Query, Result<PagedResult<DonationEncounterViewModel>>>
        {
            private readonly IConfigurationProvider provider;
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;

            public QueryHandler(IConfigurationProvider provider, MainContext context, IHttpContextAccessor httpContextAccessor)
            {
                this.provider = provider;
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
            }

            public async Task<Result<PagedResult<DonationEncounterViewModel>>> Handle(Query request, CancellationToken cancellationToken)
            {
                string facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;

                DonationDrive donationDrive = context.DonationDrives
                    .Where(x => x.EntityGuid == request.DonationDriveGuid && x.Facility.Code == facilityCode && x.DonationDriveStatus == DonationDriveStatus.Active)
                    .Include(x=> x.DonationEncounters)
                    .SingleOrDefault();

                var pageNumber = request.Page ?? 1;
                var q = request.Q?.ToLower();
                var donorStatus = request.DonorStatus?.ToLower();

                var donationEncounters = context.DonationEncounters
                    .Where(x=> x.IsComplete == false && x.DonationEncounterStatus != DonationEncounterStatus.DeferredAtDonation && x.DonationEncounterStatus != DonationEncounterStatus.DeferredAtCounselling && x.DonationEncounterStatus != DonationEncounterStatus.PendingCounselling && x.Facility.Code == facilityCode && x.DonationDrive.EntityGuid == donationDrive.EntityGuid)
                    .Include(x=> x.BloodBagDonationDetails)
                    .Include(x=> x.Donor)
                    .AsNoTracking();

                IQueryable<DonationEncounter> results;

                if (!string.IsNullOrWhiteSpace(donorStatus) && !string.IsNullOrWhiteSpace(q))
                {
                    results = !string.IsNullOrWhiteSpace(donorStatus) && !string.IsNullOrWhiteSpace(q)
                    ? donationEncounters
                    .Where(x => EF.Functions.Like(x.Donor.DonorNumber.ToLower(), $"%{q}%") || EF.Functions.Like(x.Donor.Name.ToLower(), $"%{q}%") || x.DonationEncounterStatus == Enum.Parse<DonationEncounterStatus>(donorStatus, true))
                        .OrderBy(x => x.CreatedOnUtcDonationQueue)
                    : donationEncounters;

                    results = results.OrderBy(x => x.CreatedOnUtcDonationQueue);
                }
                else if (!string.IsNullOrWhiteSpace(donorStatus))
                {
                    results = !string.IsNullOrWhiteSpace(donorStatus)
                    ? donationEncounters
                    .Where(x => x.DonationEncounterStatus == Enum.Parse<DonationEncounterStatus>(donorStatus, true))
                        .OrderBy(x => x.CreatedOnUtcDonationQueue)
                    : donationEncounters;

                    results = results.OrderBy(x => x.CreatedOnUtcDonationQueue);
                }
                else if (!string.IsNullOrWhiteSpace(q))
                {
                    results = !string.IsNullOrWhiteSpace(q)
                    ? donationEncounters
                    .Where(x => EF.Functions.Like(x.Donor.DonorNumber.ToLower(), $"%{q}%") || EF.Functions.Like(x.Donor.Name.ToLower(), $"%{q}%"))
                        .OrderBy(x => x.CreatedOnUtcDonationQueue)
                    : donationEncounters;

                    results = results.OrderBy(x => x.CreatedOnUtcDonationQueue);
                }
                else
                {
                    results = donationEncounters.OrderBy(x => x.CreatedOnUtcDonationQueue);
                }


                return await results.GetPagedAsync<DonationEncounter, DonationEncounterViewModel>(provider, pageNumber);
            }
        }
    }
}