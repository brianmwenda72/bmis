﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Domain.Enums;
using BMIS.Donation.Extensions.Paging;
using BMIS.Donation.Models.BloodBagDonationDetails;
using CSharpFunctionalExtensions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationSections
{
    public class SentDonationsDiscardsList
    {
        public class Query : IRequest<Result<PagedResult<BloodBagDonationDetailViewModel>>>
        {
            public Query(int? page, string s, Guid batchGuid, string facilityCode)
            {
                Page = page;
                Q = s;
                BatchGuid = batchGuid;
                FacilityCode = facilityCode;
            }

            public int? Page { get; set; }
            public string Q { get; set; }
            public Guid BatchGuid { get; set; }
            public string FacilityCode { get; set; }
        }

        public class QueryHandler : IRequestHandler<Query, Result<PagedResult<BloodBagDonationDetailViewModel>>>
        {
            private readonly IConfigurationProvider provider;
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;

            public QueryHandler(IConfigurationProvider provider, MainContext context, IHttpContextAccessor httpContextAccessor)
            {
                this.provider = provider;
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
            }

            public async Task<Result<PagedResult<BloodBagDonationDetailViewModel>>> Handle(Query request, CancellationToken cancellationToken)
            {
                string facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;

                BatchProcessing batchProcessing = context.BatchProcessings
                    .Include(x=> x.BloodBagDonationDetails)
                    .Where(x => x.EntityGuid == request.BatchGuid && x.DonationDrive.Facility.Code == facilityCode && x.BatchProcessingTypes == BatchProcessingTypes.Discards)
                    .SingleOrDefault();

                var pageNumber = request.Page ?? 1;
                var q = request.Q?.ToLower();

                var bloodBags = context.BloodBagDonationDetails
                    .Where(x => x.BatchProcessing.Id == batchProcessing.Id && x.Discarded == true && x.Sent == true)
                    .AsNoTracking();
                var results = !string.IsNullOrWhiteSpace(q)
                        ? bloodBags
                            .Where(x => EF.Functions.Like(x.DonationEncounter.Donor.DonorNumber.ToLower(), $"%{q}%") || EF.Functions.Like(x.DonationEncounter.Donor.Name.ToLower(), $"%{q}%"))
                            .OrderBy(x => x.BloodBagLabel)
                        : bloodBags.OrderBy(x => x.BloodBagLabel);
                return Result.Success(await results.GetPagedAsync<BloodBagDonationDetail, BloodBagDonationDetailViewModel>(provider, pageNumber));
            }
        }
    }
}