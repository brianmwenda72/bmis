﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Domain.Enums;
using CSharpFunctionalExtensions;
using DHP.Contracts;
using DHP.Contracts.Events.BloodBank;
using MassTransit;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationSections
{
    public class DiscardDonation
    {
        public class Command : IRequest<Result>
        {
            [JsonIgnore]
            public int DonationEncounterId { get; set; }
            [JsonIgnore]
            public string FacilityCode { get; set; }

            public Command(int donationEncounterId, string facilityCode)
            {
                this.DonationEncounterId = donationEncounterId;
                this.FacilityCode = facilityCode;
            }
            public string DiscardReason { get; set; }
        }
        public class CommandHandler : IRequestHandler<Command, Result>
        {
            private readonly IMapper mapper;
            private readonly MainContext context;
            private readonly ISendEndpointProvider sendEndpointProvider;
            private readonly IHttpContextAccessor httpContextAccessor;
            private readonly Uri notificationForBloodDiscardServiceAddress = new($"queue:{QueueDefinitions.BMIS.NOT_SINGLE_BAGS_DONATION_QUEUE}");


            public CommandHandler(IMapper mapper, MainContext context, IHttpContextAccessor httpContextAccessor, ISendEndpointProvider sendEndpointProvider)
            {
                this.mapper = mapper;
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
                this.sendEndpointProvider = sendEndpointProvider;
            }

            public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
            {
                try
                {
                    string facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;

                    DonationEncounter donationEncounter = context.DonationEncounters
                        .Where(x => x.Id == request.DonationEncounterId && x.Facility.Code == facilityCode)
                        .Include(x => x.BloodBagDonationDetails)
                        .Include(x => x.Vacutainers)
                        .SingleOrDefault();
                    if (donationEncounter == null)
                    {
                        return Result.Failure("The Donation Encounter Id is invalid");
                    }
                    else
                    {
                        BloodBagDonationDetail bloodBagDonationDetail = context.BloodBagDonationDetails
                            .Include(x => x.DonationEncounter)
                                .ThenInclude(x => x.Donor)
                            .Where(x => x.DonationEncounter.Id == donationEncounter.Id && x.Discarded == false)
                            .SingleOrDefault();
                        if (bloodBagDonationDetail != null)
                        {

                            bloodBagDonationDetail.Discarded = true;
                            bloodBagDonationDetail.DonationEncounter = donationEncounter;
                            bloodBagDonationDetail.DiscardReason = request.DiscardReason;
                            bloodBagDonationDetail.BloodBagDonataionDetailStatus = BloodBagDonataionDetailStatus.Discarded;
                            context.BloodBagDonationDetails.Update(bloodBagDonationDetail);
                            await context.SaveChangesAsync();

                            DonationContract donationContract = new DonationContract()
                            {
                                DonationDate = bloodBagDonationDetail.CreatedOnUtc,
                                DonationNumber = bloodBagDonationDetail.DonationNumber,
                                DonorNumber = bloodBagDonationDetail.DonationEncounter.Donor.DonorNumber,
                                BloodBagType = bloodBagDonationDetail.BloodBagType
                            };
                            await (await sendEndpointProvider.GetSendEndpoint(notificationForBloodDiscardServiceAddress)).Send(donationContract, cancellationToken);


                            donationEncounter.DonationEncounterStatus = DonationEncounterStatus.Discard;
                            donationEncounter.DonationNumber = null;
                            donationEncounter.IsComplete = false;
                            context.DonationEncounters.Update(donationEncounter);
                            await context.SaveChangesAsync();
                        }
                        else
                        {
                            return Result.Failure("The Donation Number is Invalid");
                        }

                    }
                }
                catch (Exception ex)
                {

                    Console.WriteLine("Very bad things happened " + ex.Message);
                }

                return Result.Success();
            }
        }
    }
}
