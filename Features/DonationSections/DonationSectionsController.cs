﻿using BMIS.Donation.Abstracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationSections
{
    public class DonationSectionsController : BaseApiController
    {
        #region Active Donation Drive List within a Facility
        [HttpGet("donation-drive-list")]
        public async Task<IActionResult> GetDonationDriveList(int? page, string q, string facilityCode)
        {
            return Process(await Mediator.Send(new DonationDriveList.Query(page, q, facilityCode)));
        }
        #endregion

        #region Completed Counselling Donation Encounter List With Donors
        [HttpGet("{donationDriveGuid}/complete-counselling-donation-encounter-list")]
        public async Task<IActionResult> GetDonationEncounterList(int? page, string q, Guid donationDriveGuid, string facilityCode, string donorStatus)
        {
            return Process(await Mediator.Send(new DonationEncounterList.Query(page, q, donationDriveGuid, facilityCode, donorStatus)));
        }
        #endregion

        #region Add Bag and Vacutainers
        [HttpPost("{donationEncounterId}/add-bag-and-vacutainers")]
        public async Task<IActionResult> AddBagAndVacutainersAsync(int donationEncounterId, string facilityCode, AddBagAndVacutainers.Command command)
        {
            command.DonationEncounterId = donationEncounterId;
            command.FacilityCode = facilityCode;
            return Feedback(await Mediator.Send(command), ActionPerformed.Add);
        }
        #endregion

        #region Start Donation
        [HttpPost("{donationEncounterId}/start-donation")]
        public async Task<IActionResult> StartDonationAsync(int donationEncounterId, string facilityCode, StartDonation.Command command)
        {
            command.DonationEncounterId = donationEncounterId;
            command.FacilityCode = facilityCode;
            return Feedback(await Mediator.Send(command), ActionPerformed.Add);
        }
        #endregion

        #region Stop Donation
        [HttpPost("{donationEncounterId}/stop-donation")]
        public async Task<IActionResult> StopDonationAsync(int donationEncounterId, string facilityCode, StopDonation.Command command)
        {
            command.DonationEncounterId = donationEncounterId;
            command.FacilityCode = facilityCode;
            return Feedback(await Mediator.Send(command), ActionPerformed.Add);
        }
        #endregion

        #region Discard Donation
        [HttpPost("{donationEncounterId}/discard-donation")]
        public async Task<IActionResult> DiscardDonationAsync(int donationEncounterId, string facilityCode, DiscardDonation.Command command)
        {
            command.DonationEncounterId = donationEncounterId;
            command.FacilityCode = facilityCode;
            return Feedback(await Mediator.Send(command), ActionPerformed.Add);
        }
        #endregion

        #region Deferment Routes

        [HttpPost("{donationEncounterId}/defer")]
        public async Task<IActionResult> DeferDonationAtDonationAsync(int donationEncounterId, string facilityCode, DeferDonationAtDonation.Command command)
        {
            command.DonationEncounterId = donationEncounterId;
            command.FacilityCode = facilityCode;
            return Feedback(await Mediator.Send(command), ActionPerformed.Add);
        }

        #endregion

        #region Facility Option List
        [HttpGet("active-facilities")]
        public async Task<ActionResult<List<FacilityOptions.FacilityOptionsViewModel>>> GetFacilityOptions()
        {
            return await Mediator.Send(new FacilityOptions.Query());
        }
        #endregion

        #region End Donation Drive
        [HttpPost("{donationDriveGuid}/complete-donation-drive")]
        public async Task<IActionResult> EndDonationDriveAsync(Guid donationDriveGuid, string facilityCode, EndDonationDrive.Command command)
        {
            command.DonationDriveGuid = donationDriveGuid;
            command.FacilityCode = facilityCode;
            return Feedback(await Mediator.Send(command), ActionPerformed.Add);
        }
        #endregion

        #region Batch List of successfully sent items
        [HttpGet("{donationDriveGuid}/batch-list-of-successfully-sent-items")]
        public async Task<IActionResult> GetDispatchedBatchList(int? page, string q, Guid donationDriveGuid, string facilityCode, string batchType)
        {
            return Process(await Mediator.Send(new DispatchedBatchList.Query(page, q, donationDriveGuid, facilityCode, batchType)));
        }
        #endregion

        #region Sent Blood Bags Within a Batch
        [HttpGet("{batchGuid}/sent-blood-bags")]
        public async Task<IActionResult> GetSentDonationsBagList(int? page, string q, Guid batchGuid, string facilityCode)
        {
            return Process(await Mediator.Send(new SentDonationsBagList.Query(page, q, batchGuid, facilityCode)));
        }
        #endregion

        #region Sent Dicards Within a Batch
        [HttpGet("{batchGuid}/sent-discards")]
        public async Task<IActionResult> GetSentDonationsDiscardsList(int? page, string q, Guid batchGuid, string facilityCode)
        {
            return Process(await Mediator.Send(new SentDonationsDiscardsList.Query(page, q, batchGuid, facilityCode)));
        }
        #endregion

        #region Sent Vacutainers Within a Batch
        [HttpGet("{batchGuid}/sent-vacutainers")]
        public async Task<IActionResult> GetSentDonationsVacutainerList(int? page, string q, Guid batchGuid, string facilityCode)
        {
            return Process(await Mediator.Send(new SentDonationsVacutainerList.Query(page, q, batchGuid, facilityCode)));
        }
        #endregion

        #region Update Donation Batch
        [HttpPost("{batchGuid}/update-batch")]
        public async Task<IActionResult> UpdateBatchDestinationAsync(Guid batchGuid, string facilityCode, UpdateBatchDestination.Command command)
        {
            command.BatchGuid = batchGuid;
            command.FacilityCode = facilityCode;
            return Feedback(await Mediator.Send(command), ActionPerformed.Add);
        }
        #endregion

        #region Dispatch Donation Items
        [HttpPost("{donationDriveGuid}/dispatch-donation-items")]
        public async Task<IActionResult> DispatchDonationItemsAsync(Guid donationDriveGuid, string facilityCode, DispatchDonationItems.Command command)
        {
            command.DonationDriveGuid = donationDriveGuid;
            command.FacilityCode = facilityCode;
            return Feedback(await Mediator.Send(command), ActionPerformed.Add);
        }
        #endregion

        #region Resend Donation Items
        [HttpPost("{batchNumber}/resend-donation-items")]
        public async Task<IActionResult> ResendBatchAsync(string batchNumber, string facilityCode, ResendBatch.Command command)
        {
            command.BatchNumber = batchNumber;
            command.FacilityCode = facilityCode;
            return Feedback(await Mediator.Send(command), ActionPerformed.Add);
        }
        #endregion
    }
}

