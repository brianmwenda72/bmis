﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Defaults;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Domain.Enums;
using CSharpFunctionalExtensions;
using DHP.Contracts;
using DHP.Contracts.Commands;
using DHP.Contracts.Events.BloodBank;
using DHP.Contracts.Events.BmisComponents;
using DHP.Contracts.Models.Screening;
using MassTransit;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationSections
{
    public class ResendBatch
    {
        public class Command : IRequest<Result>
        {
            [JsonIgnore]
            public string BatchNumber { get; set; }
            [JsonIgnore]
            public string FacilityCode { get; set; }
            public Command(string batchNumber, string facilityCode)
            {
                this.BatchNumber = batchNumber;
                this.FacilityCode = facilityCode;
            }
        }
        public class CommandHandler : IRequestHandler<Command, Result>
        {
            private readonly IMapper mapper;
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;
            private readonly ISendEndpointProvider sendEndpointProvider;
            private readonly Uri bloodDonatedForSeparationServiceAddress = new($"queue:{QueueDefinitions.BMIS.BLOOD_DONATED_FOR_SEPARATION_QUEUE}");
            private readonly Uri screeningBatchServiceAddress = new($"queue:{QueueDefinitions.BMIS.SCREENING_CREATE_OR_UPDATE_VACUTAINER_QUEUE}");
            private readonly Uri bloodDonatedAndDiscardedServiceAddress = new($"queue:{QueueDefinitions.BMIS.BLOOD_DONATED_QUEUE}");


            public CommandHandler(IMapper mapper, MainContext context, ISendEndpointProvider sendEndpointProvider, IHttpContextAccessor httpContextAccessor)
            {
                this.mapper = mapper;
                this.context = context;
                this.sendEndpointProvider = sendEndpointProvider;
                this.httpContextAccessor = httpContextAccessor;
            }

            public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
            {
                string facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;
                string userName = httpContextAccessor.HttpContext?.User.FindFirst("full_name")?.Value;

                BatchProcessing batchProcessing = context.BatchProcessings
                    .Where(x => x.BatchNumber == request.BatchNumber)
                    .Include(x=> x.DonationDrive)
                    .Include(x=> x.Vacutainers)
                    .Include(x=> x.BloodBagDonationDetails)
                    .FirstOrDefault();
                if (batchProcessing != null)
                {
                    switch (batchProcessing.BatchProcessingTypes)
                    {
                        case BatchProcessingTypes.BloodBag:
                            List<BloodPintContract> bloodPintContractsList = new List<BloodPintContract>();

                            var completedBloodBagList = context.BloodBagDonationDetails
                                .Where(x => x.BatchProcessing == batchProcessing)
                                .Include(x => x.DonationEncounter)
                                .ThenInclude(x => x.Donor)
                                .ToList();
                            foreach (var bloodBag in completedBloodBagList)
                            {
                                BloodPintContract bloodPintContract = new BloodPintContract()
                                {
                                    DonationDate = DateTime.UtcNow,
                                    DonationNumber = bloodBag.DonationNumber,
                                    DonorNumber = bloodBag.DonationEncounter.Donor.DonorNumber,
                                    BloodBagType = bloodBag.BloodBagType,
                                    Quantity = bloodBag.QuantityCollected,
                                    QuantityUnit = bloodBag.QuantityUnit
                                };

                                bloodPintContractsList.Add(bloodPintContract);
                            }
                            BloodPintBatchContract bloodPintBatchContract = new()
                            {
                                BatchNumber = batchProcessing.BatchNumber,
                                BatchType = EntityDefault.BloodBagBatch,
                                DonationDriveName = batchProcessing.DonationDrive.SiteName,
                                DonationDriveCode = batchProcessing.DonationDrive.Code,
                                BloodPints = bloodPintContractsList,
                                DateSend = DateTime.UtcNow,
                                FacilityFrom = facilityCode,
                                FacilityTo = context.Facilities.FirstOrDefault(x=> x.Name.ToLower() == batchProcessing.FacilityToName.ToLower()).Code,
                                TeamLead = batchProcessing.DonationDrive.TeamLead
                            };
                            await (await sendEndpointProvider.GetSendEndpoint(bloodDonatedForSeparationServiceAddress)).Send(bloodPintBatchContract, cancellationToken);
                            break;
                        case BatchProcessingTypes.Vacutainer:

                            List<VacutainerContractModel> vacutainerContractModelsList = new List<VacutainerContractModel>();

                            var anyCompleteVacutainer = context.Vacutainers
                                    .Include(x => x.DonationEncounter)
                                    .ThenInclude(x => x.DonationDrive)
                                    .Where(x => x.BatchProcessing == batchProcessing)
                                    .ToList();
                            foreach (var item in anyCompleteVacutainer)
                            {
                                Vacutainer vacutainer = context.Vacutainers
                                          .Where(x => x.EntityGuid == item.EntityGuid)
                                          .Include(x => x.DonationEncounter)
                                          .ThenInclude(x => x.Donor)
                                          .SingleOrDefault();

                                VacutainerContractModel vacutainerContractModel = new VacutainerContractModel()
                                {
                                    DonorNumber = vacutainer.DonationEncounter.Donor.DonorNumber,
                                    Label = vacutainer.Label,
                                    Type = vacutainer.VacutainerType
                                };
                                vacutainerContractModelsList.Add(vacutainerContractModel);
                            }

                            CreateOrUpdateScreeningVacutainerCommand createOrUpdateScreeningVacutainerCommand = new CreateOrUpdateScreeningVacutainerCommand()
                            {
                                BatchNumber = batchProcessing.BatchNumber,
                                BatchType = anyCompleteVacutainer.Any(x=> x.VacutainerType.ToLower() == "purple-top".ToLower()) ? "purple-top" : "red-top",
                                DonationDriveCode = batchProcessing.DonationDrive.Code,
                                DonationDriveName = batchProcessing.DonationDrive.SiteName,
                                FacilityFrom = facilityCode,
                                FacilityTo = context.Facilities.FirstOrDefault(x => x.Name.ToLower() == batchProcessing.FacilityToName.ToLower()).Code,
                                Vacutainers = vacutainerContractModelsList,
                                TeamLead = batchProcessing.DonationDrive.TeamLead
                            };
                            await (await sendEndpointProvider.GetSendEndpoint(screeningBatchServiceAddress)).Send(createOrUpdateScreeningVacutainerCommand, cancellationToken);
                            break;
                        case BatchProcessingTypes.Discards:

                            List<DonationContract> donationContractsList = new List<DonationContract>();

                            var discardedBloodBagsList = context.BloodBagDonationDetails
                            .Where(x => x.BatchProcessing == batchProcessing)
                            .Include(x => x.DonationEncounter)
                            .ThenInclude(x => x.DonationDrive)
                            .Include(x => x.DonationEncounter)
                            .ThenInclude(x => x.Donor)
                            .Include(x => x.DonationEncounter)
                            .ThenInclude(x => x.Vacutainers)
                            .ToList();

                            foreach (var discardBag in discardedBloodBagsList)
                            {

                                DonationContract donationContract = new DonationContract()
                                {
                                    DonationDate = discardBag.CreatedOnUtc,
                                    DonationNumber = discardBag.DonationNumber,
                                    DonorNumber = discardBag.DonationEncounter.Donor.DonorNumber,
                                    BloodBagType = discardBag.BloodBagType,
                                    Note = discardBag.DiscardReason,
                                    BloodComponents = new List<BloodComponentContract>()
                                    {
                                        new BloodComponentContract
                                        {
                                            ComponentNumber = EntityDefault.WholeBloodCode,
                                            Name = EntityDefault.WholeBloodName,
                                            PreparedBy = discardBag.CollectedBy,
                                            DatePrepared = DateTime.UtcNow,
                                            Note = discardBag.DiscardReason,
                                            Quantity = discardBag.QuantityCollected,
                                            QuantityUnit = discardBag.QuantityUnit
                                        }
                                    }
                                };
                                donationContractsList.Add(donationContract);
                            }

                            DonationBatchContract bloodDonatedBatch = new()
                            {
                                BatchNumber = batchProcessing.BatchNumber,
                                BatchType = EntityDefault.DiscardBatch,
                                DonationDriveName = batchProcessing.DonationDrive.SiteName,
                                DonationDriveCode = batchProcessing.DonationDrive.Code,
                                Donations = donationContractsList,
                                DateSend = DateTime.UtcNow,
                                FacilityFrom = facilityCode,
                                FacilityTo = facilityCode,
                                TeamLead = batchProcessing.DonationDrive.TeamLead
                            };
                            await (await sendEndpointProvider.GetSendEndpoint(bloodDonatedAndDiscardedServiceAddress)).Send(bloodDonatedBatch, cancellationToken);

                            break;
                        default:
                            break;
                    }

                    return Result.Success();
                }
                else
                {
                    return Result.Failure("Batch not found");
                }
            }
        }
    }
}
