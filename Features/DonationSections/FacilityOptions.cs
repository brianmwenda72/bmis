﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using System.Collections.Generic;
using BMIS.Donation.Data;
using System.Linq;

namespace BMIS.Donation.Features.DonationSections
{
    public class FacilityOptions
    {
        public class Query : IRequest<List<FacilityOptionsViewModel>>
        {
            public Query() { }
        }
        public class FacilityOptionsViewModel
        {
            public string Label { get; set; }
            public string Value { get; set; }
        }

        public class QueryHandler : IRequestHandler<Query, List<FacilityOptionsViewModel>>
        {
            private readonly IConfigurationProvider provider;
            private readonly MainContext context;

            public QueryHandler(IConfigurationProvider provider, MainContext context)
            {
                this.provider = provider;
                this.context = context;
            }

            public async Task<List<FacilityOptionsViewModel>> Handle(Query request, CancellationToken cancellationToken)
            {
                var facilityOptions = await context.Facilities
                    .Where(x=> x.Deleted == false)
                    .ProjectTo<FacilityOptionsViewModel>(provider)
                    .ToListAsync(cancellationToken: cancellationToken);


                return facilityOptions;
            }
        }
    }
}