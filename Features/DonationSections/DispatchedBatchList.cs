﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Defaults;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Domain.Enums;
using BMIS.Donation.Extensions.Paging;
using BMIS.Donation.Models.ProcessingBatches;
using CSharpFunctionalExtensions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationSections
{
    public class DispatchedBatchList
    {
        public class Query : IRequest<Result<PagedResult<BatchProcessingViewModel>>>
        {
            public Query(int? page, string s, Guid donationDriveGuid, string facilityCode, string batchType)
            {
                Page = page;
                Q = s;
                DonationDriveGuid = donationDriveGuid;
                FacilityCode = facilityCode;
                BatchType = batchType;
            }

            public int? Page { get; set; }
            public string Q { get; set; }
            public Guid DonationDriveGuid { get; set; }
            public string FacilityCode { get; set; }
            public string BatchType { get; set; }
        }

        public class QueryHandler : IRequestHandler<Query, Result<PagedResult<BatchProcessingViewModel>>>
        {
            private readonly IConfigurationProvider provider;
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;

            public QueryHandler(IConfigurationProvider provider, MainContext context, IHttpContextAccessor httpContextAccessor)
            {
                this.provider = provider;
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
            }

            public async Task<Result<PagedResult<BatchProcessingViewModel>>> Handle(Query request, CancellationToken cancellationToken)
            {
                string facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;

                DonationDrive donationDrive = context.DonationDrives
                    .Where(x => x.EntityGuid == request.DonationDriveGuid && x.Facility.Code == facilityCode && x.DonationDriveStatus == DonationDriveStatus.Active)
                    .Include(x=> x.DonationEncounters)
                    .ThenInclude(x=> x.BloodBagDonationDetails)
                    .SingleOrDefault();

                var pageNumber = request.Page ?? 1;
                var q = request.Q?.ToLower();
                var batchType = request.BatchType?.ToLower();

                var batch = context.BatchProcessings
                    .Where(x=> x.DonationDrive.Id == donationDrive.Id && x.Sent == true && x.DonationDrive.DonationDriveStatus == DonationDriveStatus.Active)
                    .Include(x=> x.BloodBagDonationDetails)
                    .Include(x=> x.Vacutainers)
                    .AsNoTracking();

                IQueryable<BatchProcessing> results;

                if (!string.IsNullOrWhiteSpace(batchType) && !string.IsNullOrWhiteSpace(q))
                {
                    results = !string.IsNullOrWhiteSpace(batchType) && !string.IsNullOrWhiteSpace(q)
                        ? batch
                            .Where(x => EF.Functions.Like(x.BatchNumber.ToLower(), $"%{q}%"))
                            .OrderByDescending(x => x.CreatedOnUtc)
                        : batch.OrderByDescending(x => x.CreatedOnUtc);
                }
                else if (!string.IsNullOrWhiteSpace(batchType) && batchType.ToLower() == EntityDefault.BloodBagBatch.ToLower())
                {
                    results = !string.IsNullOrWhiteSpace(batchType)
                        ? batch
                            .Where(x => x.BatchProcessingTypes == BatchProcessingTypes.BloodBag)
                            .OrderByDescending(x => x.CreatedOnUtc)
                        : batch.OrderByDescending(x => x.CreatedOnUtc);
                }
                else if (!string.IsNullOrWhiteSpace(batchType) && batchType.ToLower() == EntityDefault.VacutainerBatch.ToLower())
                {
                    results = !string.IsNullOrWhiteSpace(batchType)
                        ? batch
                            .Where(x=> x.BatchProcessingTypes == BatchProcessingTypes.Vacutainer)
                            .OrderByDescending(x => x.CreatedOnUtc)
                        : batch.OrderByDescending(x => x.CreatedOnUtc);
                }
                else if (!string.IsNullOrWhiteSpace(batchType) && batchType.ToLower() == EntityDefault.DiscardBatch.ToLower())
                {
                    results = !string.IsNullOrWhiteSpace(batchType)
                        ? batch
                            .Where(x => x.BatchProcessingTypes == BatchProcessingTypes.Discards)
                            .OrderByDescending(x => x.CreatedOnUtc)
                        : batch.OrderByDescending(x => x.CreatedOnUtc);
                }
                else if (!string.IsNullOrWhiteSpace(q))
                {
                    results = !string.IsNullOrWhiteSpace(q)
                        ? batch
                            .Where(x => EF.Functions.Like(x.BatchNumber.ToLower(), $"%{q}%"))
                            .OrderByDescending(x => x.CreatedOnUtc)
                        : batch.OrderByDescending(x => x.CreatedOnUtc);
                }
                else
                {
                    results = batch.OrderByDescending(x=> x.CreatedOnUtc);
                }
                return await results.GetPagedAsync<BatchProcessing, BatchProcessingViewModel>(provider, pageNumber);
            }
        }
    }
}