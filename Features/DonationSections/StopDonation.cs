﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Defaults;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Domain.Enums;
using CSharpFunctionalExtensions;
using DHP.Contracts;
using DHP.Contracts.Events.BloodBank;
using MassTransit;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using RulesEngine.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationSections
{
    public class StopDonation
    {
        public class Command : IRequest<Result>
        {
            [JsonIgnore]
            public int DonationEncounterId { get; set; }
            [JsonIgnore]
            public string FacilityCode { get; set; }

            public Command(int donationEncounterId, string facilityCode)
            {
                this.DonationEncounterId = donationEncounterId;
                this.FacilityCode = facilityCode;
            }
            public int QuantityCollected { get; set; }
            public string QuantityUnit { get; set; }
            public decimal Weight { get; set; }
            public string WeightUnit { get; set; }
            public DateTime DonationStopTime { get; set; }
        }
        public class CommandHandler : IRequestHandler<Command, Result>
        {
            private readonly IMapper mapper;
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;
            private readonly ISendEndpointProvider sendEndpointProvider;
            private readonly Uri donatedDonorServiceAddress = new($"queue:{QueueDefinitions.BMIS.DONOR_DONATED_QUEUE}");
            private readonly Uri notificationForBloodDonatedForSeparationServiceAddress = new($"queue:{QueueDefinitions.BMIS.NOT_SINGLE_BAGS_DONATION_QUEUE}");


            public CommandHandler(IMapper mapper, MainContext context, IHttpContextAccessor httpContextAccessor, ISendEndpointProvider sendEndpointProvider)
            {
                this.mapper = mapper;
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
                this.sendEndpointProvider = sendEndpointProvider;
            }

            public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
            {
                try
                {
                    string facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;
                    string userName = httpContextAccessor.HttpContext?.User.FindFirst("full_name")?.Value;

                    Facility facility = context.Facilities
                        .SingleOrDefault(x => x.Code == facilityCode);

                    DonationEncounter donationEncounter = context.DonationEncounters
                        .Include(x=> x.Questionnaire)
                        .ThenInclude(x=> x.Declaration)
                        .Where(x => x.Id == request.DonationEncounterId && x.IsComplete == false && x.Facility.Code == facilityCode)
                        .Include(x => x.BloodBagDonationDetails)
                        .SingleOrDefault();
                    if (donationEncounter == null)
                    {
                        return Result.Failure("The Donation Encounter Id is invalid");
                    }
                    else
                    {
                        BloodBagDonationDetail bloodBagDonationDetail = context.BloodBagDonationDetails
                            .Include(x => x.DonationEncounter)
                                .ThenInclude(x => x.Donor)
                            .Where(x => x.DonationEncounter.Id == donationEncounter.Id && x.Discarded == false)
                            .SingleOrDefault();

                        if (bloodBagDonationDetail != null)
                        {
                            bloodBagDonationDetail.QuantityCollected = request.QuantityCollected;
                            bloodBagDonationDetail.QuantityUnit = request.QuantityUnit;
                            bloodBagDonationDetail.Weight = request.Weight;
                            bloodBagDonationDetail.WeightUnit = request.WeightUnit;
                            bloodBagDonationDetail.BloodBagDonataionDetailStatus = BloodBagDonataionDetailStatus.NeedleOut;
                            bloodBagDonationDetail.NeedleTimeOut = request.DonationStopTime.ToUniversalTime();
                            bloodBagDonationDetail.CollectedBy = userName;
                            context.BloodBagDonationDetails.Update(bloodBagDonationDetail);
                            await context.SaveChangesAsync();

                            

                            donationEncounter.DonationStopTime = (DateTime)bloodBagDonationDetail.NeedleTimeOut;
                            context.DonationEncounters.Update(donationEncounter);
                            await context.SaveChangesAsync();



                            var jsontext = await File.ReadAllTextAsync(@"WorkFlowRules/eligibility.json", cancellationToken);
                            var permissions = JsonConvert.DeserializeObject<List<Workflow>>(jsontext);
                            var rulesEngine = new RulesEngine.RulesEngine(permissions?.ToArray());
                            var resultList = await rulesEngine.ExecuteAllRulesAsync("QuantityEligibility", bloodBagDonationDetail);
                            var response = resultList.Where(ruleResultTree => !ruleResultTree.IsSuccess).Aggregate("", (current, ruleResultTree) => $"{current}{ruleResultTree.ExceptionMessage}");

                            if (string.IsNullOrWhiteSpace(response))
                            {
                                donationEncounter.DonationEncounterStatus = DonationEncounterStatus.CompleteDonation;
                                donationEncounter.IsComplete = true;
                                context.DonationEncounters.Update(donationEncounter);
                                await context.SaveChangesAsync();

                                if (bloodBagDonationDetail.DonationEncounter.Donor.Gender.ToLower() == EntityDefault.MaleGender.ToLower())
                                {
                                    DonorDonationsContract donorDonationsContract = new DonorDonationsContract()
                                    {
                                        DonorNumber = bloodBagDonationDetail.DonationEncounter.Donor.DonorNumber,
                                        DonationDate = donationEncounter.DonationStopTime,
                                        DonationNumber = bloodBagDonationDetail.DonationNumber,
                                        NextDonationDate = donationEncounter.DonationStopTime.AddDays(90),
                                        Note = $"Donor donated {bloodBagDonationDetail.QuantityCollected} {bloodBagDonationDetail.QuantityUnit} of blood successfully",
                                        FacilityDonatedFrom = facility.Name,
                                        CreatedBy = userName
                                    };
                                    await (await sendEndpointProvider.GetSendEndpoint(donatedDonorServiceAddress)).Send(donorDonationsContract, cancellationToken);
                                }
                                else
                                {
                                    DonorDonationsContract donorDonationsContract = new DonorDonationsContract()
                                    {
                                        DonorNumber = bloodBagDonationDetail.DonationEncounter.Donor.DonorNumber,
                                        DonationDate = donationEncounter.DonationStopTime,
                                        DonationNumber = bloodBagDonationDetail.DonationNumber,
                                        NextDonationDate = donationEncounter.DonationStopTime.AddDays(120),
                                        Note = $"Donor donated {bloodBagDonationDetail.QuantityCollected} {bloodBagDonationDetail.QuantityUnit} of blood successfully",
                                        FacilityDonatedFrom = facility.Name,
                                        CreatedBy = userName
                                    };
                                    await (await sendEndpointProvider.GetSendEndpoint(donatedDonorServiceAddress)).Send(donorDonationsContract, cancellationToken);
                                }

                                DonationContract donationContract = new DonationContract()
                                {
                                    DonationDate = DateTime.UtcNow,
                                    DonationNumber = bloodBagDonationDetail.DonationNumber,
                                    DonationType = donationEncounter.Questionnaire.Declaration.DonationType,
                                    DonorNumber = bloodBagDonationDetail.DonationEncounter.Donor.DonorNumber,
                                    BloodBagType = bloodBagDonationDetail.BloodBagType
                                };
                                await (await sendEndpointProvider.GetSendEndpoint(notificationForBloodDonatedForSeparationServiceAddress)).Send(donationContract, cancellationToken);

                                return Result.Success();
                            }

                            return Result.Failure(response);
                        }
                        else
                        {
                            return Result.Failure("The Donation Number is Invalid");
                        }

                    }
                }
                catch (Exception ex)
                {

                    Console.WriteLine("Very bad things happened " + ex.Message);
                }

                return Result.Success();
            }
        }
    }
}
