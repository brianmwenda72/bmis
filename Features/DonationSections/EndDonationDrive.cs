﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Domain.Enums;
using CSharpFunctionalExtensions;
using DHP.Contracts;
using DHP.Contracts.Commands;
using MassTransit;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationSections
{
    public class EndDonationDrive
    {
        public class Command : IRequest<Result>
        {
            [JsonIgnore]
            public Guid DonationDriveGuid { get; set; }
            [JsonIgnore]
            public string FacilityCode { get; set; }

            public Command(Guid donationDriveGuid, string facilityCode)
            {
                this.DonationDriveGuid = donationDriveGuid;
                this.FacilityCode = facilityCode;
            }
        }
        public class CommandHandler : IRequestHandler<Command, Result>
        {
            private readonly IMapper mapper;
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;
            private readonly ISendEndpointProvider sendEndpointProvider;
            private readonly Uri endDonationDriveServiceAddressAtRecruitment = new($"queue:{QueueDefinitions.BMIS.ENDED_DONATION_DRIVE_QUEUE}");
            private readonly Uri endDonationDriveServiceAddressAtRegistration = new($"queue:{QueueDefinitions.BMIS.REGISTRATION_CREATE_OR_UPDATE_DONATION_DRIVE_QUEUE}");


            public CommandHandler(IMapper mapper, MainContext context, IHttpContextAccessor httpContextAccessor, ISendEndpointProvider sendEndpointProvider)
            {
                this.mapper = mapper;
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
                this.sendEndpointProvider = sendEndpointProvider;
            }

            public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
            {
                try
                {
                    string facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;

                    DonationDrive donationDrive = context.DonationDrives
                        .Where(x => x.EntityGuid == request.DonationDriveGuid && x.Facility.Code == facilityCode)
                        .Include(x => x.BatchProcessings)
                        .Include(x => x.DonationEncounters)
                        .ThenInclude(x => x.Vacutainers)
                        .Include(x => x.DonationEncounters)
                        .ThenInclude(x => x.BloodBagDonationDetails)
                        .SingleOrDefault();
                    if (donationDrive == null)
                    {
                        return Result.Failure("The Donation Drive does not exist");
                    }
                    else
                    {
                        var donationEncounterChecker = context.DonationEncounters
                            .Where(x=> x.DonationDrive.EntityGuid == donationDrive.EntityGuid)
                            .Any(x => x.IsComplete == false);

                        var bloodBagChecker = context.BloodBagDonationDetails
                            .Where(x => x.DonationEncounter.DonationDrive.EntityGuid == donationDrive.EntityGuid)
                            .Any(x => x.Sent == false);

                        var vacutainerChecker = context.Vacutainers
                            .Where(x => x.DonationEncounter.DonationDrive.EntityGuid == donationDrive.EntityGuid)
                            .Any(x => x.Sent == false);

                        var batchChecker = context.BatchProcessings
                            .Where(x => x.DonationDrive.EntityGuid == donationDrive.EntityGuid)
                            .Any(x => x.Received == false);

                        if (!donationEncounterChecker && !bloodBagChecker && !vacutainerChecker)// && !batchChecker)
                        {
                            donationDrive.DonationDriveStatus = DonationDriveStatus.Complete;
                            context.DonationDrives.Update(donationDrive);
                            await context.SaveChangesAsync();

                            var bloodBagList = context.BloodBagDonationDetails
                                .Where(x => x.DonationEncounter.DonationDrive.EntityGuid == donationDrive.EntityGuid && x.DonationEncounter.IsComplete == true && x.Discarded == false)
                                .ToList();

                            EndDonationDriveCommand endDonationDriveCommand = new ()
                            {
                                EntityGuid = donationDrive.EntityGuid,
                                TotalBloodBagsCollected = bloodBagList.Count(),
                                DriveEndTime = DateTime.UtcNow
                            };
                            await (await sendEndpointProvider.GetSendEndpoint(endDonationDriveServiceAddressAtRecruitment)).Send(endDonationDriveCommand, cancellationToken);

                            CreateOrUpdateDonationDriveCommand createOrUpdateDonationDriveCommand = new()
                            {
                                EntityGuid = donationDrive.EntityGuid,
                                Status = "Complete"
                            };
                            await (await sendEndpointProvider.GetSendEndpoint(endDonationDriveServiceAddressAtRegistration)).Send(createOrUpdateDonationDriveCommand, cancellationToken);
                        }
                        else
                        {
                            return Result.Failure("Cannot mark the drive as completed. Kindly check for incomplete donation encounters, unreceived batches or unsent bags and vacutainers.");
                        }
                    }
                }
                catch (Exception ex)
                {

                    Console.WriteLine("Very bad things happened " + ex.Message);
                }

                return Result.Success();
            }
        }
    }
}
