﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Defaults;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Domain.Enums;
using BMIS.Donation.Extensions.Paging;
using BMIS.Donation.Models.BloodBagDonationDetails;
using CSharpFunctionalExtensions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationSections
{
    public class CompletedDonationsBagList
    {
        public class Query : IRequest<Result<PagedResult<BloodBagDonationDetailViewModel>>>
        {
            public Query(int? page, string s, Guid donationDriveGuid, string facilityCode, string bloodBagType)
            {
                Page = page;
                Q = s;
                DonationDriveGuid = donationDriveGuid;
                FacilityCode = facilityCode;
                BloodBagType = bloodBagType;
            }

            public int? Page { get; set; }
            public string Q { get; set; }
            public Guid DonationDriveGuid { get; set; }
            public string FacilityCode { get; set; }
            public string BloodBagType { get; set; }
        }

        public class QueryHandler : IRequestHandler<Query, Result<PagedResult<BloodBagDonationDetailViewModel>>>
        {
            private readonly IConfigurationProvider provider;
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;

            public QueryHandler(IConfigurationProvider provider, MainContext context, IHttpContextAccessor httpContextAccessor)
            {
                this.provider = provider;
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
            }

            public async Task<Result<PagedResult<BloodBagDonationDetailViewModel>>> Handle(Query request, CancellationToken cancellationToken)
            {
                string facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;

                DonationDrive donationDrive = context.DonationDrives
                    .Where(x => x.EntityGuid == request.DonationDriveGuid && x.Facility.Code == facilityCode && x.DonationDriveStatus == DonationDriveStatus.Active)
                    .Include(x=> x.DonationEncounters)
                    .ThenInclude(x=> x.BloodBagDonationDetails)
                    .SingleOrDefault();

                var pageNumber = request.Page ?? 1;
                var q = request.Q?.ToLower();
                var bloodbagType = request.BloodBagType?.ToLower();

                var completedDonationBags = context.BloodBagDonationDetails
                    .Where(x=> x.DonationEncounter.DonationDrive.Id == donationDrive.Id && x.Discarded == false && x.BloodBagDonataionDetailStatus == BloodBagDonataionDetailStatus.NeedleOut && x.Sent == false)
                    .AsNoTracking();

                IQueryable<BloodBagDonationDetail> results;

                if (!string.IsNullOrWhiteSpace(bloodbagType) && !string.IsNullOrWhiteSpace(q))
                {
                    results = !string.IsNullOrWhiteSpace(bloodbagType) && !string.IsNullOrWhiteSpace(q)
                        ? completedDonationBags
                            .Where(x => EF.Functions.Like(x.DonationNumber.ToLower(), $"%{q}%"))
                            .OrderBy(x => x.DonationNumber)
                            .ThenBy(x => x.CreatedOnUtc)
                        : completedDonationBags.OrderBy(x => x.DonationNumber);
                }
                else if (!string.IsNullOrWhiteSpace(bloodbagType) && bloodbagType.ToLower() == EntityDefault.SingleBags.ToLower())
                {
                    results = !string.IsNullOrWhiteSpace(bloodbagType)
                        ? completedDonationBags
                            .Where(x => x.BloodBagType.ToLower() == EntityDefault.SingleBloodBags.ToLower())
                            .OrderBy(x => x.DonationNumber)
                            .ThenBy(x => x.CreatedOnUtc)
                        : completedDonationBags.OrderBy(x => x.DonationNumber);
                }
                else if (!string.IsNullOrWhiteSpace(bloodbagType) && bloodbagType.ToLower() == EntityDefault.OtherBags.ToLower())
                {
                    results = !string.IsNullOrWhiteSpace(bloodbagType)
                        ? completedDonationBags
                            .Where(x=> x.BloodBagType.ToLower() != EntityDefault.SingleBloodBags.ToLower())
                            .OrderBy(x => x.DonationNumber)
                            .ThenBy(x => x.CreatedOnUtc)
                        : completedDonationBags.OrderBy(x => x.DonationNumber);
                }
                else if (!string.IsNullOrWhiteSpace(q))
                {
                    results = !string.IsNullOrWhiteSpace(q)
                        ? completedDonationBags
                            .Where(x => EF.Functions.Like(x.DonationNumber.ToLower(), $"%{q}%"))
                            .OrderBy(x => x.DonationNumber)
                            .ThenBy(x => x.CreatedOnUtc)
                        : completedDonationBags.OrderBy(x => x.DonationNumber);
                }
                else
                {
                    results = completedDonationBags;
                }
                return await results.GetPagedAsync<BloodBagDonationDetail, BloodBagDonationDetailViewModel>(provider, pageNumber);
            }
        }
    }
}