﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Domain.Enums;
using CSharpFunctionalExtensions;
using DHP.Contracts;
using DHP.Contracts.Commands;
using MassTransit;
using MediatR;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationSections
{
    public class UpdateBatchDestination
    {
        public class Command : IRequest<Result>
        {
            [JsonIgnore]
            public Guid BatchGuid { get; set; }
            [JsonIgnore]
            public string FacilityCode { get; set; }

            public Command(Guid batchGuid, string facilityCode)
            {
                this.BatchGuid = batchGuid;
                this.FacilityCode = facilityCode;
            }
            public string FacilityTo { get; set; }
            public string TeamLead { get; set; }
        }
        public class CommandHandler : IRequestHandler<Command, Result>
        {
            private readonly IMapper mapper;
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;
            private readonly ISendEndpointProvider sendEndpointProvider;
            private readonly Uri screeningBatchServiceAddress = new($"queue:{QueueDefinitions.BMIS.SCREENING_CREATE_OR_UPDATE_VACUTAINER_QUEUE}");


            public CommandHandler(IMapper mapper, MainContext context, IHttpContextAccessor httpContextAccessor, ISendEndpointProvider sendEndpointProvider)
            {
                this.mapper = mapper;
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
                this.sendEndpointProvider = sendEndpointProvider;
            }

            public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
            {
                try
                {
                    string facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;
                    string userName = httpContextAccessor.HttpContext?.User.FindFirst("full_name")?.Value;

                    Facility facilityTo = context.Facilities
                                .SingleOrDefault(x => x.Code == request.FacilityTo);

                    BatchProcessing batchProcessing = context.BatchProcessings
                        .Where(x => x.EntityGuid == request.BatchGuid && x.Sent == true && x.Received == false)
                        .SingleOrDefault();
                    if (batchProcessing == null)
                    {
                        return Result.Failure("The Batch does not exist");
                    }
                    else
                    {
                        batchProcessing.FacilityToName = facilityTo.Name;
                        batchProcessing.TeamLead = request.TeamLead;
                        batchProcessing.DispatchedBy = userName;

                        context.BatchProcessings.Update(batchProcessing);
                        await context.SaveChangesAsync();

                        if (batchProcessing.BatchProcessingTypes == BatchProcessingTypes.Vacutainer)
                        {
                            CreateOrUpdateScreeningVacutainerCommand createOrUpdateScreeningVacutainerCommand = new CreateOrUpdateScreeningVacutainerCommand()
                            {
                                BatchNumber = batchProcessing.BatchNumber,
                                FacilityTo = facilityTo.Code,
                                TeamLead = batchProcessing.TeamLead
                            };
                            await (await sendEndpointProvider.GetSendEndpoint(screeningBatchServiceAddress)).Send(createOrUpdateScreeningVacutainerCommand, cancellationToken);
                        }
                    }
                }
                catch (Exception ex)
                {

                    Console.WriteLine("Very bad things happened " + ex.Message);
                }

                return Result.Success();
            }
        }
    }
}
