﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Domain.Enums;
using BMIS.Donation.Extensions.Paging;
using BMIS.Donation.Models.Vacutainers;
using CSharpFunctionalExtensions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationSections
{
    public class CompletedDonationsVacutainerList
    {
        public class Query : IRequest<Result<PagedResult<VacutainerViewModel>>>
        {
            public Query(int? page, string s, Guid donationDriveGuid, string facilityCode, string vacutainerType)
            {
                Page = page;
                Q = s;
                DonationDriveGuid = donationDriveGuid;
                FacilityCode = facilityCode;
                VacutainerType = vacutainerType;
            }

            public int? Page { get; set; }
            public string Q { get; set; }
            public Guid DonationDriveGuid { get; set; }
            public string FacilityCode { get; set; }
            public string VacutainerType { get; set; }
        }

        public class QueryHandler : IRequestHandler<Query, Result<PagedResult<VacutainerViewModel>>>
        {
            private readonly IConfigurationProvider provider;
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;

            public QueryHandler(IConfigurationProvider provider, MainContext context, IHttpContextAccessor httpContextAccessor)
            {
                this.provider = provider;
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
            }

            public async Task<Result<PagedResult<VacutainerViewModel>>> Handle(Query request, CancellationToken cancellationToken)
            {
                string facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;

                DonationDrive donationDrive = context.DonationDrives
                    .Where(x => x.EntityGuid == request.DonationDriveGuid && x.Facility.Code == facilityCode && x.DonationDriveStatus == DonationDriveStatus.Active)
                    .Include(x=> x.DonationEncounters)
                    .ThenInclude(x=> x.Vacutainers)
                    .SingleOrDefault();

                var pageNumber = request.Page ?? 1;
                var q = request.Q?.ToLower();
                var vacutainerType = request.VacutainerType?.ToLower();

                var completedDonationVacutainers = context.Vacutainers
                    .Where(x=> x.DonationEncounter.DonationDrive.Id == donationDrive.Id && x.Discarded == false && x.DonationEncounter.IsComplete == true && x.Sent == false)
                    .AsNoTracking();

                IQueryable<Vacutainer> results;

                if (!string.IsNullOrWhiteSpace(vacutainerType) && !string.IsNullOrWhiteSpace(q))
                {
                    results = !string.IsNullOrWhiteSpace(vacutainerType) && !string.IsNullOrWhiteSpace(q)
                        ? completedDonationVacutainers
                            .Where(x => EF.Functions.Like(x.Label.ToLower(), $"%{q}%"))
                            .OrderBy(x => x.Label)
                            .ThenBy(x => x.CreatedOnUtc)
                        : completedDonationVacutainers.OrderBy(x => x.Label);
                }
                else if (!string.IsNullOrWhiteSpace(vacutainerType))
                {
                    results = !string.IsNullOrWhiteSpace(vacutainerType)
                        ? completedDonationVacutainers
                            .Where(x => x.VacutainerType.ToLower() == vacutainerType.ToLower())
                            .OrderBy(x => x.Label)
                            .ThenBy(x => x.CreatedOnUtc)
                        : completedDonationVacutainers.OrderBy(x => x.Label);
                }
                else if (!string.IsNullOrWhiteSpace(q))
                {
                    results = !string.IsNullOrWhiteSpace(q)
                        ? completedDonationVacutainers
                            .Where(x => EF.Functions.Like(x.Label.ToLower(), $"%{q}%"))
                            .OrderBy(x => x.Label)
                            .ThenBy(x => x.CreatedOnUtc)
                        : completedDonationVacutainers.OrderBy(x => x.Label);
                }
                else
                {
                    results = completedDonationVacutainers;
                }

                return await results.GetPagedAsync<Vacutainer, VacutainerViewModel>(provider, pageNumber);
            }
        }
    }
}