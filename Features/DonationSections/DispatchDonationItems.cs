﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Defaults;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Domain.Enums;
using CSharpFunctionalExtensions;
using DHP.Contracts;
using DHP.Contracts.Commands;
using DHP.Contracts.Events.BloodBank;
using DHP.Contracts.Events.BmisComponents;
using DHP.Contracts.Models.Screening;
using Humanizer;
using Humanizer.Localisation;
using MassTransit;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationSections
{
    public class DispatchDonationItems
    {
        public class Command : IRequest<Result>
        {
            [JsonIgnore]
            public Guid DonationDriveGuid { get; set; }
            [JsonIgnore]
            public string FacilityCode { get; set; }

            public Command(Guid donationDriveGuid, string facilityCode)
            {
                this.DonationDriveGuid = donationDriveGuid;
                this.FacilityCode = facilityCode;
            }
            public List<VacutainerDispatch> VacutainerDispatches { get; set; }
            public bool ComfirmBagDispatch { get; set; }
            public bool ConfirmDiscardDispatch { get; set; }
        }
        public class VacutainerDispatch
        {
            public string VacutainerType { get; set; }
            public string FacilityTo { get; set; }
        }
        public class CommandHandler : IRequestHandler<Command, Result>
        {
            private readonly IMapper mapper;
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;
            private readonly ISendEndpointProvider sendEndpointProvider;
            private readonly Uri bloodDonatedForSeparationServiceAddress = new($"queue:{QueueDefinitions.BMIS.BLOOD_DONATED_FOR_SEPARATION_QUEUE}");
            private readonly Uri screeningBatchServiceAddress = new($"queue:{QueueDefinitions.BMIS.SCREENING_CREATE_OR_UPDATE_VACUTAINER_QUEUE}");
            private readonly Uri bloodDonatedAndDiscardedServiceAddress = new($"queue:{QueueDefinitions.BMIS.BLOOD_DONATED_QUEUE}");


            public CommandHandler(IMapper mapper, MainContext context, IHttpContextAccessor httpContextAccessor, ISendEndpointProvider sendEndpointProvider)
            {
                this.mapper = mapper;
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
                this.sendEndpointProvider = sendEndpointProvider;
            }

            public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
            {
                string facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;
                string userName = httpContextAccessor.HttpContext?.User.FindFirst("full_name")?.Value;
                try
                {
                    DonationDrive donationDrive = context.DonationDrives
                    .Include(x => x.Facility)
                    .Include(x => x.DonationEncounters)
                    .ThenInclude(x => x.Vacutainers)
                    .Include(x => x.DonationEncounters)
                    .ThenInclude(x => x.BloodBagDonationDetails)
                    .Where(x => x.EntityGuid == request.DonationDriveGuid && x.Facility.Code == facilityCode)
                    .SingleOrDefault();
                    if (donationDrive != null)
                    {

                        #region Dispatch Vacutainers
                        if (request.VacutainerDispatches != null)
                        {
                            foreach (var vacutainerDispatch in request.VacutainerDispatches)
                            {
                                Facility facilityTo = context.Facilities
                                    .SingleOrDefault(x => x.Code == vacutainerDispatch.FacilityTo);

                                var anyCompleteVacutainer = context.Vacutainers
                                    .Include(x => x.DonationEncounter)
                                    .ThenInclude(x => x.DonationDrive)
                                    .Where(x => x.DonationEncounter.IsComplete == true && x.DonationEncounter.DonationDrive.EntityGuid == donationDrive.EntityGuid && x.Sent == false && x.VacutainerType.ToLower() == vacutainerDispatch.VacutainerType.ToLower())
                                    .OrderBy(x=>x.Id)
                                    .ToList();

                                if (anyCompleteVacutainer.Count() == 0)
                                {
                                    return Result.Failure($"No {vacutainerDispatch.VacutainerType} vacutainers to dispatch within this donation drive");
                                }
                                else
                                {
                                    #region Generate Unique Batch Number
                                    var a = anyCompleteVacutainer.First();
                                    var b = anyCompleteVacutainer.Last();
                                    var c = facilityCode;
                                    var generatedBatchNumber = $"KNBTS{c}V{a.Id}{b.Id}";
                                    #endregion

                                    BatchProcessing newBatchProcessing = new BatchProcessing()
                                    {
                                        BatchNumber = generatedBatchNumber,
                                        BatchProcessingTypes = BatchProcessingTypes.Vacutainer,
                                        Sent = true,
                                        TeamLead = donationDrive.TeamLead,
                                        FacilityToName = facilityTo.Name,
                                        DispatchedBy = userName,
                                        DonationDrive = donationDrive
                                    };

                                    context.BatchProcessings.Add(newBatchProcessing);
                                    await context.SaveChangesAsync();

                                    List<VacutainerContractModel> vacutainerContractModelsList = new List<VacutainerContractModel>();

                                    foreach (var item in anyCompleteVacutainer)
                                    {
                                        Vacutainer vacutainer = context.Vacutainers
                                          .Where(x => x.EntityGuid == item.EntityGuid)
                                          .Include(x => x.DonationEncounter)
                                          .ThenInclude(x => x.Donor)
                                          .SingleOrDefault();

                                        VacutainerContractModel vacutainerContractModel = new VacutainerContractModel()
                                        {
                                            DonorNumber = vacutainer.DonationEncounter.Donor.DonorNumber,
                                            Label = vacutainer.Label,
                                            Type = vacutainer.VacutainerType
                                        };
                                        vacutainerContractModelsList.Add(vacutainerContractModel);

                                        vacutainer.BatchProcessing = newBatchProcessing;
                                        vacutainer.Sent = true;
                                        context.Vacutainers.Update(vacutainer);
                                        await context.SaveChangesAsync();
                                    }

                                    CreateOrUpdateScreeningVacutainerCommand createOrUpdateScreeningVacutainerCommand = new CreateOrUpdateScreeningVacutainerCommand()
                                    {
                                        BatchNumber = generatedBatchNumber,
                                        BatchType = vacutainerDispatch.VacutainerType,
                                        DonationDriveCode = donationDrive.Code,
                                        DonationDriveName = donationDrive.SiteName,
                                        FacilityFrom = facilityCode,
                                        FacilityTo = vacutainerDispatch.FacilityTo,
                                        Vacutainers = vacutainerContractModelsList,
                                        TeamLead = donationDrive.TeamLead
                                    };
                                    await (await sendEndpointProvider.GetSendEndpoint(screeningBatchServiceAddress)).Send(createOrUpdateScreeningVacutainerCommand, cancellationToken);


                                }
                            }
                        }
                        #endregion

                        #region Dispatch Blood Bags

                        if (request.ComfirmBagDispatch == true)
                        {
                            Facility facilityTo = context.Facilities
                                .FirstOrDefault(x => x.Code == facilityCode);

                            var completedBloodBagList = context.BloodBagDonationDetails
                                .Where(x => x.DonationEncounter.IsComplete == true && x.Sent == false && x.Discarded == false && x.DonationEncounter.DonationDrive.EntityGuid == donationDrive.EntityGuid)
                                .Include(x => x.DonationEncounter)
                                .ThenInclude(x => x.Donor)
                                .OrderBy(x => x.Id)
                                .ToList();

                            if (completedBloodBagList.Count() > 0)
                            {
                                List<BloodPintContract> bloodPintContractsList = new List<BloodPintContract>();

                                #region Generate Unique Batch Number
                                var a = completedBloodBagList.First();
                                var b = completedBloodBagList.Last();
                                var c = facilityCode;
                                var generatedBatchNumber = $"KNBTS{c}B{a.Id}{b.Id}";
                                #endregion

                                BatchProcessing completedBloodBagBatch = new BatchProcessing()
                                {
                                    BatchNumber = generatedBatchNumber,
                                    BatchProcessingTypes = BatchProcessingTypes.BloodBag,
                                    Sent = true,
                                    TeamLead = donationDrive.TeamLead,
                                    FacilityToName = facilityTo.Name,
                                    DispatchedBy = userName,
                                    DonationDrive = donationDrive
                                };

                                context.BatchProcessings.Add(completedBloodBagBatch);
                                await context.SaveChangesAsync();



                                foreach (var bloodBag in completedBloodBagList)
                                {
                                    BloodPintContract bloodPintContract = new BloodPintContract()
                                    {
                                        DonationDate = bloodBag.CreatedOnUtc,
                                        DonationNumber = bloodBag.DonationNumber,
                                        DonorNumber = bloodBag.DonationEncounter.Donor.DonorNumber,
                                        BloodBagType = bloodBag.BloodBagType,
                                        Quantity = bloodBag.QuantityCollected,
                                        QuantityUnit = bloodBag.QuantityUnit
                                    };

                                    bloodPintContractsList.Add(bloodPintContract);

                                    bloodBag.BatchProcessing = completedBloodBagBatch;
                                    bloodBag.Sent = true;
                                    context.BloodBagDonationDetails.Update(bloodBag);
                                    await context.SaveChangesAsync();
                                }

                                BloodPintBatchContract bloodPintBatchContract = new()
                                {
                                    BatchNumber = generatedBatchNumber,
                                    BatchType = EntityDefault.BloodBagBatch,
                                    DonationDriveName = donationDrive.SiteName,
                                    DonationDriveCode = donationDrive.Code,
                                    BloodPints = bloodPintContractsList,
                                    DateSend = DateTime.UtcNow,
                                    FacilityFrom = facilityCode,
                                    FacilityTo = facilityCode,
                                    TeamLead = donationDrive.TeamLead
                                };

                                await (await sendEndpointProvider.GetSendEndpoint(bloodDonatedForSeparationServiceAddress)).Send(bloodPintBatchContract, cancellationToken);

                            }
                        }

                        #endregion

                        #region Dispatch Discards
                        if (request.ConfirmDiscardDispatch == true)
                        {
                            var discardedBloodBagsList = context.BloodBagDonationDetails
                            .Where(x => x.Sent == false && x.Discarded == true && x.DonationEncounter.DonationDrive.EntityGuid == donationDrive.EntityGuid)
                            .Include(x => x.DonationEncounter)
                            .ThenInclude(x => x.DonationDrive)
                            .Include(x => x.DonationEncounter)
                            .ThenInclude(x => x.Donor)
                            .Include(x => x.DonationEncounter)
                            .ThenInclude(x => x.Vacutainers)
                            .OrderBy(x=> x.Id)
                            .ToList();
                            if (discardedBloodBagsList.Count() > 0)
                            {

                                Facility facilityTo = context.Facilities
                                .SingleOrDefault(x => x.Code == facilityCode);

                                #region Generate Unique Batch Number
                                var a = discardedBloodBagsList.First();
                                var b = discardedBloodBagsList.Last();
                                var c = facilityCode;
                                var generatedBatchNumber = $"KNBTS{c}D{a.Id}{b.Id}";
                                #endregion

                                List<DonationContract> donationContractsList = new List<DonationContract>();

                                BatchProcessing discardBatch = new BatchProcessing()
                                {
                                    BatchNumber = generatedBatchNumber,
                                    BatchProcessingTypes = BatchProcessingTypes.Discards,
                                    Sent = true,
                                    TeamLead = donationDrive.TeamLead,
                                    FacilityToName = facilityTo.Name,
                                    DispatchedBy = userName,
                                    DonationDrive = donationDrive
                                };

                                context.BatchProcessings.Add(discardBatch);
                                await context.SaveChangesAsync();



                                foreach (var discardBag in discardedBloodBagsList)
                                {
                                    discardBag.Sent = true;
                                    discardBag.BatchProcessing = discardBatch;
                                    context.BloodBagDonationDetails.Update(discardBag);
                                    await context.SaveChangesAsync();

                                    DonationContract donationContract = new DonationContract()
                                    {
                                        DonationDate = discardBag.CreatedOnUtc,
                                        DonationNumber = discardBag.DonationNumber,
                                        DonorNumber = discardBag.DonationEncounter.Donor.DonorNumber,
                                        BloodBagType = discardBag.BloodBagType,
                                        Note = discardBag.DiscardReason,
                                        BloodComponents = new List<BloodComponentContract>()
                                    {
                                        new BloodComponentContract
                                        {
                                            ComponentNumber = EntityDefault.WholeBloodCode,
                                            Name = EntityDefault.WholeBloodName,
                                            PreparedBy = discardBag.CollectedBy,
                                            DatePrepared = DateTime.UtcNow,
                                            Note = discardBag.DiscardReason,
                                            Quantity = discardBag.QuantityCollected,
                                            QuantityUnit = discardBag.QuantityUnit
                                        }
                                    }
                                    };
                                    donationContractsList.Add(donationContract);
                                }

                                DonationBatchContract bloodDonatedBatch = new()
                                {
                                    BatchNumber = generatedBatchNumber,
                                    BatchType = EntityDefault.DiscardBatch,
                                    DonationDriveName = donationDrive.SiteName,
                                    DonationDriveCode = donationDrive.Code,
                                    Donations = donationContractsList,
                                    DateSend = DateTime.UtcNow,
                                    FacilityFrom = facilityCode,
                                    FacilityTo = facilityCode,
                                    TeamLead = donationDrive.TeamLead
                                };
                                await (await sendEndpointProvider.GetSendEndpoint(bloodDonatedAndDiscardedServiceAddress)).Send(bloodDonatedBatch, cancellationToken);

                            }
                        }
                        #endregion

                        return Result.Success();
                    }
                    else
                    {
                        return Result.Failure("The Donation Drive does not exist");
                    }
                }
                catch (Exception ex)
                {
                    return Result.Failure($"{ex.Message}");
                    //Console.WriteLine("Very bad things happened " + ex.Message);
                }

            }
        }
    }
}
