﻿using AutoMapper;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Models.BloodBagDonationDetails;
using BMIS.Donation.Models.DonationDrives;
using BMIS.Donation.Models.DonationEncounters.DonationEncounterAtDonation;
using BMIS.Donation.Models.ProcessingBatches;
using BMIS.Donation.Models.Vacutainers;

namespace BMIS.Donation.Features.DonationSections
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<DonationEncounter, DonationEncounterViewModel>();
            CreateMap<Donor, DonorModel>();

            CreateMap<DonationDrive, DonationDriveViewModel>();

            CreateMap<Vacutainer, DispatchVacutainerModel>();
            CreateMap<DispatchVacutainerModel, Vacutainer>();

            CreateMap<DispatchBloodBagModel, BloodBagDonationDetail>();
            CreateMap<BloodBagDonationDetail, DispatchBloodBagModel>();

            CreateMap<BloodBagDonationDetail, BloodBagDonationDetailViewModel>();
            CreateMap<Vacutainer, VacutainerViewModel>();

            CreateMap<Facility, FacilityOptions.FacilityOptionsViewModel>()
                .ForMember(x => x.Value, y => y.MapFrom(z => z.Code))
                .ForMember(x => x.Label, y => y.MapFrom(z => z.Name));

            CreateMap<BatchProcessing, BatchProcessingViewModel>();
        }
    }
}
