﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Extensions.Paging;
using BMIS.Donation.Models.Vacutainers;
using CSharpFunctionalExtensions;
using Humanizer;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationDbAccess
{
    public class VacutainerListByDonation
    {
        public class Query : IRequest<Result<PagedResult<VacutainerViewModel>>>
        {
            public int DonationEncounterId { get; set; }
            public Query(int donationEncounterId, int? page, string s, string facilityCode)
            {
                DonationEncounterId = donationEncounterId;
                Page = page;
                Q = s;
                FacilityCode = facilityCode;
            }

            public int? Page { get; set; }
            public string Q { get; set; }
            public string FacilityCode { get; set; }
        }

        public class QueryHandler : IRequestHandler<Query, Result<PagedResult<VacutainerViewModel>>>
        {
            private readonly IConfigurationProvider provider;
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;

            public QueryHandler(IConfigurationProvider provider, MainContext context, IHttpContextAccessor httpContextAccessor)
            {
                this.provider = provider;
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
            }

            public async Task<Result<PagedResult<VacutainerViewModel>>> Handle(Query request, CancellationToken cancellationToken)
            {

                string facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;

                var pageNumber = request.Page ?? 1;
                var q = request.Q?.ToLower();

                var vacutainers = context.Vacutainers
                    .Include(x => x.DonationEncounter)
                    .ThenInclude(x => x.Facility)
                    .Where(x => x.DonationEncounter.Id == request.DonationEncounterId && x.DonationEncounter.Facility.Code == facilityCode)
                    .AsNoTracking();
                var results = !string.IsNullOrWhiteSpace(q)
                        ? vacutainers
                            .Where(x => EF.Functions.Like(x.Label.ToLower(), $"%{q}%"))
                            .OrderByDescending(x => x.CreatedOnUtc)
                        : vacutainers.OrderByDescending(x => x.CreatedOnUtc);
                return Result.Success(await results.GetPagedAsync<Vacutainer, VacutainerViewModel>(provider, pageNumber));
            }
        }
    }
}