﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Domain.Enums;
using BMIS.Donation.Extensions.Paging;
using BMIS.Donation.Models.DonationEncounters.DonationEncounterAtDonation;
using CSharpFunctionalExtensions;
using Humanizer;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationDbAccess
{
    public class DonorDonationList
    {
        public class Query : IRequest<Result<PagedResult<DonationEncounterViewModel>>>
        {
            public Guid DriveGuid { get; set; }
            public Query(Guid driveGuid, int? page, string s, string facilityCode)
            {
                DriveGuid = driveGuid;
                Page = page;
                Q = s;
                FacilityCode = facilityCode;
            }

            public int? Page { get; set; }
            public string Q { get; set; }
            public string FacilityCode { get; set; }
        }

        public class QueryHandler : IRequestHandler<Query, Result<PagedResult<DonationEncounterViewModel>>>
        {
            private readonly IConfigurationProvider provider;
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;

            public QueryHandler(IConfigurationProvider provider, MainContext context, IHttpContextAccessor httpContextAccessor)
            {
                this.provider = provider;
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
            }

            public async Task<Result<PagedResult<DonationEncounterViewModel>>> Handle(Query request, CancellationToken cancellationToken)
            {

                string facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;

                var pageNumber = request.Page ?? 1;
                var q = request.Q?.ToLower();

                var donationEncounters = context.DonationEncounters
                    .Include(x => x.Facility)
                    .Include(x => x.DonationDrive)
                    .Include(x => x.Donor)
                    .Where(x => x.DonationDrive.EntityGuid == request.DriveGuid && x.Facility.Code == facilityCode)
                    .AsNoTracking();
                var results = !string.IsNullOrWhiteSpace(q)
                        ? donationEncounters
                            .Where(x => EF.Functions.Like(x.Donor.DonorNumber.ToLower(), $"%{q}%") || EF.Functions.Like(x.Donor.Name.ToLower(), $"%{q}%"))
                            .OrderByDescending(x => x.CreatedOnUtcDonationQueue)
                        : donationEncounters.OrderByDescending(x => x.CreatedOnUtcDonationQueue);
                return Result.Success(await results.GetPagedAsync<DonationEncounter, DonationEncounterViewModel>(provider, pageNumber));
            }
        }
    }
}