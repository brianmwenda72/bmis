﻿using AutoMapper;
using BMIS.Donation.Data;
using CSharpFunctionalExtensions;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationDbAccess
{
    public class FacilityDelete
    {
        public class Command : IRequest<Result>
        {
            public Command() { }
        }
        public class CommandHandler : IRequestHandler<Command, Result>
        {
            private readonly IMapper mapper;
            private readonly MainContext context;


            public CommandHandler(IMapper mapper, MainContext context)
            {
                this.mapper = mapper;
                this.context = context;
            }

            public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
            {
                var facilities = context.Facilities
                    .ToList();
                if (facilities.Count() > 0)
                {
                    foreach (var facility in facilities)
                    {
                        context.Facilities.Remove(facility);
                        await context.SaveChangesAsync();
                    }
                }
                else
                {
                    return Result.Failure("facilities count is 0");
                }

                return Result.Success();
            }
        }
    }
}