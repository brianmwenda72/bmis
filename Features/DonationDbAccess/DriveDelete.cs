﻿using AutoMapper;
using BMIS.Donation.Data;
using CSharpFunctionalExtensions;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationDbAccess
{
    public class DriveDelete
    {
        public class Command : IRequest<Result>
        {
            public Command() { }
        }
        public class CommandHandler : IRequestHandler<Command, Result>
        {
            private readonly IMapper mapper;
            private readonly MainContext context;


            public CommandHandler(IMapper mapper, MainContext context)
            {
                this.mapper = mapper;
                this.context = context;
            }

            public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
            {
                var drives = context.DonationDrives
                    .ToList();

                if (drives.Count() > 0)
                {
                    foreach (var drive in drives)
                    {
                        context.DonationDrives.Remove(drive);
                        await context.SaveChangesAsync();
                    }
                }
                else
                {
                    return Result.Failure("drives count is 0");
                }

                return Result.Success();
            }
        }
    }
}