﻿using BMIS.Donation.Abstracts;
using BMIS.Donation.Models.DonationEncounters.DonationEncounterAtCounselling;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationDbAccess
{
    public class ModuleDbAccess : BaseApiController
    {
        [HttpDelete("delete-bags")]
        public async Task<IActionResult> BagDelete() => Feedback(await Mediator.Send(new BagDelete.Command()), "Record deleted successfully");

        [HttpDelete("delete-batches")]
        public async Task<IActionResult> BatchDelete() => Feedback(await Mediator.Send(new BatchDelete.Command()), "Record deleted successfully");

        [HttpDelete("delete-donors")]
        public async Task<IActionResult> DonorDelete() => Feedback(await Mediator.Send(new DonorDelete.Command()), "Record deleted successfully");

        [HttpDelete("delete-drives")]
        public async Task<IActionResult> DriveDelete() => Feedback(await Mediator.Send(new DriveDelete.Command()), "Record deleted successfully");

        [HttpDelete("delete-encounters")]
        public async Task<IActionResult> EncounterDelete() => Feedback(await Mediator.Send(new EncounterDelete.Command()), "Record deleted successfully");

        [HttpDelete("delete-facilities")]
        public async Task<IActionResult> FacilityDelete() => Feedback(await Mediator.Send(new FacilityDelete.Command()), "Record deleted successfully");

        [HttpDelete("delete-questionnaires")]
        public async Task<IActionResult> QuestionnaireDelete() => Feedback(await Mediator.Send(new QuestionnaireDelete.Command()), "Record deleted successfully");

        [HttpDelete("delete-vacutainers")]
        public async Task<IActionResult> VacutainerDelete() => Feedback(await Mediator.Send(new VacutainerDelete.Command()), "Record deleted successfully");

        [HttpGet("{driveGuid}/drive-donation-encounter-list")]
        public async Task<IActionResult> GetDonationEncounterList(Guid driveGuid, int? page, string q, string facilityCode) => Process(await Mediator.Send(new DonorDonationList.Query(driveGuid, page, q, facilityCode)));

        [HttpGet("{donationEcounterId}/donation-bag-list")]
        public async Task<IActionResult> GetBagListByDonationId(int donationEcounterId, int? page, string q, string facilityCode) => Process(await Mediator.Send(new BagListByDonation.Query(donationEcounterId, page, q, facilityCode)));

        [HttpGet("{donationEcounterId}/donation-vacutainer-list")]
        public async Task<IActionResult> GetVacutainerListByDonationId(int donationEcounterId, int? page, string q, string facilityCode) => Process(await Mediator.Send(new VacutainerListByDonation.Query(donationEcounterId, page, q, facilityCode)));

        [HttpGet("all-drive-list")]
        public async Task<IActionResult> GetAllDriveList(int? page, string q) => Process(await Mediator.Send(new DonationDriveList.Query(page, q)));

        [HttpGet("donor-encounter-list")]
        public async Task<IActionResult> GetDonorList(int? page, string q) => Process(await Mediator.Send(new DonorList.Query(page, q)));

    }
}

