﻿using AutoMapper;
using BMIS.Donation.Data;
using CSharpFunctionalExtensions;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationDbAccess
{
    public class VacutainerDelete
    {
        public class Command : IRequest<Result>
        {
            public Command() { }
        }
        public class CommandHandler : IRequestHandler<Command, Result>
        {
            private readonly IMapper mapper;
            private readonly MainContext context;


            public CommandHandler(IMapper mapper, MainContext context)
            {
                this.mapper = mapper;
                this.context = context;
            }

            public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
            {
                var vacutainers = context.Vacutainers
                    .ToList();
                if (vacutainers.Count() > 0)
                {
                    foreach (var vacutainer in vacutainers)
                    {
                        context.Vacutainers.Remove(vacutainer);
                        await context.SaveChangesAsync();
                    }
                }
                else
                {
                    return Result.Failure("vacutainers count is 0");
                }

                return Result.Success();
            }
        }
    }
}