﻿using AutoMapper;
using BMIS.Donation.Data;
using CSharpFunctionalExtensions;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationDbAccess
{
    public class BagDelete
    {
        public class Command : IRequest<Result>
        {
            public Command() { }
        }
        public class CommandHandler : IRequestHandler<Command, Result>
        {
            private readonly IMapper mapper;
            private readonly MainContext context;


            public CommandHandler(IMapper mapper, MainContext context)
            {
                this.mapper = mapper;
                this.context = context;
            }

            public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
            {
                var bags = context.BloodBagDonationDetails
                    .ToList();
                if (bags.Count() > 0)
                {
                    foreach (var bag in bags)
                    {
                        context.BloodBagDonationDetails.Remove(bag);
                        await context.SaveChangesAsync();
                    }
                }
                else
                {
                    return Result.Failure("bags count is 0");
                }

                return Result.Success();
            }
        }
    }
}