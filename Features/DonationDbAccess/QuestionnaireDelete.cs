﻿using AutoMapper;
using BMIS.Donation.Data;
using CSharpFunctionalExtensions;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationDbAccess
{
    public class QuestionnaireDelete
    {
        public class Command : IRequest<Result>
        {
            public Command() { }
        }
        public class CommandHandler : IRequestHandler<Command, Result>
        {
            private readonly IMapper mapper;
            private readonly MainContext context;


            public CommandHandler(IMapper mapper, MainContext context)
            {
                this.mapper = mapper;
                this.context = context;
            }

            public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
            {
                var questionnaires = context.Questionnaires
                    .ToList();
                if (questionnaires.Count() > 0)
                {
                    foreach (var questionnaire in questionnaires)
                    {
                        context.Questionnaires.Remove(questionnaire);
                        await context.SaveChangesAsync();
                    }
                }
                else
                {
                    return Result.Failure("questionnaires count is 0");
                }

                return Result.Success();
            }
        }
    }
}