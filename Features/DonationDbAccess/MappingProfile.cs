﻿using AutoMapper;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Models.BloodBagDonationDetails;
using BMIS.Donation.Models.DonationDrives;
using BMIS.Donation.Models.DonationEncounters.DonationEncounterAtDonation;
using BMIS.Donation.Models.Vacutainers;

namespace BMIS.Donation.Features.DonationDbAccess
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<bool, string>().ConvertUsing(x => x.ToString().ToLower());

            CreateMap<DonationEncounter, DonationEncounterViewModel>();
            CreateMap<Donor, DonorModel>();
            CreateMap<DonorModel, DonationEncounterViewModel>();

            CreateMap<BloodBagDonationDetail, BloodBagDonationDetailViewModel>();
            CreateMap<Vacutainer, VacutainerViewModel>();
            CreateMap<DonationDrive, DonationDriveViewModel>();
        }
    }
}
