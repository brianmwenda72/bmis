﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Domain.Enums;
using BMIS.Donation.Extensions.Paging;
using BMIS.Donation.Models.BloodBagDonationDetails;
using CSharpFunctionalExtensions;
using Humanizer;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationDbAccess
{
    public class BagListByDonation
    {
        public class Query : IRequest<Result<PagedResult<BloodBagDonationDetailViewModel>>>
        {
            public int DonationEncounterId { get; set; }
            public Query(int donationEncounterId, int? page, string s, string facilityCode)
            {
                DonationEncounterId = donationEncounterId;
                Page = page;
                Q = s;
                FacilityCode = facilityCode;
            }

            public int? Page { get; set; }
            public string Q { get; set; }
            public string FacilityCode { get; set; }
        }

        public class QueryHandler : IRequestHandler<Query, Result<PagedResult<BloodBagDonationDetailViewModel>>>
        {
            private readonly IConfigurationProvider provider;
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;

            public QueryHandler(IConfigurationProvider provider, MainContext context, IHttpContextAccessor httpContextAccessor)
            {
                this.provider = provider;
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
            }

            public async Task<Result<PagedResult<BloodBagDonationDetailViewModel>>> Handle(Query request, CancellationToken cancellationToken)
            {

                string facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;

                var pageNumber = request.Page ?? 1;
                var q = request.Q?.ToLower();

                var bags = context.BloodBagDonationDetails
                    .Include(x => x.DonationEncounter)
                    .ThenInclude(x => x.Facility)
                    .Where(x => x.DonationEncounter.Id == request.DonationEncounterId && x.DonationEncounter.Facility.Code == facilityCode)
                    .AsNoTracking();
                var results = !string.IsNullOrWhiteSpace(q)
                        ? bags
                            .Where(x => EF.Functions.Like(x.BloodBagLabel.ToLower(), $"%{q}%"))
                            .OrderByDescending(x => x.CreatedOnUtc)
                        : bags.OrderByDescending(x => x.CreatedOnUtc);
                return Result.Success(await results.GetPagedAsync<BloodBagDonationDetail, BloodBagDonationDetailViewModel>(provider, pageNumber));
            }
        }
    }
}