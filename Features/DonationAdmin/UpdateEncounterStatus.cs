﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Domain.Enums;
using CSharpFunctionalExtensions;
using DHP.Contracts;
using DHP.Contracts.Events.BloodBank;
using MassTransit;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationAdmin
{
    public class UpdateEncounterStatus
    {
        public class Command : IRequest<Result>
        {
            [JsonIgnore]
            public int DonationEncounterId { get; set; }

            public Command(int donationEncounterId)
            {
                this.DonationEncounterId = donationEncounterId;
            }
            public string EncounterStatus { get; set; }
        }
        public class CommandHandler : IRequestHandler<Command, Result>
        {
            private readonly IMapper mapper;
            private readonly MainContext context;


            public CommandHandler(IMapper mapper, MainContext context, IHttpContextAccessor httpContextAccessor, ISendEndpointProvider sendEndpointProvider)
            {
                this.mapper = mapper;
                this.context = context;
            }

            public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
            {
                DonationEncounter donationEncounter = context.DonationEncounters
                    .FirstOrDefault(x => x.Id == request.DonationEncounterId);
                if (donationEncounter != null)
                {
                    donationEncounter.DonationEncounterStatus = (DonationEncounterStatus)Enum.Parse(typeof(DonationEncounterStatus), request.EncounterStatus);
                    context.Update(donationEncounter);
                    await context.SaveChangesAsync();
                    return Result.Success();
                }
                else
                {
                    return Result.Failure("Donation Encounter does not exist");
                }
            }
        }
    }
}
