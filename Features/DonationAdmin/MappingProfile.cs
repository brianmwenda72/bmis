﻿using AutoMapper;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Models.DonationEncounters.DonationEncounterAtDonation;

namespace BMIS.Donation.Features.DonationAdmin
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<DonationEncounter, DonationEncounterViewModel>();
            CreateMap<Donor, DonorModel>();
        }
    }
}
