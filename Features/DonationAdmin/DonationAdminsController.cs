using BMIS.Donation.Abstracts;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationAdmin
{
    public class DonationAdminsController : BaseApiController
    {
        #region Encounter List
        [HttpGet("all-donation-encounter-list")]
        public async Task<IActionResult> GetDonationEncounterList(int? page, string q, string facilityCode)
        {
            return Process(await Mediator.Send(new DonationEncounterList.Query(page, q, facilityCode)));
        }
        #endregion

        #region Update Encounter Status
        [HttpPost("{donationEncounterId}/update-donation-encounter-status")]
        public async Task<IActionResult> UpdateEncounterStatusAsync(int donationEncounterId, UpdateEncounterStatus.Command command)
        {
            command.DonationEncounterId = donationEncounterId;
            return Feedback(await Mediator.Send(command), ActionPerformed.Add);
        }
        #endregion

        #region Update Encounter Donation Number
        [HttpPost("{donationEncounterId}/update-donation-number")]
        public async Task<IActionResult> UpdateDonationNumberAsync(int donationEncounterId, UpdateDonationNumber.Command command)
        {
            command.DonationEncounterId = donationEncounterId;
            return Feedback(await Mediator.Send(command), ActionPerformed.Add);
        }
        #endregion
    }
}