﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using CSharpFunctionalExtensions;
using MassTransit;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationAdmin
{
    public class UpdateDonationNumber
    {
        public class Command : IRequest<Result>
        {
            [JsonIgnore]
            public int DonationEncounterId { get; set; }

            public Command(int donationEncounterId)
            {
                this.DonationEncounterId = donationEncounterId;
            }
            public string DonationNumber { get; set; }
        }
        public class CommandHandler : IRequestHandler<Command, Result>
        {
            private readonly IMapper mapper;
            private readonly MainContext context;


            public CommandHandler(IMapper mapper, MainContext context, IHttpContextAccessor httpContextAccessor, ISendEndpointProvider sendEndpointProvider)
            {
                this.mapper = mapper;
                this.context = context;
            }

            public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
            {
                DonationEncounter donationEncounter = context.DonationEncounters
                    .Include(x=> x.BloodBagDonationDetails)
                    .Include(x=> x.Vacutainers)
                    .FirstOrDefault(x => x.Id == request.DonationEncounterId);
                if (donationEncounter != null)
                {
                    var donationNumberValidator = context.BloodBagDonationDetails
                                .Where(x => x.Deleted == false)
                                .Any(a => a.BloodBagLabel.ToLower() == request.DonationNumber.ToLower());

                    if (donationNumberValidator == true)
                    {
                        return Result.Failure("The donation number provided for this bag has already been assigned");
                    }
                    else
                    {
                        BloodBagDonationDetail bloodBagDonationDetail = context.BloodBagDonationDetails
                            .FirstOrDefault(x => x.DonationEncounter == donationEncounter && x.Discarded == false);
                        if (bloodBagDonationDetail != null)
                        {
                            bloodBagDonationDetail.BloodBagLabel = request.DonationNumber;
                            context.Update(bloodBagDonationDetail);
                            await context.SaveChangesAsync();
                        }

                        var vacutainerList = context.Vacutainers
                            .Where(x => x.DonationEncounter == donationEncounter)
                            .ToList();

                        foreach (var vacutainer in vacutainerList)
                        {
                            vacutainer.Label = request.DonationNumber;
                            context.Update(vacutainer);
                            await context.SaveChangesAsync();
                        }

                        donationEncounter.DonationNumber = request.DonationNumber;
                        context.Update(donationEncounter);
                        await context.SaveChangesAsync();

                        return Result.Success();
                    }
                }
                else
                {
                    return Result.Failure("Donation Encounter does not exist");
                }
            }
        }
    }
}
