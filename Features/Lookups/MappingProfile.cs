﻿using AutoMapper;
using BMIS.Donation.Domain.Entities;

namespace BMIS.Donation.Features.Lookups
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Create.LookupViewModel, Lookup>()
                .ForMember(x => x.Value, y => y.MapFrom(z => z.Value.Trim().ToLower().Replace(" ", "-")));
            CreateMap<Update.LookupViewModel, Lookup>();
            CreateMap<Delete.LookupViewModel, Lookup>();
            CreateMap<Lookup, List.ListViewModel>();
            CreateMap<Lookup, Details.LookupVm>();
            CreateMap<Lookup, GetById.GetByIdViewModel>();
            CreateMap<Lookup, Details.DetailsViewModel>()
                .ForMember(x => x.Children, y => y.MapFrom(z => z.Children));
            CreateMap<Lookup, GetLookupsByValue.GetByValueViewModel>();
        }
    }
}