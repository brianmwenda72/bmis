﻿using AutoMapper;
using BMIS.Donation.Extensions.Paging;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.Lookups
{
    public class Details
    {
        public class Query : IRequest<DetailsViewModel>
        {
            public Query(int id, int? page, string s)
            {
                Page = page;
                Q = s;
                Id = id;
            }

            public int Id { get; set; }
            public int? Page { get; set; }
            public string Q { get; set; }
        }

        public class DetailsViewModel
        {
            public int Id { get; set; }
            public string Label { get; set; }
            public PagedResult<LookupVm> Children { get; set; }
        }

        public class LookupVm
        {
            public int Id { get; set; }
            public string Label { get; set; }
            public string Value { get; set; }
            public int DisplayOrder { get; set; }
            public bool IsActive { get; set; }
        }

        public class QueryHandler : IRequestHandler<Query, DetailsViewModel>
        {
            private readonly IConfigurationProvider provider;
            private readonly MainContext db;

            public QueryHandler(IConfigurationProvider provider, MainContext db)
            {
                this.provider = provider;
                this.db = db;
            }

            public async Task<DetailsViewModel> Handle(Query request, CancellationToken cancellationToken)
            {
                var pageNumber = request.Page ?? 1;
                var q = request.Q?.ToLower();

                var lookup = db.Lookups
                    .AsNoTracking()
                    .Include(x => x.Children);
                var result = !string.IsNullOrWhiteSpace(q)
                    ? await lookup
                        .Include(x => x.Children.Where(c => EF.Functions.Like(c.Value.ToLower(), $"%{q}%")))
                        .FirstAsync(x => x.Id == request.Id, cancellationToken: cancellationToken)
                    : await lookup.FirstAsync(x => x.Id == request.Id, cancellationToken: cancellationToken);
                return new DetailsViewModel
                {
                    Children = result.Children.OrderBy(x => x.DisplayOrder).AsQueryable()
                        .GetPaged<Lookup, LookupVm>(pageNumber, 10, provider),
                    Label = result.Label,
                    Id = result.Id
                };
            }
        }
    }
}