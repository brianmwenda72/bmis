﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using CSharpFunctionalExtensions;
using MediatR;
using Newtonsoft.Json;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.Lookups
{
    public class Update
    {
        public class LookupViewModel : IRequest<Result>
        {
            [JsonIgnore]
            public int Id { get; set; }
            public string Label { get; set; }
            public string Value { get; set; }
            public int DisplayOrder { get; set; }
            public bool IsActive { get; set; }
            
        }

        public class LookupViewModelHandler : IRequestHandler<LookupViewModel, Result>
        {
            private readonly IMapper mapper;
            private readonly MainContext context;

            public LookupViewModelHandler(IMapper mapper,
                MainContext context)
            {
                this.mapper = mapper;
                this.context = context;
            }
            public async Task<Result> Handle(LookupViewModel request, CancellationToken cancellationToken)
            {
                Lookup lookup = context.Lookups
                    .Where(x => x.Id == request.Id)
                    .SingleOrDefault();
                if (lookup is not null)
                {
                    if (string.IsNullOrWhiteSpace(request.Value))
                    {

                        lookup.Label = request.Label;
                        lookup.Value = request.Label.Trim().ToLower().Replace(" ", "-");
                        lookup.DisplayOrder = request.DisplayOrder;
                        lookup.IsActive = request.IsActive;

                        context.Update(lookup);
                        await context.SaveChangesAsync();
                    }
                    else
                    {
                        lookup.Label = request.Label;
                        lookup.Value = request.Value;
                        lookup.DisplayOrder = request.DisplayOrder;
                        lookup.IsActive = request.IsActive;

                        context.Update(lookup);
                        await context.SaveChangesAsync();
                    }

                    return Result.Success();
                }
                else
                {
                    return Result.Failure("The Lookup Id does not exist");
                }
            }

        }
    }
}
