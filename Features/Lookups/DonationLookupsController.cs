﻿using BMIS.Donation.Abstracts;
using BMIS.Donation.Extensions.Paging;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.Lookups
{
    public class DonationLookupsController : BaseApiController
    {
        [HttpPost()]
        [Route("{parentid}")]
        public async Task<IActionResult> CreateLookupAsync(int parentid, Create.LookupViewModel lookupViewModel)
        {
            lookupViewModel.ParentId = parentid;
            return Feedback(await Mediator.Send(lookupViewModel), ActionPerformed.Add);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetLookupsByIdAsync(int id) => Process(await Mediator.Send(new GetById.Query(id)));

        [HttpGet("{id:int}/parentoptions")]
        public async Task<ActionResult<Details.DetailsViewModel>> GetLookupsByIdAsync(int id, int page, string q) => await Mediator.Send(new Details.Query(id, page, q));

        [HttpGet]
        public async Task<ActionResult<PagedResult<List.ListViewModel>>> GetLookups(int? page, string q) => await Mediator.Send(new List.Query(page, q));

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateLookupAsync(int id, Update.LookupViewModel lookupViewModel)
        {
            lookupViewModel.Id = id;
            return Feedback(await Mediator.Send(lookupViewModel), ActionPerformed.Update);
        }

        [HttpGet("{value}/options")]
        public async Task<ActionResult<List<GetLookupsByValue.GetByValueViewModel>>> GetLookupsByValue(string value) => await Mediator.Send(new GetLookupsByValue.Query(value));
    }
}