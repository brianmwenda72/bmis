﻿using AutoMapper;
using BMIS.Donation.Extensions.Paging;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.Lookups
{
    public class List
    {
        public class Query : IRequest<PagedResult<ListViewModel>>
        {
            public Query(int? page, string q)
            {
                Page = page;
                Q = q;
            }

            public int? Page { get; set; }
            public string Q { get; set; }
        }

        public class ListViewModel
        {
            public int Id { get; set; }
            public string Value { get; set; }
            public string Label { get; set; }
            public int DisplayOrder { get; set; }
            public bool IsActive { get; set; }
        }

        public class QueryHandler : IRequestHandler<Query, PagedResult<ListViewModel>>
        {
            private readonly IConfigurationProvider provider;
            private readonly MainContext db;

            public QueryHandler(IConfigurationProvider provider, MainContext db)
            {
                this.provider = provider;
                this.db = db;
            }

            public async Task<PagedResult<ListViewModel>> Handle(Query request, CancellationToken cancellationToken)
            {
                var pageNumber = request.Page ?? 1;
                var q = request.Q?.ToLower();
                var lookup = db.Lookups
                    .AsNoTracking()
                    .Include(x => x.Parent)
                    .Where(x => x.Parent == null);

                var results = !string.IsNullOrWhiteSpace(q)
                    ? lookup
                        .Where(x => EF.Functions.Like(x.Value.ToLower(), $"%{q}%"))
                        .OrderBy(x => x.DisplayOrder)
                    : lookup.OrderBy(x => x.DisplayOrder);

                return await results.GetPagedAsync<Lookup, ListViewModel>(provider, pageNumber);
            }
        }
    }
}