﻿using CSharpFunctionalExtensions;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BMIS.Donation.Data;

namespace BMIS.Donation.Features.Lookups
{
    public class Delete
    {
        public class LookupViewModel : IRequest<Result>
        {
            public LookupViewModel(int id)
            {
                Id = id;
            }

            public int Id { get; set; }
        }

        public class LookupViewModelHandler : IRequestHandler<LookupViewModel, Result>
        {
            private readonly MainContext db;

            public LookupViewModelHandler(MainContext db)
            {
                this.db = db;
            }

            public async Task<Result> Handle(LookupViewModel request, CancellationToken cancellationToken)
            {
                var lookup = await db.Lookups.FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
                if (lookup is null) return Result.Failure("Record not found");
                db.Lookups.Remove(lookup);
                await db.SaveChangesAsync(cancellationToken);
                return Result.Success();
            }
        }
    }
}