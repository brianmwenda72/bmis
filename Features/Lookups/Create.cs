﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using CSharpFunctionalExtensions;
using MediatR;
using Newtonsoft.Json;
using System.Threading;
using System.Threading.Tasks;


namespace BMIS.Donation.Features.Lookups
{
    /// <summary>
    /// Creates a lookup
    /// </summary>
    public class Create
    {
        public class LookupViewModel : IRequest<Result>
        {
            public string Value { get; set; }
            public string Label { get; set; }
            public int DisplayOrder { get; set; }
            public bool IsActive { get; set; }
            [JsonIgnore]
            public int ParentId { get; internal set; }
        }

        public class LookupViewModelHandler : IRequestHandler<LookupViewModel, Result>
        {
            private readonly IMapper mapper;
            private readonly MainContext context;

            public LookupViewModelHandler(IMapper mapper,
                MainContext context)
            {
                this.mapper = mapper;
                this.context = context;
            }
            public async Task<Result> Handle(LookupViewModel request, CancellationToken cancellationToken)
            {
                if (string.IsNullOrWhiteSpace(request.Value))
                {
                    request.Value = request.Label.Trim().ToLower().Replace(" ", "-");
                }
                Lookup lookup = mapper.Map<Lookup>(request);
                if (request.ParentId != 0)
                {
                    lookup.Parent = context.Lookups.Find(request.ParentId);
                }
                context.Add(lookup);
                await context.SaveChangesAsync();

                return Result.Success();
            }
        }
    }
}
