﻿using AutoMapper;
using BMIS.Donation.Data;
using CSharpFunctionalExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.Lookups
{
    public class GetById
    {
        public class Query : IRequest<Result<GetByIdViewModel>>
        {
            public Query(int id)
            {
                Id = id;
            }

            public int Id { get; set; }
        }

        public class GetByIdViewModel
        {
            public int Id { get; set; }
            public string Label { get; set; }
            public string Value { get; set; }
            public int DisplayOrder { get; set; }
            public bool IsActive { get; set; }
        }

        public class QueryHandler : IRequestHandler<Query, Result<GetByIdViewModel>>
        {
            private readonly MainContext db;
            private readonly IMapper mapper;

            public QueryHandler(MainContext db, IMapper mapper)
            {
                this.db = db;
                this.mapper = mapper;
            }

            public async Task<Result<GetByIdViewModel>> Handle(Query request, CancellationToken cancellationToken)
            {
                var lookup = await db.Lookups
                    .AsNoTracking()
                    .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken: cancellationToken);
                return lookup != null ? Result.Success(mapper.Map<GetByIdViewModel>(lookup)) : Result.Failure<GetByIdViewModel>("Sorry the id does not exist");
            }
        }
    }
}