﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using BMIS.Donation.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.Lookups
{
    public class GetLookupsByValue
    {
        public class Query : IRequest<List<GetByValueViewModel>>
        {
            public Query(string value)
            {
                Value = value;
            }

            public string Value { get; internal set; }
        }

        public class GetByValueViewModel
        {
            public string Value { get; set; }
            public string Label { get; set; }
            public int DisplayOrder { get; set; }
            public bool IsActive { get; set; }
        }

        public class QueryHandler : IRequestHandler<Query, List<GetByValueViewModel>>
        {
            private readonly IConfigurationProvider provider;
            private readonly MainContext db;

            public QueryHandler(IConfigurationProvider provider,
                MainContext db)
            {
                this.provider = provider;
                this.db = db;
            }

            public async Task<List<GetByValueViewModel>> Handle(Query request, CancellationToken cancellationToken)
            {
                var parent = await db.Lookups
                    .FirstOrDefaultAsync(x => x.Value == request.Value, cancellationToken: cancellationToken);
                if (parent == null)
                {
                    return new List<GetByValueViewModel>();
                }
                var children = await db.Lookups
                    .Include(x => x.Parent)
                    .Where(x => x.Parent == parent)
                    .OrderBy(x => x.DisplayOrder)
                    .ProjectTo<GetByValueViewModel>(provider)
                    .ToListAsync(cancellationToken: cancellationToken);

                return children;
            }
        }
    }
}