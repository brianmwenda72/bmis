﻿using BMIS.Donation.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationReports
{

    public class SiteDonationReport
    {
        public class Query : IRequest<List<Model>>
        {



        }

        public class Model
        {
            public string Name { get; set; }
            public int SuccessfulDonations { get; set; }
        }

        public class QueryHandler : IRequestHandler<Query, List<Model>>
        {
            private readonly MainContext context;
            public QueryHandler(MainContext context)
            {
                this.context = context;
            }
            public async Task<List<Model>> Handle(Query request, CancellationToken cancellationToken)
            {
                return await context.DonationEncounters
                                        .Include(x => x.Facility)
                                        .Where(x => x.IsComplete == true && x.Defered == false)
                                      .GroupBy(x => x.Facility.Name)
                                      .Select(g => new Model
                                      {
                                          Name = g.Key,
                                          SuccessfulDonations = g.Count()
                                      })
                                      .OrderBy(x => x.SuccessfulDonations)
                                      .Take(5)
                                      .ToListAsync(cancellationToken);
            }


        }
    }
}
