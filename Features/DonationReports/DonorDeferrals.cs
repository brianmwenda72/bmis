﻿using BMIS.Donation.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationReports
{

    public class DonorDeferrals
    {
        public class Query : IRequest<List<Model>>
        {



        }

        public class Model
        {

            public int No_of_Defered { get; set; }
            public string Gender { get; set; }

            public string DefermentReason { get; set; }

        }

        public class QueryHandler : IRequestHandler<Query, List<Model>>
        {
            private readonly MainContext context;
            public QueryHandler(MainContext context)
            {
                this.context = context;
            }
            public async Task<List<Model>> Handle(Query request, CancellationToken cancellationToken)
            {
                return await context.DonationEncounters
                                      .Include(x => x.Donor)
                                      .Where(x => x.Defered == true)
                                      .GroupBy(x => x.Donor.Gender)
                                      .Select(g => new Model
                                      {
                                          Gender = g.Key,
                                          No_of_Defered  = g.Count(),
                                      })

                                      .OrderBy(x => x.No_of_Defered )
                                      .Take(5)
                                      .ToListAsync(cancellationToken);
            }


        }
    }
}
