﻿using BMIS.Donation.Data;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationReports
{

    public class DrivesMonthlyQuery
    {
        public class Query : IRequest<List<DriveQueryOutput>>
        {

            [JsonIgnore]
            public string FacilityCode { get; set; }
            [JsonIgnore]
            public int Year { get; set; }

            public Query(string facilityCode, int year)
            {
                FacilityCode = facilityCode;
                Year = year;
            }

        }
        [Keyless]
        public class DriveQueryOutput
        {
            public string Month { get; set; }
            public int MonthInt { get; set; }
            public int ActiveDrives { get; set; }
            public int CompletedDrives { get; set; }
        }

        public class QueryHandler : IRequestHandler<Query, List<DriveQueryOutput>>
        {
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;
            public QueryHandler(MainContext context, IHttpContextAccessor httpContextAccessor)
            {
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
            }
            public async Task<List<DriveQueryOutput>> Handle(Query request, CancellationToken cancellationToken)
            {
                var facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;
                var facility = context.Facilities
                    .AsNoTracking()
                    .FirstOrDefault(x => x.Code == facilityCode);
                var rawDrives = new List<DriveQueryOutput>();
                if (facility != null)
                {
                    if (facilityCode == "001")
                    {
                        rawDrives = await context.DriveQueryOutputs
                            .FromSqlInterpolated(
                                $@"SELECT
                        TO_CHAR(schedule_start_date, 'Mon') AS month,
                        EXTRACT(MONTH FROM schedule_start_date) AS month_int,
                        COALESCE(COUNT((CASE WHEN donation_drive_status= 1 THEN 1 END)), 0) AS active_drives,
                        COALESCE(COUNT(CASE WHEN donation_drive_status=2 THEN 1 END), 0) AS completed_drives
                    FROM donation_drives
                    WHERE EXTRACT(YEAR FROM schedule_start_date) = {request.Year}
                    GROUP BY TO_CHAR(schedule_start_date, 'Mon'), EXTRACT(MONTH FROM schedule_start_date)
                    ORDER BY EXTRACT(MONTH FROM schedule_start_date")
                            .ToListAsync(cancellationToken);
                    }
                    else
                    {
                        rawDrives = await context.DriveQueryOutputs
                            .FromSqlInterpolated(
                                $@"WITH cte_x
                    AS
                    (SELECT
                        TO_CHAR(schedule_start_date, 'Mon') AS Month,
                        EXTRACT(MONTH FROM schedule_start_date) AS MonthInt,
                        COALESCE(CASE donation_drive_status WHEN 1 THEN COUNT(*) END, 0) AS Active,
                        COALESCE(CASE donation_drive_status WHEN 2 THEN COUNT(*) END, 0) AS Complete
                    FROM donation_drives
                    WHERE EXTRACT(YEAR FROM schedule_start_date) = {request.Year}  AND facility_id = {facility.Id}
                    GROUP BY TO_CHAR(schedule_start_date, 'Mon'), EXTRACT(MONTH FROM schedule_start_date), donation_drive_status
                    ORDER BY EXTRACT(MONTH FROM schedule_start_date))
                    SELECT
                      cte_x.Month,
                      cte_x.MonthInt,
                      SUM(cte_x.Active) AS active_drives,
                      SUM(cte_x.Complete) AS completed_drives
                    FROM cte_x
                    GROUP BY Month,
                             MonthInt
                    ORDER BY MonthInt")
                            .ToListAsync(cancellationToken);

                    }


                }
                return rawDrives;
            }
        }
    }
}
