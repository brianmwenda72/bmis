﻿using BMIS.Donation.Data;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationReports
{

    public class DonationDailyQuery
    {
        public class Query : IRequest<List<DailyDonationQueryOutput>>
        {

            [JsonIgnore]
            public string FacilityCode { get; set; }

            public Query(string facilityCode)
            {
                FacilityCode = facilityCode;
            }

        }
        [Keyless]
        public class DailyDonationQueryOutput
        {
            public string Day { get; set; }
            public int SuccessfulDonations { get; set; }
            public int DiferredDonations { get; set; }
            public double DayInt { get; set; }
        }

        public class QueryHandler : IRequestHandler<Query, List<DailyDonationQueryOutput>>
        {
            private readonly MainContext context;
            private readonly IHttpContextAccessor httpContextAccessor;
            public QueryHandler(MainContext context, IHttpContextAccessor httpContextAccessor)
            {
                this.context = context;
                this.httpContextAccessor = httpContextAccessor;
            }
            public async Task<List<DailyDonationQueryOutput>> Handle(Query request, CancellationToken cancellationToken)
            {
                var facilityCode = request.FacilityCode ?? httpContextAccessor.HttpContext?.User.FindFirst("facility_code")?.Value;
                var facility = context.Facilities
                    .FirstOrDefault(x => x.Code == facilityCode);
                var dateNow = DateTime.UtcNow;
                var cul = CultureInfo.CurrentCulture;
                var weekRange = cul.Calendar.GetWeekOfYear(dateNow, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
                var weekNum = weekRange - 1;
                List<DailyDonationQueryOutput> rawDailyDonations;
                if (facility is { Code: "001" })
                {
                    rawDailyDonations = await context.DailyDonationQueryOutputs
                        .FromSqlInterpolated(
                            $@"WITH cte_x
                    AS
                    (SELECT
                        TO_CHAR(created_on_utc_donation_queue, 'Dy') AS Day,
                        EXTRACT(DOW FROM created_on_utc_donation_queue) AS DayInt,
                        COALESCE(CASE donation_encounter_status WHEN 5 THEN COUNT(*) END, 0) AS SuccessfulDonations,
                        COALESCE(CASE donation_encounter_status WHEN 8 THEN COUNT(*) END, 0) AS DiferredDonations
                    FROM donation_encounters
                    WHERE EXTRACT(WEEK FROM created_on_utc_donation_queue) = {weekNum}  
                    GROUP BY TO_CHAR(created_on_utc_donation_queue, 'Dy'), EXTRACT(DOW FROM created_on_utc_donation_queue), donation_encounter_status
                    ORDER BY EXTRACT(DOW FROM created_on_utc_donation_queue))
                    SELECT
                      cte_x.Day,
                    cte_x.DayInt AS day_int,
                      SUM(cte_x.SuccessfulDonations) AS successful_donations,
                      SUM(cte_x.DiferredDonations) AS diferred_donations
                    FROM cte_x
                    GROUP BY Day,
                             DayInt
                    ORDER BY DayInt")
                        .ToListAsync(cancellationToken);
                }
                else
                {
                    rawDailyDonations = await context.DailyDonationQueryOutputs
                        .FromSqlInterpolated(
                            $@"WITH cte_x
                    AS
                    (SELECT
                        TO_CHAR(created_on_utc_donation_queue, 'Dy') AS Day,
                        EXTRACT(DOW FROM created_on_utc_donation_queue) AS DayInt,
                        COALESCE(CASE donation_encounter_status WHEN 5 THEN COUNT(*) END, 0) AS SuccessfulDonations,
                        COALESCE(CASE donation_encounter_status WHEN 8 THEN COUNT(*) END, 0) AS DiferredDonations
                    FROM donation_encounters
                    WHERE EXTRACT(WEEK FROM created_on_utc_donation_queue) = {weekNum}  AND facility_id = {facility?.Id}
                    GROUP BY TO_CHAR(created_on_utc_donation_queue, 'Dy'), EXTRACT(DOW FROM created_on_utc_donation_queue), donation_encounter_status
                    ORDER BY EXTRACT(DOW FROM created_on_utc_donation_queue))
                    SELECT
                      cte_x.Day,
                        cte_x.DayInt AS day_int,
                      SUM(cte_x.SuccessfulDonations) AS successful_donations,
                      SUM(cte_x.DiferredDonations) AS diferred_donations
                    FROM cte_x
                    GROUP BY Day,
                             DayInt
                    ORDER BY DayInt")
                        .ToListAsync(cancellationToken);
                }
                return rawDailyDonations ?? new List<DailyDonationQueryOutput>();
            }
        }
    }
}
