using BMIS.Donation.Abstracts;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.DonationReports
{
    [Route("api/donation-reports")]
    public class DonationReportsController : BaseApiController
    {
        #region Drive Queries
        [HttpGet("{year:int}/monthly-drives-report")]
        public async Task<IActionResult> DrivesMonthlyQueryAsync(string facilityCode, int year)
        {
            return Process<List<DrivesMonthlyQuery.DriveQueryOutput>>(await Mediator.Send(new DrivesMonthlyQuery.Query(facilityCode, year)));
        }
        #endregion

        #region Donation Queries
        [HttpGet("recent-week-donation-report")]
        public async Task<IActionResult> DonationDailyQueryAsync(string facilityCode)
        {
            return Process<List<DonationDailyQuery.DailyDonationQueryOutput>>(await Mediator.Send(new DonationDailyQuery.Query(facilityCode)));
        }
        #endregion
        [HttpGet("donations-by-county")]
        public async Task<IActionResult> DonationsByCounty() => Ok(await Mediator.Send(new CountiesDonationReport.Query()));

        [HttpGet("donations-by-site")]
        public async Task<IActionResult> DonationsBySite() => Ok(await Mediator.Send(new SiteDonationReport.Query()));

        [HttpGet("Donor-Deferrals")]
        public async Task<IActionResult> DonorDeferrals() => Ok(await Mediator.Send(new DonorDeferrals.Query()));
    }
}