﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Models.FinancialYearDefinitions;
using CSharpFunctionalExtensions;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.FinancialYearDefinitions
{
    public class Detail
    {
        public class Query : IRequest<Result<FinancialYearDefinitionViewModel>>
        {
            public Query(int id)
            {
                Id = id;
            }

            public int Id { get; }
        }

        public class QueryHandler : IRequestHandler<Query, Result<FinancialYearDefinitionViewModel>>
        {
            private readonly IMapper mapper;
            private readonly MainContext context;

            public QueryHandler(IMapper mapper, MainContext context)
            {
                this.mapper = mapper;
                this.context=context;
            }

            public async Task<Result<FinancialYearDefinitionViewModel>> Handle(Query request, CancellationToken cancellationToken)
            {
                var currentFinancialYearDefinition = await context.FinancialYearDefinitions.FindAsync(request.Id);

                if (currentFinancialYearDefinition is not null)
                {
                    var formViewModel = mapper.Map<FinancialYearDefinitionViewModel>(currentFinancialYearDefinition);

                    return Result.Success(formViewModel);
                }

                return Result.Failure<FinancialYearDefinitionViewModel>("FinancialYearDefinition not found");
            }
        }
    }
}