﻿using AutoMapper;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Extensions.Paging;
using BMIS.Donation.Models.FinancialYearDefinitions;
using CSharpFunctionalExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.FinancialYearDefinitions
{
    public class List
    {
        public class Query : IRequest<Result<PagedResult<FinancialYearDefinitionViewModel>>>
        {
            public Query(int? page, string s)
            {
                Page = page;
                Q = s;
            }

            public int? Page { get; set; }
            public string Q { get; set; }
        }

        public class QueryHandler : IRequestHandler<Query, Result<PagedResult<FinancialYearDefinitionViewModel>>>
        {
            private readonly IConfigurationProvider provider;
            private readonly MainContext context;

            public QueryHandler(IConfigurationProvider provider, MainContext context)
            {
                this.provider = provider;
                this.context = context;
            }

            public async Task<Result<PagedResult<FinancialYearDefinitionViewModel>>> Handle(Query request, CancellationToken cancellationToken)
            {
                var pageNumber = request.Page ?? 1;
                var q = request.Q?.ToLower();
                var year = context.FinancialYearDefinitions
                    .AsNoTracking();
                var results = !string.IsNullOrWhiteSpace(q)
                        ? year
                            .Where(x => EF.Functions.Like(x.Name.ToLower(), $"%{q}%"))
                            .OrderBy(x => x.Name)
                        : year.OrderBy(x => x.Name);
                return Result.Success(await results.GetPagedAsync<FinancialYearDefinition, FinancialYearDefinitionViewModel>(provider, pageNumber));
            }
        }
    }
}