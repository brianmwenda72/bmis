﻿using AutoMapper;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Models.FinancialYearDefinitions;

namespace BMIS.Donation.Features.FinancialYearDefinitions
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<FinancialYearDefinition, FinancialYearDefinitionViewModel>();
        }
    }
}
