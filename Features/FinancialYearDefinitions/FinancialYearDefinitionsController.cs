using BMIS.Donation.Abstracts;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.FinancialYearDefinitions
{
    public class FinancialYearDefinitionsController : BaseApiController
    {
        [HttpGet]
        public async Task<IActionResult> GetFinancialYearDefinitions(int? page, string q) => Process(await Mediator.Send(new List.Query(page, q)));

        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetFinancialYearDefinitionById(int id) => Process(await Mediator.Send(new Detail.Query(id)));
    }
}