﻿using AutoMapper;
using BMIS.Donation.Extensions.Paging;
using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.Facilities
{
    public class List
    {
        public class Query : IRequest<PagedResult<Model>>
        {
            public Query(int? page, string s)
            {
                Page = page;
                Q = s;
            }

            public int? Page { get; set; }
            public string Q { get; set; }
        }

        public class Model
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Code { get; set; }
            public string County { get; set; }
            public string SubCounty { get; set; }
            public string Ward { get; set; }
        }

        public class QueryHandler : IRequestHandler<Query, PagedResult<Model>>
        {
            private readonly MainContext context;
            private readonly IConfigurationProvider provider;
            public QueryHandler(MainContext context, IConfigurationProvider provider)
            {
                this.context = context;
                this.provider = provider;
            }

            public async Task<PagedResult<Model>> Handle(Query request, CancellationToken cancellationToken)
            {
                var pageNumber = request.Page ?? 1;
                var q = request.Q?.ToLower().Trim();
                var model = context.Facilities.AsNoTracking();

                var results = !string.IsNullOrWhiteSpace(q)
                    ? model
                        .Where(x => x.Name.ToLower() == q)
                        .OrderByDescending(x => x.Id)
                    : model.OrderByDescending(x => x.Id);

                return await results.GetPagedAsync<Facility, Model>(provider, pageNumber);

            }
        }
    }
}
