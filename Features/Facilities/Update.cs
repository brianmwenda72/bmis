﻿using AutoMapper;
using BMIS.Donation.Data;
using CSharpFunctionalExtensions;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.Facilities
{
    public class Update
    {
        public class Command : IRequest<Result>
        {
            public Command(int id)
            {
                Id = id;
            }

            public int Id { get; set; }
            public string Name { get; set; }
            public string Code { get; set; }
            public string County { get; set; }
            public string SubCounty { get; set; }
            public string Ward { get; set; }
        }

        public class CommandHandler : IRequestHandler<Command, Result>
        {
            private readonly IMapper mapper;
            private readonly MainContext context;

            public CommandHandler(IMapper mapper, MainContext context)
            {
                this.mapper = mapper;
                this.context=context;
            }

            public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
            {
                var currentFacility = await context.Facilities.FindAsync(request.Id);

                if (currentFacility == null)
                {
                    return Result.Failure("Facility not found");
                }
                else
                {
                    currentFacility.Name = request.Name;
                    currentFacility.Code = request.Code;
                    currentFacility.County = request.County;
                    currentFacility.SubCounty = request.SubCounty;
                    currentFacility.Ward = request.Ward;
                    context.Facilities.Update(currentFacility);
                    await context.SaveChangesAsync();
                    return Result.Success();
                }

            }
        }
    }
}