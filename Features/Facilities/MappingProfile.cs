﻿using AutoMapper;
using BMIS.Donation.Domain.Entities;

namespace BMIS.Donation.Features.Facilities
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Facility, List.Model>();
        }
    }
}
