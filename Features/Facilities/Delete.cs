﻿using BMIS.Donation.Data;
using BMIS.Donation.Domain.Entities;
using CSharpFunctionalExtensions;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace BMIS.Donation.Features.Facilities
{
    public class Delete
    {
        public class Command : IRequest<Result>
        {
            public Command(int id)
            {
                Id = id;
            }

            public int Id { get; }
        }

        public class CommandHanlder : IRequestHandler<Command, Result>
        {
            private readonly MainContext context;

            public CommandHanlder(MainContext context)
            {
                this.context=context;
            }

            public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
            {
                Facility currentFacility = await context.Facilities.FindAsync(request.Id);
                context.Facilities.Remove(currentFacility);
                await context.SaveChangesAsync();

                return Result.Success();
            }
        }
    }
}
