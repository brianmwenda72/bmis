﻿using BMIS.Donation.Abstracts;
using BMIS.Donation.Extensions.Paging;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BMIS.Donation.Features.Facilities
{
    public class FacilitiesController : BaseApiController
    {
        [HttpGet]
        public async Task<ActionResult<PagedResult<List.Model>>> Get(int? page, string q) => Ok(await Mediator.Send(new List.Query(page, q)));

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDepartment(int id) => Feedback(await Mediator.Send(new Delete.Command(id)), "Record deleted successfully");

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateDepartment(int id) => Feedback(await Mediator.Send(new Update.Command(id)), ActionPerformed.Update);

    }
}
