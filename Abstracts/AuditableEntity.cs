﻿using System;

namespace BMIS.Donation.Abstracts
{
    public abstract class AuditableEntity : BaseEntity
    {
        public DateTime CreatedOnUtc { get; set; } = DateTime.UtcNow;

        public string CreatedBy { get; set; }

        public DateTime? UpdatedOnUtc { get; set; } = DateTime.UtcNow;

        public string LastModifiedBy { get; set; }
        public bool Deleted { get; set; } = false;
    }

    public abstract class AuditableEntity<T> : BaseEntity<T>
    {
        public DateTime CreatedOnUtc { get; set; } = DateTime.UtcNow;

        public string CreatedBy { get; set; }

        public DateTime? UpdatedOnUtc { get; set; } = DateTime.UtcNow;

        public string LastModifiedBy { get; set; }
        public bool Deleted { get; set; } = false;
    }
}