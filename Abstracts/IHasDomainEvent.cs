﻿using System.Collections.Generic;

namespace BMIS.Donation.Abstracts
{
    public interface IHasDomainEvent
    {
        public List<DomainEvent> DomainEvents { get; set; }
    }
}