﻿using System;

namespace BMIS.Donation.Abstracts
{
    /// <summary>
    /// Represents the base entity
    /// </summary>
    public abstract class BaseEntity
    {
        public int Id { get; set; }
    }

    public abstract class BaseEntity<T>
    {
        public T Id { get; set; }
    }
}