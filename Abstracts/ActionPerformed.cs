﻿
namespace BMIS.Donation.Abstracts
{
    public enum ActionPerformed
    {
        Add,
        Delete,
        Update,
        Upload
    }
}