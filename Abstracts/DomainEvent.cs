﻿using System;

namespace BMIS.Donation.Abstracts
{
    public abstract class DomainEvent
    {
        public bool IsPublished { get; set; }
        public DateTime DateOccurred { get; protected set; } = DateTime.UtcNow;
    }
}