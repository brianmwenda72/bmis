﻿using BMIS.Donation.Domain.Defaults;
using BMIS.Donation.Domain.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BMIS.Donation.Models.ProcessingBatches
{
    public class BatchProcessingViewModel
    {
        public int Id { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public Guid EntityGuid { get; set; }
        public string BatchNumber { get; set; }
        public string BatchProcessingTypes { get; set; }
        public string FacilityToName { get; set; }
        public string TeamLead { get; set; }
        public string DispatchedBy { get; set; }
        public bool Received { get; set; }
        public string ReceivedBy { get; set; }
        public DateTime ReceiptTime { get; set; }
        public int TotalNumberOfScreeningVacutainers => Vacutainers.Count(x => x.VacutainerType.ToLower() == EntityDefault.ScreeningVacutainer.ToLower() && x.Sent == true);
        public int TotalNumberOfGroupingVacutainers => Vacutainers.Count(x => x.VacutainerType.ToLower() == EntityDefault.GroupingVacutainer.ToLower() && x.Sent == true);
        public int TotalNumberOfSingleBags => BloodBagDonationDetails.Count(x => x.BloodBagType.ToLower() == EntityDefault.SingleBloodBags.ToLower() && x.Sent == true);
        public int TotalNumberOfDoubleBags => BloodBagDonationDetails.Count(x => x.BloodBagType.ToLower() == EntityDefault.DoubleBloodBags.ToLower() && x.Sent == true);
        public int TotalNumberOfTripleBags => BloodBagDonationDetails.Count(x => x.BloodBagType.ToLower() == EntityDefault.TripleBloodBags.ToLower() && x.Sent == true);
        public int TotalNumberOfQuadrupleBags => BloodBagDonationDetails.Count(x => x.BloodBagType.ToLower() == EntityDefault.QuadrupleBloodBags.ToLower() && x.Sent == true);
        public int TotalNumberOfDiscardedBags => BloodBagDonationDetails.Count(x => x.Discarded == true && x.Sent == true);

        [JsonIgnore]
        public List<BloodBagDonationDetail> BloodBagDonationDetails { get; set; }
        [JsonIgnore]
        public List<Vacutainer> Vacutainers { get; set; }
    }
}
