﻿
using System;

namespace BMIS.Donation.Models.Vacutainers
{
    public class VacutainerViewModel
    {
        public int Id { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public Guid EntityGuid { get; set; }
        public string Label { get; set; }
        public string VacutainerType { get; set; }
    }
}
