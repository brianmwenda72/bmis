﻿using System;

namespace BMIS.Donation.Models.CounsellingDonationDrives
{
    public class DriveModel
    {
        public int Id { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public Guid EntityGuid { get; set; }
        public string SiteName { get; set; }
        public string ScheduleName { get; set; }
        public string Code { get; set; }
        public int TotalPopulation { get; set; }
        public int ExpectedDonors { get; set; }
        public string DonationDriveStatus { get; set; }
        public DateTime ScheduleStartDate { get; set; }
        public DateTime ScheduleEndDate { get; set; }
        public string TeamLead { get; set; }
    }
}
