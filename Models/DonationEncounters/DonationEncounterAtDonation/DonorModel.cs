﻿
using System;

namespace BMIS.Donation.Models.DonationEncounters.DonationEncounterAtDonation
{
    public class DonorModel
    {
        public int Id { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public string DonorNumber { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public DateTime BirthDate { get; set; }
        public int Age { get; set; }
    }
}
