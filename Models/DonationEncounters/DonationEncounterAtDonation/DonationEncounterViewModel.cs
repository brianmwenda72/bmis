﻿using Humanizer;
using Humanizer.Localisation;
using System;

namespace BMIS.Donation.Models.DonationEncounters.DonationEncounterAtDonation
{
    public class DonationEncounterViewModel
    {
        public int Id { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DonorModel Donor { get; set; }
        public string DonationNumber { get; set; }
        public string DonationEncounterStatus { get; set; }
        public DateTime CreatedOnUtcDonationQueue { get; set; }
        public string TimeOnQueue => (DateTime.UtcNow - CreatedOnUtcDonationQueue).Humanize(minUnit: TimeUnit.Second);
        public DateTime DonationStartTime { get; set; }
        public DateTime DonationStopTime { get; set; }
        public bool IsComplete { get; set; }
    }
}
