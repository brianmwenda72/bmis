﻿using BMIS.Donation.Models.CounsellingDonationDrives;
using Humanizer;
using Humanizer.Localisation;
using System;

namespace BMIS.Donation.Models.DonationEncounters.DonationEncounterAtCounselling
{
    public class DonationEncounterDriveViewModel
    {
        public int Id { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DonorModel Donor { get; set; }
        public DriveModel DonationDrive { get; set; }
        public string DonationEncounterStatus { get; set; }
        public string TimeOnQueue => (DateTime.UtcNow - CreatedOnUtc).Humanize(minUnit: TimeUnit.Second);
    }
}
