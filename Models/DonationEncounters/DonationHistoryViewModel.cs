﻿using Humanizer;
using Humanizer.Localisation;
using System;

namespace BMIS.Donation.Models.DonationEncounters
{
    public class DonationHistoryViewModel
    {
        public int Id { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public string DonationNumber { get; set; }
        public string DonationEncounterStatus { get; set; }
        public string TimeOnQueue => (DateTime.UtcNow - CreatedOnUtc).Humanize(minUnit: TimeUnit.Second);
    }
}
