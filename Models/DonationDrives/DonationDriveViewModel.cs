﻿using BMIS.Donation.Domain.Defaults;
using BMIS.Donation.Domain.Entities;
using BMIS.Donation.Domain.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BMIS.Donation.Models.DonationDrives
{
    public class DonationDriveViewModel
    {
        public int Id { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public Guid EntityGuid { get; set; }
        public string SiteName { get; set; }
        public string ScheduleName { get; set; }
        public string Code { get; set; }
        public int TotalPopulation { get; set; }
        public int ExpectedDonors { get; set; }
        public string DonationDriveStatus { get; set; }
        public DateTime ScheduleStartDate { get; set; }
        public DateTime ScheduleEndDate { get; set; }
        public string TeamLead { get; set; }
        public int ActiveDonors => DonationEncounters.Count(x => x.IsComplete == false && x.Defered == false && x.DonationEncounterStatus != DonationEncounterStatus.PendingCounselling);
        public int CompleteDonations => DonationEncounters.Count(x => x.DonationEncounterStatus == DonationEncounterStatus.CompleteDonation);
        public int DeferedDonationsAtDonations => DonationEncounters.Count(x => x.DonationEncounterStatus == DonationEncounterStatus.DeferredAtDonation);
        public int DiscardsAtDonations => DonationEncounters.Where(x => x.IsComplete == true).SelectMany(x => x.BloodBagDonationDetails).Count(x => x.BloodBagDonataionDetailStatus == BloodBagDonataionDetailStatus.Discarded && x.Sent == false);
        public int NoOfSingleBags => DonationEncounters.Where(x => x.IsComplete == true).SelectMany(x => x.BloodBagDonationDetails).Count(x => x.BloodBagType.ToLower() == EntityDefault.SingleBloodBags.ToLower() && x.Discarded == false && x.Sent == false);
        public int NoOfDoubleBags => DonationEncounters.Where(x => x.IsComplete == true).SelectMany(x => x.BloodBagDonationDetails).Count(x => x.BloodBagType.ToLower() == EntityDefault.DoubleBloodBags.ToLower() && x.Discarded == false && x.Sent == false);
        public int NoOfTripleBags => DonationEncounters.Where(x => x.IsComplete == true).SelectMany(x => x.BloodBagDonationDetails).Count(x => x.BloodBagType.ToLower() == EntityDefault.TripleBloodBags.ToLower() && x.Discarded == false && x.Sent == false);
        public int NoOfQuadrupleBags => DonationEncounters.Where(x => x.IsComplete == true).SelectMany(x => x.BloodBagDonationDetails).Count(x => x.BloodBagType.ToLower() == EntityDefault.QuadrupleBloodBags.ToLower() && x.Discarded == false && x.Sent == false);
        public int NoOfGroupingVacutainers => DonationEncounters.Where(x => x.IsComplete == true).SelectMany(x => x.Vacutainers).Count(x => x.VacutainerType.ToLower() == EntityDefault.GroupingVacutainer.ToLower() && x.Sent == false);
        public int NoOfScreeningVacutainers => DonationEncounters.Where(x=> x.IsComplete == true).SelectMany(x => x.Vacutainers).Count(x => x.VacutainerType.ToLower() == EntityDefault.ScreeningVacutainer.ToLower() && x.Sent == false);

        [JsonIgnore]
        public List<DonationEncounter> DonationEncounters { get; set; } = new List<DonationEncounter>();

    }
}
