﻿using System;

namespace BMIS.Donation.Models.Questionnaires
{
    public class PortalQuestionnaireViewModel
    {
        public QuestionnaireModel Response { get; set; }
    }
}
