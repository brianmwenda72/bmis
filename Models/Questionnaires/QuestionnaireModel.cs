﻿
using System;

namespace BMIS.Donation.Models.Questionnaires
{
    public class QuestionnaireModel
    {
        //Health Questionnaires
        public string FeelingWellToday { get; set; }
        public string EatenInTheLastSixHours { get; set; }
        public string EverFainted { get; set; }
        public string AnyTreatmentOrMedicationsInPast6Months { get; set; }
        public string AnyInjectionsOrVaccinationsInPast6Months { get; set; }
        public string AnyBloodOrOrganTransfusionInThePast12Months { get; set; }
        public string AnyPregnancyOrBreastFeedingForFemaleDonors { get; set; }
        public string AnyProblemsWithHeartOrLungs { get; set; }
        public string AnyBleedingConditionsOrBloodDisease { get; set; }
        public string DoYouHaveCancer { get; set; }
        public string DoYouHaveDiabetisOrEpilepsyOrTB { get; set; }
        public string AnyOtherLongTermIllness { get; set; }
        public string SpecifyLongTermIllnessIfAny { get; set; }

        //Risk Assessment
        public string HaveReceivedOrGivenAnySexualFavours { get; set; }
        public string BeenRapedOrSodomised { get; set; }
        public string HaveHadSTDs { get; set; }
        public string HaveHadRecklessSex { get; set; }
        public string HaveHadYellowEyesOrSkin { get; set; }
        public string HaveUsedNonMedicinalDrugs { get; set; }
        public string DoYouConsiderYourBloodSafe { get; set; }
        public string HaveAnySexBesidesRegularPartner { get; set; }
        public string HaveAnyStubWoundOrAccidentalNeedleStickInjury { get; set; }
        public string HaveSexualContactOrLivedWithSomeoneWithYellowEyesOrSkin { get; set; }
        public string AnyInjectionOutsideHealthFacility { get; set; }
        public string HaveYouAndPartnerBeenTestedForHIV { get; set; }

        //Declaration
        public string DonationType { get; set; }
        public string DeclarationConsentText { get; set; }
        public string Consented { get; set; }
    }
}
