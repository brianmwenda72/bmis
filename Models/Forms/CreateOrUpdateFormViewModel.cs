﻿using Newtonsoft.Json.Linq;

namespace BMIS.Donation.Models.Forms
{
    public class CreateOrUpdateFormViewModel
    {
        public string Label { get; set; }
        public string Name { get; set; }
        public JArray JsonSchemas { get; set; }
    }
}