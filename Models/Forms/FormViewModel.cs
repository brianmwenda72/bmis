﻿using Newtonsoft.Json.Linq;
using System;

namespace BMIS.Donation.Models.Forms
{
    public class FormViewModel
    {
        public int Id { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public string Label { get; set; }
        public string Name { get; set; }
        public dynamic JsonSchemas { get; set; }
    }
}