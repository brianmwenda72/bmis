﻿using System;

namespace BMIS.Donation.Models.BloodBagDonationDetails
{
    public class BloodBagDonationDetailViewModel
    {
        public int Id { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public Guid EntityGuid { get; set; }
        public int DonationEncounterId { get; set; }
        public string DonationMethod { get; set; }
        public string DonationNumber { get; set; }
        public string BloodBagType { get; set; }
        public string BloodBagLabel { get; set; }
        public string BloodBagDonataionDetailStatus { get; set; }
        public int QuantityCollected { get; set; }
        public string QuantityUnit { get; set; }
        public bool IsSufficient { get; set; }
        public bool Sent { get; set; }
        public bool Discarded { get; set; }
        public string DiscardReason { get; set; }
        public decimal Weight { get; set; }
        public string WeightUnit { get; set; }
        public DateTime NeedleTimeIn { get; set; }
        public DateTime? NeedleTimeOut { get; set; }
    }
}
