﻿using Newtonsoft.Json.Linq;

namespace BMIS.Donation.Models.CounsellingSections.VitalModels
{
    public class CreateOrUpdateVitalViewModel
    {
        public decimal Weight { get; set; }
        public decimal Height { get; set; }
        public decimal BMI { get; set; }
        public decimal Temparature { get; set; }
        public decimal Haemoglobin { get; set; }
        public decimal SystolicBP { get; set; }
        public decimal DystolicBP { get; set; }
        public decimal PulseRate { get; set; }
    }
}