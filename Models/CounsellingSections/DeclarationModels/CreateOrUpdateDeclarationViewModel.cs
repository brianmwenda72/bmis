﻿using Newtonsoft.Json.Linq;

namespace BMIS.Donation.Models.CounsellingSections.DeclarationModels
{
    public class CreateOrUpdateDeclarationViewModel
    {
        public string DonationType { get; set; }
        public string DeclarationConsentText { get; set; }
        public string Consented { get; set; }
    }
}