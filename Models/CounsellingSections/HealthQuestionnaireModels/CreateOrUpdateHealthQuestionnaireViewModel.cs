﻿using Newtonsoft.Json.Linq;

namespace BMIS.Donation.Models.CounsellingSections.HealthQuestionnaireModels
{
    public class CreateOrUpdateHealthQuestionnaireViewModel
    {
        public string FeelingWellToday { get; set; }
        public string EatenInTheLastSixHours { get; set; }
        public string EverFainted { get; set; }
        public string AnyTreatmentOrMedicationsInPast6Months { get; set; }
        public string AnyInjectionsOrVaccinationsInPast6Months { get; set; }
        public string AnyBloodOrOrganTransfusionInThePast12Months { get; set; }
        public string AnyProblemsWithHeartOrLungs { get; set; }
        public string AnyBleedingConditionsOrBloodDisease { get; set; }
        public string AnyPregnancyOrBreastFeedingForFemaleDonors { get; set; }
        public string DoYouHaveCancer { get; set; }
        public string DoYouHaveDiabetisOrEpilepsyOrTB { get; set; }
        public string AnyOtherLongTermIllness { get; set; }
        public string SpecifyLongTermIllnessIfAny { get; set; }
    }
}