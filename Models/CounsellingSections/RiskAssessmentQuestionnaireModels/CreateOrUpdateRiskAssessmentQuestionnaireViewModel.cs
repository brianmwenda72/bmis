﻿using Newtonsoft.Json.Linq;

namespace BMIS.Donation.Models.CounsellingSections.RiskAssessmentQuestionnaireModels
{
    public class CreateOrUpdateRiskAssessmentQuestionnaireViewModel
    {
        public string HaveReceivedOrGivenAnySexualFavours { get; set; }
        public string BeenRapedOrSodomised { get; set; }
        public string HaveHadSTDs { get; set; }
        public string HaveHadRecklessSex { get; set; }
        public string HaveHadYellowEyesOrSkin { get; set; }
        public string HaveUsedNonMedicinalDrugs { get; set; }
        public string DoYouConsiderYourBloodSafe { get; set; }
        public string HaveAnySexBesidesRegularPartner { get; set; }
        public string HaveAnyStubWoundOrAccidentalNeedleStickInjury { get; set; }
        public string HaveSexualContactOrLivedWithSomeoneWithYellowEyesOrSkin { get; set; }
        public string AnyInjectionOutsideHealthFacility { get; set; }
        public string HaveYouAndPartnerBeenTestedForHIV { get; set; }
    }
}